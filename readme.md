## Sistema de registro del Congreso Nacional de Software Libre

Para mas información sobre el Congreso Nacional de Software Libre visita [la web del evento](http://cnsl.org.ve).

### Especificaciones

* Lenguaje de programación [PHP 5.4](http://php.net)
* Framework [Laravel 4.2](http://laravel.com)
* RDBMS [MariaDB 5.5](http://mariadb.org/)

Para mas información por favor dirigirse a la [wiki del proyecto](/cnsl-dev/cnsl11/wiki)