<?php 


class PaisesTableSeeder extends Seeder {
 
    public function run()
    {
        $paises = array(
        	array('nombre' => 'Venezuela', 'visible' => TRUE),
        );
 
        DB::table('paises')->insert($paises);
    }
 
}