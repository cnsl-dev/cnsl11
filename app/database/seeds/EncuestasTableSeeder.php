<?php 

class EncuestasTableSeeder extends Seeder {
 
    public function run()
    {
        $encuestas = array(
        	array(
        		'titulo' => 'Encuesta del XI Congreso Nacional de Software Libre', 
        		'descripcion' => 'Encuesta del XI Congreso Nacional de Software Libre',
        	)
        );

        $encuesta_evento = array(
        	array('encuesta_id' => 1, 'evento_id' => 1)
        );
 
        DB::table('encuestas')->insert($encuestas);
        DB::table('encuesta_evento')->insert($encuesta_evento);
    }
}