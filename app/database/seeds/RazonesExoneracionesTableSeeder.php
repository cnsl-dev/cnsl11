<?php 


class RazonesExoneracionesTableSeeder extends Seeder {
 
    public function run()
    {
        $razones = array(
        	array(
        			'titulo' => 'Ponente', 
        			'certificado_masculino' => 'Ponente', 
        			'certificado_femenino' => 'Ponente'
        		),
        	array(
        			'titulo' => 'Organizador', 
        			'certificado_masculino' => 'Organizador',
        			'certificado_femenino' => 'Organizadora'
        		),
        	array(
        			'titulo' => 'Colaborador', 
        			'certificado_masculino' => 'Colaborador',
        			'certificado_femenino' => 'Colaboradora'
        		),
        	array(
        			'titulo' => 'Patrocinador', 
        			'certificado_masculino' => 'Participante',
        			'certificado_femenino' => 'Participante'
        		),
        	array(
        			'titulo' => 'Persona con Discapacidad', 
        			'certificado_masculino' => 'Participante',
        			'certificado_femenino' => 'Participante'
        		),
        	array(
        			'titulo' => 'Niño, niña o adolescente (<18)', 
        			'certificado_masculino' => 'Participante',
        			'certificado_femenino' => 'Participante'
        		),
        	array(
        			'titulo' => 'Adulto mayor', 
        			'certificado_masculino' => 'Participante',
        			'certificado_femenino' => 'Participante'
        		),
        );
 
        DB::table('razones_exoneraciones')->insert($razones);
    }
 
}