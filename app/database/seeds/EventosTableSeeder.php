<?php 

class EventosTableSeeder extends Seeder {
 
    public function run()
    {
        $eventos = array(
        	array(
        			'nombre' => 'Décimo primer Congreso Nacional de Software Libre', 
        			'abreviatura' => 'CNSL11',
        			'activo' => TRUE,
        	),
        );
 
        DB::table('eventos')->insert($eventos);
    }
 
}