<?php 


class UsuariosTableSeeder extends Seeder {
 
    public function run()
    {
        $usuarios = array(
        	array(
                'nombres' => 'admi',
                'apellidos' => 'nistrador',
        		'dni' => 'V-00000000',
        		'password' => Hash::make('123456'),
        		'email' => 'admin@admin.com',
                'confirmado' => TRUE,
        	)
        );

        $rol_usuario = array(
            array(
                'rol_id' => 1,
                'usuario_id' => 1,
            )
        );
 
        DB::table('usuarios')->insert($usuarios);
        DB::table('rol_usuario')->insert($rol_usuario);
    }
 
}