<?php 

class OpcionesTableSeeder extends Seeder {
 
    public function run()
    {
        $opciones = array(
        	array(
        		'descripcion' => 'Si', 
        		'pregunta_id' => 1,
        		'orden' => 1
        	),
        	array(
        		'descripcion' => 'No', 
        		'pregunta_id' => 1,
        		'orden' => 2
        	),
        	array(
        		'descripcion' => 'No sé absolutamente nada de informática', 
        		'pregunta_id' => 2,
        		'orden' => 1
        	),
        	array(
        		'descripcion' => 'Sé cómo usar básicamente un computador', 
        		'pregunta_id' => 2,
        		'orden' => 2
        	),
        	array(
        		'descripcion' => 'Uso regularmente computadoras', 
        		'pregunta_id' => 2,
        		'orden' => 3
        	),
        	array(
        		'descripcion' => 'Resuelvo problemas en mi sistema operativo', 
        		'pregunta_id' => 2,
        		'orden' => 4
        	),
        	array(
        		'descripcion' => 'Programo o administro servidores', 
        		'pregunta_id' => 2,
        		'orden' => 5
        	),
        	array(
        		'descripcion' => 'No sé qué es GNU/Linux', 
        		'pregunta_id' => 3,
        		'orden' => 1
        	),
        	array(
        		'descripcion' => 'Conozco y he usado un poco GNU/Linux', 
        		'pregunta_id' => 3,
        		'orden' => 2
        	),
        	array(
        		'descripcion' => 'Soy usuario de GNU/linux, pero como sistema secundario', 
        		'pregunta_id' => 3,
        		'orden' => 3
        	),
        	array(
        		'descripcion' => 'Prefiero GNU/Linux pero uso otros sistemas también', 
        		'pregunta_id' => 3,
        		'orden' => 4
        	),
        	array(
        		'descripcion' => 'Sólo uso GNU/Linux, no existe otro sistema para mi', 
        		'pregunta_id' => 3,
        		'orden' => 5
        	),
        	array(
        		'descripcion' => 'Si', 
        		'pregunta_id' => 4,
        		'orden' => 1
        	),
        	array(
        		'descripcion' => 'No', 
        		'pregunta_id' => 4,
        		'orden' => 2
        	)
        );
 
        DB::table('opciones')->insert($opciones);
    }
}