<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('RolesTableSeeder');
		$this->call('UsuariosTableSeeder');
		$this->call('ResponsabilidadesRegionalesTableSeeder');
		$this->call('RazonesExoneracionesTableSeeder');
		$this->call('PaisesTableSeeder');
		$this->call('DepartamentosTableSeeder');
		$this->call('CiudadesTableSeeder');
		$this->call('EventosTableSeeder');
		$this->call('SedesTableSeeder');
		$this->call('EncuestasTableSeeder');
		$this->call('PreguntasTableSeeder');
		$this->call('OpcionesTableSeeder');
		$this->call('EjesTematicosTableSeeder');
	}

}
