<?php 

class SedesTableSeeder extends Seeder {
 
    public function run()
    {
        $sedes = array(
            array(
                    'evento_id' => 1,
                    'ciudad_id' => 338,
                    'activo' => TRUE,
                    'actual' => FALSE,
            ),
            array(
                    'evento_id' => 1,
                    'ciudad_id' => 137,
                    'activo' => TRUE,
                    'actual' => TRUE,
            ),
        );
 
        DB::table('sedes')->insert($sedes);
    }
 
}