<?php 


class ResponsabilidadesRegionalesTableSeeder extends Seeder {
 
    public function run()
    {
        $responsabilidades = array(
        	array('nombre' => 'Organizador regional'),
        	array('nombre' => 'Coordinador de ponencias'),
        	array('nombre' => 'Coordinador de recursos'),
        );
 
        DB::table('responsabilidades_regionales')->insert($responsabilidades);
    }
 
}