<?php 

class PreguntasTableSeeder extends Seeder {
 
    public function run()
    {
        $preguntas = array(
        	array(
        		'titulo' => '¿Has asistido anteriormente al CNSL?', 
        		'seleccion' => 'simple',
        		'encuesta_id' => 1,
        		'orden' => 1
        	),
        	array(
        		'titulo' => '¿Cuál es tu nivel de conocimiento de computación?', 
        		'seleccion' => 'simple',
        		'encuesta_id' => 1,
        		'orden' => 2
        	),
        	array(
        		'titulo' => '¿Con cuanta frecuencia usas GNU/Linux?', 
        		'seleccion' => 'simple',
        		'encuesta_id' => 1,
        		'orden' => 3
        	),
        	array(
        		'titulo' => '¿Podemos contactarte a futuro para enviarte información sobre Software Libre?', 
        		'seleccion' => 'simple',
        		'encuesta_id' => 1,
        		'orden' => 4
        	),
        );
 
        DB::table('preguntas')->insert($preguntas);
    }
}