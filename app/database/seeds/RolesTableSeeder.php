<?php 


class RolesTableSeeder extends Seeder {

    public function run()
    {
        $roles = array(
            array(
                'nombre' => 'administrador',
                'descripcion' => 'Usuario administrador del sistema'
            ),
            array(
                'nombre' => 'Coordinador Nacional',
                'descripcion' => 'Coordinador Nacional',
            ),
            array(
                'nombre' => 'Coordinador de Ponencias',
                'descripcion' => 'Coordinador de Ponencias',
            ),
            array(
                'nombre' => 'Coordinador de Registro',
                'descripcion' => 'Coordinador de Registro',
            ),
            array(
                'nombre' => 'Coordinador de Logistica',
                'descripcion' => 'Coordinador de Logistica',
            ),
            array(
                'nombre' => 'Coordinador de Recursos',
                'descripcion' => 'Coordinador de Recursos',
            ),
            array(
                'nombre' => 'Usuario autenticado',
                'descripcion' => 'Menor nivel de acceso posible, se le asigna a los visitantes resiente regitrados',
            ),
        );

        $permisos = array(

            /*******************************************************
             *                                                     *
             *              Permisos del administrador             *
             *                                                     *
             *******************************************************/
            array(
                'permiso' => 'es_administrador',
                'rol_id' => '1',
            ),
            array(
                'permiso' => 'crear_usuarios',
                'rol_id' => '1',
            ),
            array(
                'permiso' => 'listar_usuarios',
                'rol_id' => '1',
            ),
            array(
                'permiso' => 'modificar_usuarios',
                'rol_id' => '1',
            ),
            array(
                'permiso' => 'eliminar_usuarios',
                'rol_id' => '1',
            ),
            array(
                'permiso' => 'crear_roles',
                'rol_id' => '1',
            ),
            array(
                'permiso' => 'listar_roles',
                'rol_id' => '1',
            ),
            array(
                'permiso' => 'modificar_roles',
                'rol_id' => '1',
            ),
            array(
                'permiso' => 'eliminar_roles',
                'rol_id' => '1',
            ),
            /*******************************************************
             *                                                     *
             *          Permisos del coordinador nacional          *
             *                                                     *
             *******************************************************/
            array(
                'permiso' => 'crear_sedes',
                'rol_id' => '2',
            ),
            array(
                'permiso' => 'listar_sedes',
                'rol_id' => '2',
            ),
            array(
                'permiso' => 'modificar_sedes',
                'rol_id' => '2',
            ),
            array(
                'permiso' => 'eliminar_sedes',
                'rol_id' => '2',
            ),
            array(
                'permiso' => 'crear_encuestas',
                'rol_id' => '2',
            ),
            array(
                'permiso' => 'listar_encuestas',
                'rol_id' => '2',
            ),
            array(
                'permiso' => 'modificar_encuestas',
                'rol_id' => '2',
            ),
            array(
                'permiso' => 'eliminar_encuestas',
                'rol_id' => '2',
            ),
            /*******************************************************
             *                                                     *
             *        Permisos del coordinador de ponencia         *
             *                                                     *
             *******************************************************/
            array(
             'permiso' => 'todas_las_ponencias',
             'rol_id' => '3',
            ),
            array(
             'permiso' => 'show_ponencia',
             'rol_id' => '3',
            ),
            /*******************************************************
             *                                                     *
             *        Permisos del coordinador de registro         *
             *                                                     *
             *******************************************************/
            // array(
            //  'permiso' => 'crear_roles',
            //  'rol_id' => '1',
            // ),
            // array(
            //  'permiso' => 'listar_roles',
            //  'rol_id' => '1',
            // ),
            // array(
            //  'permiso' => 'modificar_roles',
            //  'rol_id' => '1',
            // ),
            // array(
            //  'permiso' => 'eliminar_roles',
            //  'rol_id' => '1',
            // ),
            /*******************************************************
             *                                                     *
             *        Permisos del coordinador de recursos         *
             *                                                     *
             *******************************************************/
            array(
                'permiso' => 'crear_ingresos',
                'rol_id' => '6',
            ),
            array(
                'permiso' => 'listar_ingresos',
                'rol_id' => '6',
            ),
            array(
                'permiso' => 'modificar_ingresos',
                'rol_id' => '6',
            ),
            array(
                'permiso' => 'eliminar_ingresos',
                'rol_id' => '6',
            ),
            array(
                'permiso' => 'crear_gastos',
                'rol_id' => '6',
            ),
            array(
                'permiso' => 'listar_gastos',
                'rol_id' => '6',
            ),
            array(
                'permiso' => 'modificar_gastos',
                'rol_id' => '6',
            ),
            array(
                'permiso' => 'eliminar_gastos',
                'rol_id' => '6',
            ),
            /*******************************************************
             *                                                     *
             *        Permisos del coordinador de logistica        *
             *                                                     *
             *******************************************************/
            // array(
            //  'permiso' => 'crear_roles',
            //  'rol_id' => '1',
            // ),
            // array(
            //  'permiso' => 'listar_roles',
            //  'rol_id' => '1',
            // ),
            // array(
            //  'permiso' => 'modificar_roles',
            //  'rol_id' => '1',
            // ),
            // array(
            //  'permiso' => 'eliminar_roles',
            //  'rol_id' => '1',
            // ),
            /*******************************************************
             *                                                     *
             *                  Usuario autenticado                *
             *                                                     *
             *******************************************************/
            array(
             'permiso' => 'mis_registros',
             'rol_id' => '7',
            ),
            array(
             'permiso' => 'registro_en_linea',
             'rol_id' => '7',
            ),
            array(
             'permiso' => 'edit_mi_registro',
             'rol_id' => '7',
            ),
            array(
             'permiso' => 'delete_mi_registro',
             'rol_id' => '7',
            ),
            array(
             'permiso' => 'mis_ponencias',
             'rol_id' => '7',
            ),
            array(
             'permiso' => 'crear_ponencias',
             'rol_id' => '7',
            ),
            array(
             'permiso' => 'edit_mi_ponencia',
             'rol_id' => '7',
            ),
            array(
             'permiso' => 'postulacion_ponencia_agregar_sede',
             'rol_id' => '7',
            ),
            array(
             'permiso' => 'mi_resumen_curricular',
             'rol_id' => '7',
            ),
        );
 
        DB::table('roles')->insert($roles);
        DB::table('permisos')->insert($permisos);
    }

}