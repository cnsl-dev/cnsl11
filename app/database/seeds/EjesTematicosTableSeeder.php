<?php 

class EjesTematicosTableSeeder extends Seeder {
 
    public function run()
    {
        $ejes_tematicos = array(
        	array( 'descripcion' => 'Conceptos Teóricos de las Tecnologías Libres'),
        	array( 'descripcion' => 'Demostraciones de Programas o Herramientas hechas con Tecnologías Libres'),
        	array( 'descripcion' => 'Desarrollo de Software Libre: herramientas y productos'),
        	array( 'descripcion' => 'Edición y Diseño multimedia con Tecnologías Libres'),
        	array( 'descripcion' => 'Innovación y Propuestas para la usabilidad de Tecnologías Libres en Venezuela'),
        	array( 'descripcion' => 'Modelos de relación ciencia-tecnología-sociedad'),
        	array( 'descripcion' => 'Tecnologías libres en la educación'),
        	array( 'descripcion' => 'Tecnologías libres para el proceso productivo en venezuela'),
        );
 
        DB::table('ejes_tematicos')->insert($ejes_tematicos);
    }
 
}