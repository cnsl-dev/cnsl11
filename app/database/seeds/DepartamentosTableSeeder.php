<?php 


class DepartamentosTableSeeder extends Seeder {
 
    public function run()
    {
        $departamentos = array(
        	array( 'nombre' => 'Amazonas', 'iso_3166-2'=> 'VE-X', 'visible' => TRUE, 'pais_id' => 1),
            array( 'nombre' => 'Anzoátegui', 'iso_3166-2'=> 'VE-B', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Apure', 'iso_3166-2'=> 'VE-C', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Aragua', 'iso_3166-2'=> 'VE-D', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Barinas', 'iso_3166-2'=> 'VE-E', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Bolívar', 'iso_3166-2'=> 'VE-F', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Carabobo', 'iso_3166-2'=> 'VE-G', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Cojedes', 'iso_3166-2'=> 'VE-H', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Delta Amacuro', 'iso_3166-2'=> 'VE-Y', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Falcón', 'iso_3166-2'=> 'VE-I', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Guárico', 'iso_3166-2'=> 'VE-J', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Lara', 'iso_3166-2'=> 'VE-K', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Mérida', 'iso_3166-2'=> 'VE-L', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Miranda', 'iso_3166-2'=> 'VE-M', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Monagas', 'iso_3166-2'=> 'VE-N', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Nueva Esparta', 'iso_3166-2'=> 'VE-O', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Portuguesa', 'iso_3166-2'=> 'VE-P', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Sucre', 'iso_3166-2'=> 'VE-R', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Táchira', 'iso_3166-2'=> 'VE-S', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Trujillo', 'iso_3166-2'=> 'VE-T', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Vargas', 'iso_3166-2'=> 'VE-W', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Yaracuy', 'iso_3166-2'=> 'VE-U', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Zulia', 'iso_3166-2'=> 'VE-V', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Distrito Capital', 'iso_3166-2'=> 'VE-A', 'visible' => TRUE, 'pais_id' => 1 ),
            array( 'nombre' => 'Dependencias Federales', 'iso_3166-2'=> 'VE-Z', 'visible' => TRUE, 'pais_id' => 1 )
        );
 
        DB::table('departamentos')->insert($departamentos);
    }
 
}