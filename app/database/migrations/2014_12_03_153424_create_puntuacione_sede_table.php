<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePuntuacioneSedeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('puntuacion_sede', function(Blueprint $table)
		{
			$table->integer('puntuacion_id')->unsigned()->index();
			$table->integer('sede_id')->unsigned()->index();
			$table->foreign('puntuacion_id')->references('id')->on('puntuaciones')->onDelete('cascade');
			$table->foreign('sede_id')->references('id')->on('sedes')->onDelete('cascade');
			$table->timestamps();
			$table->primary('puntuacion_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('puntuacion_sede');
	}

}
