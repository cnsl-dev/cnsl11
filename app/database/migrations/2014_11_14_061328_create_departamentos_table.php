<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepartamentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('departamentos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre', 128);
			$table->string('iso_3166-2', 16);
			$table->boolean('visible')->default(true);
			$table->integer('pais_id', FALSE, TRUE)->nullable();
			$table->timestamps();
			$table->foreign('pais_id')
				  ->references('id')
				  ->on('paises')
				  ->onUpdate('cascade')
				  ->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('departamentos');
	}

}
