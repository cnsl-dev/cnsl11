<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImpresorasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('impresoras', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('identificador', 128);
			$table->boolean('activa')->default(FALSE);
			$table->integer('sede_id', FALSE, TRUE)->nullable();
			$table->integer('modelo_id', FALSE, TRUE)->nullable();
			$table->timestamps();
			$table->foreign('sede_id')
				->references('id')
				->on('sedes')
				->onUpdate('cascade')
				->onDelete('set null');
			$table->foreign('modelo_id')
				->references('id')
				->on('modelos_impresoras')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('impresoras');
	}

}
