<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('perfiles', function(Blueprint $table)
		{
			$table->integer('usuario_id')->unsigned();
			$table->string('avatar', 256)->nullable();
			$table->date('fecha_nacimiento')->nullable();
			$table->string('telefono', 16)->nullable();
			$table->string('twitter', 64)->nullable();
			$table->integer('ciudad_id')->unsigned()->index()->nullable();
			$table->text('direccion')->nullable();
			$table->string('profesion_u_oficio', 64)->nullable();
			$table->string('organizacion',64)->nullable();
			$table->string('web',64)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->primary('usuario_id');
			$table->foreign('usuario_id')
				  ->references('id')
				  ->on('usuarios')
				  ;
			$table->foreign('ciudad_id')
				  ->references('id')
				  ->on('ciudades')
				  ->onDelete('set null')
				  ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('perfiles');
	}

}
