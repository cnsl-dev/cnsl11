<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivottableRolesusuarios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rol_usuario', function(Blueprint $table) {
			$table->integer('rol_id')->unsigned()->index();
			$table->integer('usuario_id')->unsigned()->index();
			$table->foreign('rol_id')->references('id')->on('roles')->onDelete('cascade');
			$table->foreign('usuario_id')->references('id')->on('usuarios')->onDelete('cascade');
			$table->primary(array('rol_id', 'usuario_id'));
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rol_usuario');
	}

}
