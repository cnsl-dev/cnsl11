<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePonenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ponencias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo_de_la_ponencia', 256);
			$table->text('resumen_de_la_ponencia');
			$table->string('laminas', 256);
			$table->integer('duracion', FALSE, TRUE);
			$table->enum('status', array('Admitida', 'En revision', 'No procede', 'Rechazada'))->default('En revisión');
			$table->integer('usuario_id', FALSE, TRUE)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->foreign('usuario_id')
				->references('id')
				->on('usuarios')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ponencias');
	}

}
