<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermisosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permisos', function(Blueprint $table)
		{
			$table->string('permiso', 50);
			$table->integer('rol_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();
			$table->primary(array('permiso', 'rol_id'));
			$table->foreign('rol_id')
				  ->references('id')
				  ->on('roles')
				  ->onDelete('cascade')
				  ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permisos');
	}

}
