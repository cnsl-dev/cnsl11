<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOpcionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('opciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('descripcion', 256);
			$table->integer('pregunta_id', FALSE, TRUE)->nullable();
			$table->tinyInteger('orden');
			$table->timestamps();
			$table->foreign('pregunta_id')
				->references('id')
				->on('preguntas')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('opciones');
	}

}
