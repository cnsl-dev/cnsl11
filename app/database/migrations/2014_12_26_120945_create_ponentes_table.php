<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePonentesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ponentes', function(Blueprint $table)
		{
			$table->integer('usuario_id')->unsigned();
			$table->text('resumen_curricular');
			$table->timestamps();
			$table->foreign('usuario_id')
				->references('id')
				->on('usuarios')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ponentes');
	}

}
