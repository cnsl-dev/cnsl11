<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrganizadoresRegionalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('organizadores_regionales', function(Blueprint $table)
		{
			$table->integer('usuario_id')->unsigned();
			$table->integer('sede_id', FALSE, TRUE);
			$table->integer('responsabilidad_id', FALSE, TRUE);
			$table->timestamps();
			$table->foreign('usuario_id')
				->references('id')
				->on('usuarios')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreign('sede_id')
				->references('id')
				->on('sedes')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreign('responsabilidad_id')
				->references('id')
				->on('responsabilidades_regionales')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->primary('usuario_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('organizadores_regionales');
	}

}
