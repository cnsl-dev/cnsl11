<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSedesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sedes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('evento_id', FALSE, TRUE)->nullable();
			$table->integer('ciudad_id', FALSE, TRUE)->nullable();
			$table->date('inicio')->nullable();
			$table->date('fin')->nullable();
			$table->date('limite_ponencias')->nullable();
			$table->boolean('activo')->default(TRUE);
			$table->boolean('actual')->default(FALSE);
			$table->timestamps();
			$table->foreign('evento_id')
				->references('id')
				->on('eventos')
				->onUpdate('cascade')
				->onDelete('set null');
			$table->foreign('ciudad_id')
				->references('id')
				->on('ciudades')
				->onUpdate('cascade')
				->onDelete('set null');
			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sedes');
	}

}
