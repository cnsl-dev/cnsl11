<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostulacionesPonenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('postulaciones_ponencias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sede_id', FALSE, TRUE)->index()->nullable();
			$table->integer('ponencia_id', FALSE, TRUE)->index();
			$table->boolean('aceptada')->default(FALSE);
			$table->integer('ciudad_id', FALSE, TRUE)->index()->nullable();
			$table->timestamps();
			$table->foreign('sede_id')
				->references('id')
				->on('sedes')
				->onUpdate('cascade')
				->onDelete('set null');
				$table->foreign('ponencia_id')
				->references('id')
				->on('ponencias')
				->onUpdate('cascade')
				->onDelete('cascade');
				$table->foreign('ciudad_id')
				->references('id')
				->on('ciudades')
				->onUpdate('cascade')
				->onDelete('set null');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('postulaciones_ponencias');
	}

}
