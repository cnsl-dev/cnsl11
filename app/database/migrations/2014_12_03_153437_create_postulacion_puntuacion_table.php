<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostulacionPuntuacionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('postulacion_puntuacion', function(Blueprint $table)
		{
			$table->integer('postulacion_id')->unsigned()->index();
			$table->integer('puntuacion_id')->unsigned()->index();
			$table->foreign('postulacion_id')->references('id')->on('postulaciones_ponencias')->onDelete('cascade');
			$table->foreign('puntuacion_id')->references('id')->on('puntuaciones')->onDelete('cascade');
			$table->timestamps();
			$table->primary('puntuacion_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('postulacion_puntuacion');
	}

}
