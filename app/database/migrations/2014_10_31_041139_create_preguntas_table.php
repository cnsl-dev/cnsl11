<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePreguntasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('preguntas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->enum('seleccion', array('simple', 'multiple'))->default('simple');
			$table->integer('encuesta_id', false, true)->nullable();
			$table->tinyInteger('orden');
			$table->timestamps();
			$table->foreign('encuesta_id')
				->references('id')
				->on('encuestas')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('preguntas');
	}

}
