<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCertificadoIngresoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('certificado_ingreso', function(Blueprint $table)
		{
			$table->integer('certificado_id')->unsigned()->index();
			$table->integer('ingreso_id')->unsigned()->index();
			$table->foreign('certificado_id')->references('registro_id')->on('certificados')->onDelete('cascade');
			$table->foreign('ingreso_id')->references('id')->on('ingresos')->onDelete('cascade');
			$table->timestamps();
			$table->primary(array('certificado_id', 'ingreso_id'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('certificado_ingreso');
	}

}
