<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDonacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('donaciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('descripcion');
			$table->integer('patrocinante_id', FALSE, TRUE)->nullable();
			$table->timestamps();
			$table->foreign('patrocinante_id')
				->references('id')
				->on('patrocinantes')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('donaciones');
	}

}
