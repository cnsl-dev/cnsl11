<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRazonesExoneracionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('razones_exoneraciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo', 64);
			$table->string('certificado_masculino',32);
			$table->string('certificado_femenino',32);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('razones_exoneraciones');
	}

}
