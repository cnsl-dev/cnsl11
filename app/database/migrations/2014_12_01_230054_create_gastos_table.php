<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGastosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gastos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->enum('tipo', array('Regional', 'Nacional', 'Compartido'));
			$table->decimal('monto',8,2);
			$table->integer('sede_id', FALSE, TRUE)->nullable();
			$table->timestamps();
			$table->foreign('sede_id')
				->references('id')
				->on('sedes')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gastos');
	}

}
