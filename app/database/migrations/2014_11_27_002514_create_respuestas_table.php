<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRespuestasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('respuestas', function(Blueprint $table)
		{
			$table->integer('evento_id')->unsigned()->index();
			$table->integer('opcion_id')->unsigned()->index();
			$table->integer('usuario_id')->unsigned()->index();
			$table->foreign('evento_id')->references('id')->on('eventos')->onDelete('cascade');
			$table->foreign('opcion_id')->references('id')->on('opciones')->onDelete('cascade');
			$table->foreign('usuario_id')->references('id')->on('usuarios')->onDelete('cascade');
			$table->timestamps();
			$table->primary(array('evento_id', 'opcion_id', 'usuario_id'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('respuestas');
	}

}
