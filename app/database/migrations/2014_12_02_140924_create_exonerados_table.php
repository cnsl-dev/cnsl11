<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExoneradosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exonerados', function(Blueprint $table)
		{
			$table->integer('registro_id')->unsigned();
			$table->integer('razon_id')->unsigned()->index()->nullable();
			$table->boolean('impreso')->default(FALSE);
			$table->boolean('entregado')->default(FALSE);
			$table->timestamps();
			$table->primary('registro_id');
			$table->foreign('registro_id')
				->references('id')
				->on('registros')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreign('razon_id')
				->references('id')
				->on('razones_exoneraciones')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exonerados');
	}

}
