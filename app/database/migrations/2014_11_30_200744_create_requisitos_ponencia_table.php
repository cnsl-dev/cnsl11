<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRequisitosPonenciaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('requisitos_ponencia', function(Blueprint $table)
		{
			$table->integer('ponencia_id', TRUE, TRUE);
			$table->text('hardware')->nullable();
			$table->text('software')->nullable();
			$table->boolean('conectividad')->default(FALSE);
			$table->boolean('proximidad')->default(FALSE);
			$table->text('otros')->nullable();
			$table->timestamps();
			$table->foreign('ponencia_id')
				->references('id')
				->on('ponencias')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('requisitos_ponencia');
	}

}
