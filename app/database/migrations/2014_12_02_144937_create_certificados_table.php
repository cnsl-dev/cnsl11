<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCertificadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('certificados', function(Blueprint $table)
		{
			$table->integer('registro_id')->unsigned();
			$table->enum('tipo', array('Estudiante', 'Profesional'));
			$table->boolean('impreso')->default(FALSE);
			$table->boolean('entregado')->default(FALSE);
			$table->timestamps();
			$table->primary('registro_id');
			$table->foreign('registro_id')
				->references('id')
				->on('registros')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('certificados');
	}

}
