<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEjeTematicoPonenciaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eje_tematico_ponencia', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('eje_tematico_id')->unsigned()->index();
			$table->foreign('eje_tematico_id')->references('id')->on('ejes_tematicos')->onDelete('cascade');
			$table->integer('ponencia_id')->unsigned()->index();
			$table->foreign('ponencia_id')->references('id')->on('ponencias')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eje_tematico_ponencia');
	}

}
