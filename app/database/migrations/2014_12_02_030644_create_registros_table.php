<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegistrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registros', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('usuario_id', FALSE, TRUE);
			$table->integer('sede_id', FALSE, TRUE)->nullable();
			$table->datetime('fecha_registro');
			$table->datetime('fecha_validacion')->nullable();
			$table->datetime('fecha_modificacion')->nullable();
			$table->foreign('usuario_id')
				->references('id')
				->on('usuarios')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreign('sede_id')
				->references('id')
				->on('sedes')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registros');
	}

}
