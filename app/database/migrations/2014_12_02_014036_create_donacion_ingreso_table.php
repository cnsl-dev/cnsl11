<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDonacionIngresoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('donacion_ingreso', function(Blueprint $table)
		{
			$table->integer('donacion_id')->unsigned()->index();
			$table->foreign('donacion_id')->references('id')->on('donaciones')->onDelete('cascade');
			$table->integer('ingreso_id')->unsigned()->index();
			$table->foreign('ingreso_id')->references('id')->on('ingresos')->onDelete('cascade');
			$table->primary('donacion_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('donacion_ingreso');
	}

}
