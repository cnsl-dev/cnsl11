<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlantillasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plantillas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('descripcion',32)->nullable();
			$table->boolean('activa')->default(FALSE);
			$table->smallInteger('superior');
			$table->smallInteger('izquierdo');
			$table->smallInteger('tipo_x');
			$table->smallInteger('tipo_y');
			$table->smallInteger('ciudad_x');
			$table->smallInteger('ciudad_y');
			$table->smallInteger('fecha_x');
			$table->smallInteger('fecha_y');
			$table->smallInteger('organizador_x');
			$table->smallInteger('organizador_y');
			$table->integer('impresora_id', FALSE, TRUE)->nullable();
			$table->timestamps();
			$table->foreign('impresora_id')
				->references('id')
				->on('impresoras')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plantillas');
	}

}
