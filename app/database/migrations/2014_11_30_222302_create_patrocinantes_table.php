<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePatrocinantesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patrocinantes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre',128);
			$table->boolean('nacional')->default(FALSE);
			$table->integer('sede_id', false, true)->nullable();
			$table->timestamps();
			$table->foreign('sede_id')
				->references('id')
				->on('sedes')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patrocinantes');
	}

}
