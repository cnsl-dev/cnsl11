<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEncuestaEventoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('encuesta_evento', function(Blueprint $table)
		{
			$table->integer('encuesta_id')->unsigned()->index();
			$table->integer('evento_id')->unsigned()->index();
			$table->foreign('encuesta_id')->references('id')->on('encuestas')->onDelete('cascade');
			$table->foreign('evento_id')->references('id')->on('eventos')->onDelete('cascade');
			$table->primary(array('encuesta_id', 'evento_id'));
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('encuesta_evento');
	}

}
