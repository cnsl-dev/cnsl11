<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMensajesPonenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mensajes_ponencias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('mensaje');
			$table->integer('ponencia_id', FALSE, TRUE)->index();
			$table->integer('emisor', FALSE, TRUE)->index()->nullable();
			$table->integer('receptor', FALSE, TRUE)->index()->nullable();
			$table->integer('respuesta', FALSE, TRUE)->index()->nullable();
			$table->boolean('leido')->default(FALSE);
			$table->datetime('enviado');
			$table->foreign('ponencia_id')
				->references('id')
				->on('ponencias')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreign('emisor')
				->references('id')
				->on('usuarios')
				->onUpdate('cascade')
				->onDelete('set null');
			$table->foreign('receptor')
				->references('id')
				->on('usuarios')
				->onUpdate('cascade')
				->onDelete('set null');
			$table->foreign('respuesta')
				->references('id')
				->on('mensajes_ponencias')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mensajes_ponencias');
	}

}
