<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCiudadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ciudades', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre', 128);
			$table->boolean('capital');
			$table->boolean('visible')->default(TRUE);
			$table->integer('departamento_id', FALSE, TRUE)->nullable();
			$table->timestamps();
			$table->foreign('departamento_id')
				->references('id')
				->on('departamentos')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ciudades');
	}

}
