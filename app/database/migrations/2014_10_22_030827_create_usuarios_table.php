<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email',100)->unique()->nullable();
			$table->string('nombres', 128);
			$table->string('apellidos', 128);
			$table->string('dni', 24)->unique();
			$table->enum('sexo', array('Masculino', 'Femenino'))->nullable();
			$table->string('password',128)->nullable();
			// campos de configuración
			$table->boolean('registrado_en_linea')->default(TRUE);
			$table->boolean('por_confirmar_en_linea')->default(FALSE);
			$table->boolean('confirmado')->default(FALSE);
			$table->boolean('activo')->default(TRUE);
			$table->boolean('baneado')->default(FALSE);
			$table->string('confirmation_code',100)->nullable();
			$table->string('remember_token',100)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}

}
