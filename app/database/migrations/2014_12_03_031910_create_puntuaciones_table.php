<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePuntuacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('puntuaciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->tinyInteger('puntuacion');
			$table->string('ip',64);
			$table->integer('usuario_id', FALSE, TRUE)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('puntuaciones');
	}

}
