<?php 

use Mockery as m;

/**
* 
*/
class RolesControllerTest extends TestCase
{
	
	function __construct()
	{
		$this->collection = m::mock('Illuminate\Database\Eloquent\Collection')->shouldDeferMissing();
	}

	public function setUp(){
		
		parent::setUP();

		$this->rol = m::mock('Rol');
		$this->permiso = m::mock('Permiso');
		$this->input = array('nombre' => 'rol', 'descripcion' => 'un rol', 'permisos' => ['crear_encuestas', 'editar_encuestas', 'eliminar_encuestas']);

		$this->app->instance('Rol', $this->rol);
		$this->app->instance('Permiso', $this->permiso);

	}

	public function tearDown(){
		
		parent::tearDown();

		m::close();
	}

	public function testIndex(){
		$this->rol->shouldReceive('all')->once()->andReturn($this->collection);

		$this->call('GET', 'admin/roles');

		$this->assertViewHas('listObjs');
	}

	public function testCreate(){
		$this->rol->shouldReceive('getAllColumnsNames')->once()->andReturn(array());

		$this->call('GET', 'admin/roles/create');

		$this->assertResponseOk();
	}

	public function testStore(){

		$this->rol->shouldReceive('create')->once()->andReturn(new Rol());
		$this->permiso->shouldReceive('create')->times(3);
		
		$this->validate(true);
		$this->call('POST', 'admin/roles', $this->input);

		$this->assertRedirectedToRoute('admin.roles.index');
	}

	public function testStoreFail(){
		
		$this->validate(false);
		$this->call('POST', 'admin/roles');

		$this->assertRedirectedToRoute('admin.roles.create');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testShow(){

		$this->rol->shouldReceive('findOrFail')
				   ->with(1)
				   ->once()
				   ->andReturn(new Rol());

		$this->call('GET', 'admin/roles/1');

		$this->assertViewHas('obj');
	}

	public function testEdit(){

		$this->rol->shouldReceive('find')
				   ->with(1)
				   ->andReturn(new Rol());

		$this->call('GET', 'admin/roles/1/edit');

		$this->assertViewHas('obj');
	}

	public function testUpdate(){

		$rol = m::mock(new Rol(['id' => 1]));

		$this->rol->shouldReceive('find')
				   ->with(1)
				   ->andReturn($rol);

		$this->permiso->shouldReceive('where')
						->once()
						->andReturn(m::mock(['delete' => true]));

		$this->permiso->shouldReceive('create')->times(3);

		$rol->shouldReceive('update')
					->once();

		$this->validate(true);

		$this->call('PATCH', 'admin/roles/1', $this->input);

		$this->assertRedirectedTo('/admin/roles/1');

	}

	public function testUpdateFail(){
		
		$this->validate(false);

		$this->call('PATCH', 'admin/roles/1');

		$this->assertRedirectedTo('admin/roles/1/edit');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}


	public function testDestroy(){
		

		$this->rol->shouldReceive('find')->with(1)->andReturn(m::mock(['delete' => true]));

		$this->call('DELETE', 'admin/roles/1');
		
		$this->assertRedirectedToRoute('admin.roles.index');
	}

	public function testGetVarContextCreate(){
		//$this->rol->shouldReceive('getAllColumnsNames')->once();
	}

	protected function validate($bool)
	{
		Validator::shouldReceive('make')
				->once()
				->andReturn(m::mock(array('passes' => $bool)));
	}
}