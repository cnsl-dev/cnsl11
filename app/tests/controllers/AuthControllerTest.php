<?php 

use Mockery as m;

/**
* Class test UserController
*/
class AuthControllerTest extends TestCase
{
	public function setUp(){
		
		parent::setUP();

		$this->mock = m::mock('Eloquent', 'Usuario');
		$this->app->instance('Usuario', $this->mock);

	}

	public function tearDown(){
		
		parent::tearDown();

		m::close();
	}

	public function testLogin(){

		$this->call('GET', 'login');

		$this->assertResponseOk();
	}
}