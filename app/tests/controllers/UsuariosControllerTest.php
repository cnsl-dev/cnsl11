<?php 

use Mockery as m;

/**
* Class test UsuariosController
*/
class UsuariosControllerTest extends TestCase
{
	
	public function __construct()
	{
		$this->collection = m::mock('Illuminate\Database\Eloquent\Collection')->shouldDeferMissing();
	}

	public function setUp(){
		
		parent::setUP();

		$this->usuario = m::mock('Eloquent', 'Usuario');
		$this->rol = m::mock('Rol');

		$this->app->instance('Usuario', $this->usuario);
		$this->app->instance('Rol', $this->rol);

	}

	public function tearDown(){
		
		parent::tearDown();

		m::close();
	}


	public function testIndex(){
		$this->usuario->shouldReceive('all')->once()->andReturn($this->collection);

		$this->call('GET', 'admin/usuarios');

		$this->assertViewHas('usuarios');
	}

	public function testCreate(){

		$this->rol->shouldReceive('lists')->with('nombre', 'id')->andReturn($this->collection);

		$this->call('GET', 'admin/usuarios/create');

		$this->assertViewHas('roles');

		$this->assertResponseOk();
	}

	public function testStore(){

		$this->usuario->shouldReceive('create')->once();
		
		$this->validate(true);
		$this->call('POST', 'admin/usuarios', array('username' => 'nombre', 'email' => 'correo', 'password' => '123456', 'password-repeat' => '123456'));

		$this->assertRedirectedToRoute('admin.usuarios.index');
	}

	public function testStoreFail(){
		
		$this->validate(false);
		$this->call('POST', 'admin/usuarios');

		$this->assertRedirectedToRoute('admin.usuarios.create');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testShow(){

		$this->usuario->shouldReceive('find')
				   ->with(1)
				   ->once()
				   ->andReturn(new Usuario());

		$this->call('GET', 'admin/usuarios/1');

		$this->assertViewHas('usuario');
	}

	public function testEdit(){

		$this->rol->shouldReceive('lists')->once('nombre', 'id')->andReturn($this->collection);

		$this->usuario->shouldReceive('find')
				   ->with(1)
				   ->andReturn(new Usuario());

		$this->call('GET', 'admin/usuarios/1/edit');

		$this->assertViewHas('roles');

		$this->assertViewHas('usuario');
	}


	public function testUpdate(){

		$usuario = m::mock(new Usuario());

		$this->usuario->shouldReceive('find')
				   ->with(1)
				   ->andReturn($usuario);

		$usuario->shouldReceive('update')
					->once();

		$this->validate(true);

		$this->call('PATCH', 'admin/usuarios/1', array('username' => 'nombre', 'email' => 'correo', 'password' => '123456', 'password-repeat' => '123456'));

		$this->assertRedirectedToRoute('admin.usuarios.index');

	}


	public function testUpdateFail(){
		
		$this->validate(false);

		$this->call('PATCH', 'admin/usuarios/1');

		$this->assertRedirectedTo('admin/usuarios/1/edit');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}


	public function testDestroy(){
		

		$this->usuario->shouldReceive('find')->with(1)->andReturn(m::mock(['delete' => true]));

		$this->call('DELETE', 'admin/usuarios/1');
		
		$this->assertRedirectedToRoute('admin.usuarios.index');
	}

	protected function validate($bool)
	{
		Validator::shouldReceive('make')
				->once()
				->andReturn(m::mock(array('passes' => $bool)));
	}

	// public function mock($class)
	// {
	//     $mock = Mockery::mock($class);

	//     $this->app->instance($class, $mock);

	//     return $mock;
	// }
}
