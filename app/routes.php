<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::controller('authenticate', 'LoginController', array(
	'getLogin' => 'login',
	'postLogin' => 'login.post',
	'getLogout' => 'logout',
));

Route::controller('signup', 'SignUpController',array(
	'getFindCedula' => 'find_cedula',
	'postFindCedula' => 'find_cedula',
	'getCreateAccount' => 'create_account',
	'postCreateAccount' => 'create_account',
	'getCofirmEmail' => 'confirm_email',
	'getResendEmailCreateAccount' => 'resend_verify',
	'postResendEmailCreateAccount' => 'resend_verify.post',
));

Route::controller('account', 'AccountController', array(
	'getMyAccount' => 'my_account',
	'getEditAccount' => 'edit_account',
	'postEditEmail' => 'edit_email.post',
	'postEditPassword' => 'edit_password.post',
	'getEditProfile' => 'edit_profile',
	'postEditProfile' => 'edit_profile.post',
	'getAvatar' => 'avatar',
));

Route::group(array('before' => 'guest'), function(){

	Route::controller('password', 'RemindersController', array(
		'getRemind' => 'remind',
	));
});

Route::group(array('before' => 'auth'), function(){

	Route::get('admin', function(){
		return View::make('layouts.admin');
	});

});

/**
 *	Componentes de la aplicación
 */

Route::group(array('prefix' => 'auth'), function(){
	raiz();
	Route::get('usuarios/find/{id}','UsuariosController@find');
	Route::resource('usuarios', 'UsuariosController');
	Route::resource('roles', 'RolesController');
});


Route::group(array('prefix' => 'regionalizacion'), function(){
	raiz();
	Route::resource('paises', 'PaisesController');
	Route::get('departamentos/{id}/ciudades','DepartamentosController@find');
	Route::resource('departamentos', 'DepartamentosController');
	Route::resource('ciudades', 'CiudadesController');
});

Route::group(array('prefix' => 'encuesta'), function(){
	raiz();
	Route::resource('encuestas', 'EncuestasController');
	Route::resource('preguntas', 'PreguntasController');
	Route::resource('opciones', 'OpcionesController');
	Route::controller('respuestas', 'RespuestasController', array(
			'getResponder' => 'encuesta.respuestas.responder',
			'postResponder' => 'encuesta.respuestas.responder.post',
			'getMisRespuestas' => 'encuesta.respuestas.mis_respuestas'
		));

});

Route::group(array('prefix' => 'evento', 'before' => 'evento_activo'), function(){
	raiz();
	Route::resource('eventos', 'EventosController');
	Route::resource('sedes', 'SedesController');
	Route::controller('organizador', 'OrganizadoresController', array(
		'getSede' => 'organizador.sede',
		'postSede' => 'organizador.sede.post',
		'deleteUnlink' => 'organizador.sede.delete'
	));
	Route::resource('patrocinantes', 'PatrocinantesController');
	Route::resource('donaciones', 'DonacionesController');
	Route::controller('registros', 'RegistrosController', array(
		'getMisRegistros' => 'registros.mis_registros',
		'getRegistrarse' => 'registros.registro_en_linea',
		'postRegistrarse' => 'registros.registro_en_linea',
		'getEditMiRegistro' => 'registros.mi_registro.edit',
		'patchEditMiRegistro' => 'registros.mi_registro.edit',
		'deleteMiRegistro' => 'registros.mi_registro.delete',
		'getRegistradosEnMiSede' => 'registros.registros_en_mi_sede',
		'getTodosLosRegistros' => 'registros.todos_los_registros',
		'getRegistradosPorSede' => 'registros.registros_por_sede',
		'getPuerta' => 'registros.puerta',
		'postPuerta' => 'registros.puerta',
		'getValidar' => 'registros.validar',
		'postValidar' => 'registros.validar',
	));
	Route::resource('exonerados', 'ExoneradosController');
	Route::resource('certificados', 'CertificadosController');
	Route::controller('ponentes', 'PonentesController', array(
		'getCreate' => 'ponentes.create',
		'postStore' => 'ponentes.store',
		'getEdit'	=> 'ponentes.edit',
		'patchUpdate' => 'ponentes.update'
	));
	Route::controller('ponencias', 'PonenciasController', array(
		'getIndex' => 'ponencias',
		'getFiltros' => 'ponencias.filtros',
		'getCreate' => 'ponencias.create',
		'postStore' => 'ponencias.store',
		'getShow'	=> 'ponencias.show',
		'getLaminas' => 'ponencias.laminas',
		'getMisLaminas' => 'ponencias.mis_laminas',
		'getMisPonencias' => 'ponencias.mis_ponencias',
		'getEditMiPonencia' => 'ponencias.mi_ponencia.edit',
		'patchUpdateMiPonencia' => 'ponencias.mi_ponencia.update',
		'deleteMiPonencia' => 'ponencias.mi_ponencia.delete',
		'patchModificarStatus' => 'ponencias.status.update'
	));
	Route::controller('postulaciones/ponencias', 'PostulacionesPonenciasController', array(
		'getAgregarSede' => 'postulaciones-ponencias.agregar-sede',
		'postAgregarSede' => 'postulaciones-ponencias.agregar-sede.post',
		'getLaminasCompletas' => 'postulaciones-ponencias.laminas-completas'
	));
	Route::controller('mensajes/ponencias', 'MensajesPonenciasController',array(
		'getList' => 'mensajes.ponencias.list',
		'postStore' => 'mensajes.ponencias.store',
		'getListOwn' => 'mensajes.ponencias.list_own',
		'postStoreOwn' => 'mensajes.ponencias.store_own',
		'getShow' => 'mensajes.ponencias.show',
		'getShowOwn' => 'mensajes.ponencias.show_own',
    ));
	Route::resource('responsabilidades-regionales', 'ResponsabilidadesRegionalesController');
	Route::resource('organizadores-regionales', 'OrganizadoresRegionalesController');
	Route::resource('modelos-impresoras', 'ModelosImpresorasController');
	Route::resource('impresoras', 'ImpresorasController');
	Route::resource('plantillas', 'PlantillasController');
	Route::resource('razones-exoneracion', 'RazonesExoneracionesController');
	Route::resource('ejes-tematicos', 'EjesTematicosController');

	Route::controller('comprobantes', 'ComprobantesController', array(
			'getRegistroBarcode' => 'comprobantes.registros',
			'getParaRegistro' => 'comprobantes.para_registro',
	));
});


Route::group(array('prefix' => 'contabilidad'), function(){
	raiz();
	Route::resource('ingresos', 'IngresosController');
	Route::resource('gastos', 'GastosController');
});

Route::group(array('prefix' => 'votacion'), function(){
	raiz();
	Route::resource('puntuaciones', 'PuntuacionesController');
});



function raiz($filtro='auth'){
	Route::get('/', array('before' => $filtro , function(){
		return View::make('layouts.admin');
	}));
}