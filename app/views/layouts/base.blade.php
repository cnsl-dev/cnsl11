<!doctype html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Congreso Nacional de Software Libre" >
		<meta name="keywords" content="Software Libre, Técnologias Libres, GNU, Linux" >
		<meta name="author" content="Hernán Aguilera" >
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="/imgs/cnsl_favicon.ico">
		<title>
			@yield('title')
		</title>
		<link rel="stylesheet" type="text/css" href="/css/normalize.css">
		<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/css/chosen/chosen.css">
		<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		@yield('more_css')
		<link rel="stylesheet" type="text/css" href="/css/style.css">
		<script src="/js/lib/modernizr.js"></script>
	</head>
	<body>
		@yield('before_body')
			<div class="main-menu">
				<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
					<div class="container">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
							<a style="padding: 2px 6px" class="navbar-brand" href="#">
									<!-- <img
									src="/imgs/cnsl11_50x50.png" 
									alt="CNSL11"
									> -->
							</a>
						</div>
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<!-- Left Nav Section -->
							<ul class="nav navbar-nav">
							  <li><a href="/">Inicio</a></li>
							  @yield('menu_principal')
							</ul>
							<!-- Right Nav Section -->
							@if(Auth::check())
								<ul class="nav navbar-nav navbar-right">
									<li>
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">
											{{ Auth::user()->nombres }}
										<span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu">
											<li><a href="{{ route('my_account') }}">Mi cuenta</a></li>
											<li><a href="{{ route('logout') }}">Salir</a></li>
										</ul>
									</li>
								</ul>
							@else
								<ul class="nav navbar-nav navbar-right">
									<li >
										<a href="{{route('login')}}">
											Ingresar
										</a>
									</li>
									<li>
										<a href="{{route('find_cedula')}}">
											Registrate
										</a>
									</li>
								</ul>
							@endif
						</div>
					</div>
				</nav>
			</div>
		<div class="container">
			@yield('base')
		</div>
		<div class="container">
			<footer>
			  <p class="text-muted"><i class="fa fa-github"></i> <a target="__blank" href="https://github.com/HernanAguilera/Registro-CNSL">Source code</a></p>
			</footer> 
		</div>
		<script type="text/javascript" src="/js/lib/jquery-1.11.1.min.js"></script>
		<script src="/js/lib/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/init.js"></script>
		<script type="text/javascript" src="/js/lib/chosen.jquery.min.js"></script>
		<script type="text/javascript" src="/js/lib/jquery.datetimepicker.js"></script>
		<script type="text/javascript" src="/js/lib/bootstrap-checkbox.min.js"></script>
		<script type="text/javascript" src="/js/widgets/select.widget.js"></script>
		<script type="text/javascript" src="/js/widgets/check.widget.js"></script>
		<script type="text/javascript" src="/js/widgets/date.widget.js"></script>
		@yield('more_js')
		<script type="text/javascript" src="/js/ready.js"></script>
	</body>
</html>