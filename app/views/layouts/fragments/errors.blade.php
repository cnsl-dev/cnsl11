@if (Session::has('message'))
	<div class="{{ Session::has('class') ? 'alert alert-'.Session::get('class') : 'flash alert' }}">
		<p>{{ Session::get('message') }}</p>
		@if ($errors->any())
			<ul>
				{{ implode('', $errors->all('<li class="error">:message</li>')) }}
			</ul>
		@endif
	</div>
@endif