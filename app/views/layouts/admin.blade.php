@extends('layouts.base')

@section('menu_principal')
    @if(Session::has('menu'))
	@foreach(json_decode(Session::get('menu', array()), true) as $item)
	    <li
		@if(Request::is(str_replace('/', '', $item['menu'])) || Request::is(str_replace('/', '', $item['menu']).'/*'))
		    class = 'active'
		@endif
	    >
		<a href="{{$item['menu']}}">{{$item['nombre']}}</a>
	    </li>
	@endforeach
    @endif
@stop

@section('base')
    <div class="row">
	<div class="col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Menú</h3>
			</div>
		    <div class="list-group panel-body">
				@if(Session::has('menu'))
				    @foreach(json_decode(Session::get('menu', array()), true) as $item)
						@if(Request::is(str_replace('/', '', $item['menu'])) || Request::is(str_replace('/', '', $item['menu']).'/*'))
						    @foreach($item['sub-menu'] as $submenu)
						    	@if(Request::is(substr($item['menu'],1).$submenu['admin-menu']))
						    		<a class="list-group-item active" href="{{ $item['menu'].$submenu['admin-menu'] }}">
										{{ $submenu['nombre'] }}
									</a>
						    	@else
						    		<a class="list-group-item" href="{{ $item['menu'].$submenu['admin-menu'] }}">
										{{ $submenu['nombre'] }}
									</a>
						    	@endif
						    @endforeach
						@endif
				    @endforeach
				@endif
				@yield('menu_secundario')
		    </div>
		</div>
	</div>
	<div class="col-md-9">	
	   	@include('layouts.fragments.errors')
	    @section('main')
			@if(Session::has('menu'))
			    @foreach(json_decode(Session::get('menu', array()), true) as $item)
					@if(Request::is(str_replace('/', '', $item['menu'])) || Request::is(str_replace('/', '', $item['menu']).'/*'))
					    @foreach($item['sub-menu'] as $submenu)
							<a class="btn btn-primary btn-lg" style="margin-bottom:.5em" href="{{ $item['menu'].$submenu['admin-menu'] }}">
								{{ $submenu['nombre'] }}
							</a>
					    @endforeach
					@endif
			    @endforeach
			@endif
	    @stop
	    @yield('main')
	</div>
    </div>

@stop