@extends('layouts.admin')

@section('base')

<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Ingresar cédula</h3>
			</div>
			<div class="panel-body">
				{{Form::open(array('route' => 'registros.puerta'))}}
					{{Form::label('dni', 'Cedula:')}}
					<div class="row">
						<div class="col-md-3">
							{{Form::select('nacionalidad', array('V'=>'V','E'=>'E','P'=>'P'), NULL,array('class' => 'form-control'))}}
						</div>
						<div class="col-md-9">
							{{Form::text('dni', NULL, array('class' => 'form-control'))}}
						</div>
					</div>
					<div class="buttons-action text-center">
						{{Form::submit('Enviar', array('class' => 'btn btn-primary'))}}
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>

@stop