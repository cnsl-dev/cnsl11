@extends('layouts.admin')

@section('main')

	<h1>Todos los registrados</h1>
	@if($registros->count()>0)
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Certificado</th>
					<th>Sede</th>
				</tr>
			</thead>

			<tbody>
				@foreach($registros as $registro)
					<tr>
						<td>{{$registro->usuario->nombres}}</td>
						<td>{{$registro->usuario->apellidos}}</td>
						<td>{{$registro->certificado ? $registro->certificado->tipo : 'No'}}</td>
						<td>{{$registro->sede->ciudad->nombre}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>

	@else
		<p>No hay registros aun.</p>
	@endif

@stop