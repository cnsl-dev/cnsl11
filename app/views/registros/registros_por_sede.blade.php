@extends('layouts.admin')

@section('main')

	<h1>Todos los registrados</h1>
	@if($registros->count()>0)
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>Sede</th>
					<th>Regitrados</th>
				</tr>
			</thead>

			<tbody>
				@foreach($registros as $registro)
					<tr>
						<td>{{$registro->ciudad}}</td>
						<td>{{$registro->registrados}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>

	@else
		<p>No hay registros aun.</p>
	@endif

@stop