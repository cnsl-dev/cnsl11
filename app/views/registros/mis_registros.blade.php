@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="{{ asset('js/lib/bootstrap-dialog.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/modules/registros/delete.js') }}"></script>
@stop

@section('main')
<h2>Has confirmado asistencia para las siguientes sedes:</h2>
<div class="row">
	<div class="col-md-12">
		@if($registros->count()>0)
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>Sede</th>
						<th>Certificado</th>
						<th>Comprobante</th>
						<th width="100"></th>
					</tr>
				</thead>
				<tbody>
					@foreach($registros as $registro)
						<tr>
							<td>{{$registro->sede->ciudad->nombre}}</td>
							<td>{{$registro->certificado ? $registro->certificado->tipo : 'No'}}</td>
							<td>
								<a href="{{route('comprobantes.para_registro', $registro->id)}}" target="__blank">Imprimir</a>
							</td>
							<td>
								@if($registro->sede->activo)
									<a class="btn btn-primary" href="{{route('registros.mi_registro.edit', $registro->id)}}">
										<i class="fa fa-pencil-square-o"></i>
									</a>
			                    	<button 
			                    		class="btn btn-danger deleteConfirm" 
			                    		data-route="{{route('registros.mi_registro.delete')}}" 
			                    		data-id="{{$registro->sede->id}}"
			                    		data-token="{{csrf_token()}}"
			                    	>
									    <i class="fa fa-trash-o"></i>
									</button>
								@else
									Inactiva
								@endif
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		@else
			<p>No se ha registrado para ninguna sede aún</p>
		@endif
	</div>
	<a href="{{route('registros.registro_en_linea')}}" class="btn btn-danger btn-lg">Registrarse para una nueva sede</a>
</div>

@stop