@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/modules/registros/registrarse.js"></script>
@stop

@section('main')
	<div class="row">
		<div class="alert alert-success" role="alert">
			<p>El certificado del CNSL es totalmente <strong>OPCIONAL</strong> y nuestro evento ha sido y será siempre un evento de Entrada Libre y <strong>GRATUITA</strong>. En el Congreso Nacional de Software Libre <strong>no restringimos el acceso y por lo tanto no exigimos el pago de un certificado</strong>, ni cualquier otro requisito que la voluntad de aprender y participar.</p>
			<p>Si usted desea un certificado de participación en el evento pediremos a cambio <strong>una colaboración de Bs.F {{ Config::get('certificado')['Profesional']['monto'] }}</strong> (o <strong>Bs.F {{ Config::get('certificado')['Estudiante']['monto'] }} si es estudiante</strong>)</p>
		</div>
	</div>
	{{Form::open($parametros_formulario)}}
	<div class="col-md-12">
		<div class="panel panel-default">
		  <div class="panel-heading">
		  	<h2>Registrarse para una sede del CNSL</h2>
		  </div>
		  <div class="panel-body">
		    {{Form::label('sede_id', 'Sede:')}}
			{{Form::select('sede_id', $sedes, $registro->sede_id, array('id' => 'sede', 'class' => 'form-control'))}}
			<div id="data-certificado">
				{{Form::label('certificado', '¿Desea recibir un certificado de participación para esta sede?')}}
				{{ Form::label('si-certificado', 'Si', array('class' => 'radio-inline')) }}
				{{ Form::radio('certificado', '1', $certificado, array('id' => 'si-certificado', 'class' => 'radio-inline')) }}
				{{ Form::label('no-certificado', 'No', array('class' => 'radio-inline')) }}
				{{ Form::radio('certificado', '0', !$certificado, array('id' => 'no-certificado', 'class' => 'radio-inline')) }}
			  	<div id="desea-certificado">
			  		<div class="alert alert-info" role="alert">
				  		<p>La colaboración para recibir el certificado puede realizarse el día del evento o, si prefiere no hacer colas, desde un <strong>depósito bancario o transferencia</strong> a la <strong>Cuenta Corriente</strong> del <strong>Banco Mercantil</strong> número <strong>0105-0722-79-1722054913</strong> a nombre de: <strong>Proyecto GNU A.C. (RIF J-29752180-1)</strong>.</p>
				  	</div>
				  	<div id="contenedor-tipo" style="margin-top:.5em" class="col-md-6">
						{{ Form::label('', 'Tipo:')}}
						{{ Form::label('estudiante', 'Estudiante:', array('class' => 'radio-inline')) }}
						{{ Form::radio('tipo_certificado', 'Estudiante', $certificado && $certificado->tipo=='Estudiante', array('id' => 'estudiante', 'class' => 'radio-inline')) }}
						{{ Form::label('profesional', 'Profesional', array('class' => 'radio-inline')) }}
						{{ Form::radio('tipo_certificado', 'Profesional', $certificado && $certificado->tipo=='Profesional', array('id' => 'profesional', 'class' => 'radio-inline')) }}
					</div>
					<div id="contenedor-pago" class="col-md-6">
						{{ Form::label('', 'Pago:', array('style' => 'display:block'))}}
						{{ Form::label('deposito', 'Deposito', array('class' => 'radio-inline')) }}
						{{ Form::radio('tipo_pago', 'Deposito', $ingreso && $ingreso->tipo=='Deposito', array('id' => 'deposito', 'class' => 'radio-inline')) }}
						{{ Form::label('transferencia', 'Transferencia:', array('class' => 'radio-inline')) }}
						{{ Form::radio('tipo_pago', 'Transferencia', $ingreso && $ingreso->tipo=='Transferencia', array('id' => 'transferencia', 'class' => 'radio-inline')) }}
						{{ Form::text('codigo_operacion', $ingreso ? $ingreso->codigo_operacion : '', array('id' => 'codigo_operacion', 'class' => 'form-control', 'placeholder' => 'Coloque acá el código de la operación...')) }}
					</div>
			  	</div>
			</div>
		  </div>
		</div>
		<div class="col-md-12">
			{{Form::submit('Registrarse', array('id' => 'registrarse', 'class' => 'btn btn-primary'))}}
			<a href="{{route('registros.mis_registros')}}" class="btn btn-default">Cancelar</a>
		</div>
	</div>
	{{Form::close()}}
@stop