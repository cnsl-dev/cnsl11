@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/modules/registros/puerta.js"></script>
@stop

@section('base')

<?php $op_input = array('class' => 'form-control') ?>

<div class="row">
	{{ Form::open() }}
		@if($usuario->id != 0)
			{{ Form::hidden('usuario_id', $usuario->id) }}
		@endif
		@if($registro->id != 0)
			{{ Form::hidden('registro_id', $registro->id) }}
		@endif
		<div style="padding:0" class="col-md-5 panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Verificar Datos</h3>
			</div>
			<div class="panel-body">
				<div class="col-md-12">
					{{ Form::label('nombres', 'Nombres:') }}
					{{ Form::text('nombres', $usuario->nombres, $op_input) }}
				</div>
				<div class="col-md-12">
					{{ Form::label('apellidos', 'Apellidos:') }}
					{{ Form::text('apellidos', $usuario->apellidos, $op_input) }}
				</div>
				<div class="col-md-12">
					{{ Form::label('dni', 'Cédula:') }}
				</div>
				<div class="col-md-3">
					{{Form::select('nacionalidad', array('V'=>'V','E'=>'E','P'=>'P'), null, array('class' =>'form-control'))}}
				</div>
				<div class="col-md-9">
					{{ Form::text('dni', null, $op_input) }}
				</div>
				<div class="col-md-12" style="margin-top:.5em">
					{{ Form::label('', 'Certificado:')}}
					{{ Form::label('si-certificado', 'Si', array('class' => 'radio-inline')) }}
					{{ Form::radio('certificado', '1', $certificado || $exonerado, array('id' => 'si-certificado', 'class' => 'radio-inline')) }}
					{{ Form::label('no-certificado', 'No', array('class' => 'radio-inline')) }}
					{{ Form::radio('certificado', '0', !$certificado && !$exonerado, array('id' => 'no-certificado', 'class' => 'radio-inline')) }}
				</div>
			</div>
		</div>
		<div style="text-align:center;" class="col-md-2">
			{{ Form::submit('Enviar', array('class' => 'btn btn-primary', 'style' => 'margin-top:50%;')) }}
		</div>
		<div id="data-certificado" style="padding:0;" class="col-md-5 panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Certificado</h3>
			</div>
			<div class="panel-body">
				<div id="contenedor-pago" class="col-md-12">
					{{ Form::label('', 'Pago:')}}
					{{ Form::label('efectivo', 'Efectivo', array('class' => 'radio-inline')) }}
					{{ Form::radio('tipo_pago', 'Efectivo', !$ingreso || $ingreso->tipo=='Efectivo', array('id' => 'efectivo', 'class' => 'radio-inline')) }}
					{{ Form::label('deposito', 'Deposito', array('class' => 'radio-inline')) }}
					{{ Form::radio('tipo_pago', 'Deposito', $ingreso && $ingreso->tipo=='Deposito', array('id' => 'deposito', 'class' => 'radio-inline')) }}
					{{ Form::label('transferencia', 'Transferencia:', array('class' => 'radio-inline')) }}
					{{ Form::radio('tipo_pago', 'Transferencia', $ingreso && $ingreso->tipo=='Transferencia', array('id' => 'transferencia', 'class' => 'radio-inline')) }}
					{{ Form::text('codigo_operacion', $ingreso ? $ingreso->codigo_operacion : '', array('id' => 'codigo_operacion', 'class' => 'form-control', 'placeholder' => 'Coloque acá el código de la operación...')) }}
				</div>
				<div id="contenedor-monto" style="margin-top:.5em" class="col-md-12">
					{{ Form::label('', 'Tipo:')}}
					{{ Form::label('estudiante', 'Estudiante:', array('class' => 'radio-inline')) }}
					{{ Form::radio('tipo_certificado', 'Estudiante', $certificado && $certificado->tipo=='Estudiante', array('id' => 'estudiante', 'class' => 'radio-inline')) }}
					{{ Form::label('profesional', 'Profesional', array('class' => 'radio-inline')) }}
					{{ Form::radio('tipo_certificado', 'Profesional',  $certificado && $certificado->tipo=='Profesional', array('id' => 'profesional', 'class' => 'radio-inline')) }}
				</div>
				<div style="margin-top:.5em;" class="col-md-12">
					{{ Form::label('exonerado', 'Exonerado:')}}
					<?php $opt_exonerado = array('id' => 'exonerado', 'class' => 'checkbox-inline');  ?>
					@if($exonerado)
						<?php $opt_exonerado['checked'] = 'checked'; ?>
					@endif
					{{ Form::checkbox('exonerado', '1', $exonerado, $opt_exonerado) }}
				</div>
				<div id="contenedor-razon" class="col-md-12">
					{{Form::label('razon', 'Razón:')}}
					{{Form::select('razon_exoneracion', $razones, $exonerado ? $exonerado->razon_id : null, array('id' => 'select_exon', 'class' => 'form-control'))}}
				</div>
			</div>
		</div>
	{{ Form::close() }}
</div>

@if ($errors->any())
	<ul class="alert alert-danger">
		{{ implode('', $errors->all('<li>:message</li>')) }}
	</ul>
@endif

@stop


@stop