@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/modules/scaffold/delete.js"></script>
@stop

@section('main')

<h1>{{ $titlePage }}</h1>

<p><a class="btn btn-success" href="{{ route($route.'.create') }}"><i class="fa fa-plus-circle"></i>
 Agregar nuevo elemento</a></p>

@if ($listObjs->count())
	<div id="tabla" class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					@foreach($headers as $nombre_propiedad => $propiedad)
						@if($nombre_propiedad != 'id')
							<th>{{ ucfirst($nombre_propiedad) }}</th>
						@endif
					@endforeach
					<th width="150">Operaciones</th>
				</tr>
			</thead>

			<tbody>
				@foreach ($listObjs as $obj)
					<tr>
						@foreach($obj->getVisibles() as $nombre_propiedad => $propiedad)
							@if($nombre_propiedad != 'id')
								@if(is_array($propiedad))
									<td>
										<ul>
											@foreach($propiedad as $elemento)
												<li>{{ $elemento }}</li>
											@endforeach
										</ul>
									</td>
								@else
									<td>{{ $propiedad }}</td>
								@endif
							@endif
						@endforeach
	                    <td>
	                    	<a class="btn btn-default" href="{{ route($route.'.show', $obj->getKey()) }}"><i class="fa fa-eye"></i></a>
	                    	<a class="btn btn-primary" href="{{ route($route.'.edit', $obj->getKey()) }}"><i class="fa fa-pencil-square-o"></i></a>
	                    	<button 
	                    		class="btn btn-danger deleteConfirm" 
	                    		data-route="{{$route}}" 
	                    		data-id="{{ $obj->getKey() }}"
	                    		data-token="{{csrf_token()}}"
	                    	>
							    <i class="fa fa-trash-o"></i>
							</button>
	                    </td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<!-- Confirmación para eliminar -->
	<div class="modal fade" id="modalDelete" data-route="{{$route}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
	        <h4 class="modal-title" id="myModalLabel">Confimación</h4>
	      </div>
	      <div class="modal-body">
	        ¿Esta seguro que desea borrar este registro?
	        <div id="dataObject"></div>
	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button class="btn btn-danger" id="deleteAccept">delete</button>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- <div id="modalDelete" class="reveal-modal small confirm-delete" data-route="{{$route}}" data-reveal> 
		<h3>¿Confirma que desea eliminar este registro?</h3>
		<div id="dataObject"></div>
		<a href="#" id="deleteAccept" class="button tiny alert">Eliminar</a>
        <a href="#" data-reveal-id="modalDelete" class="button tiny secondary">Cancelar</a> 
	</div> -->
@else
	There are no {{ $titlePage }}
@endif

@stop
