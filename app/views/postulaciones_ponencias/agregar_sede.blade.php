@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/modules/ponencias/seleccionar_sedes.js"></script>
<script type="text/javascript" src="/js/modules/departamentos/ciudades.js"></script>
@stop

@section('main')
	<div class="panel panel-primary">
		<div class="panel-heading">
			Sedes a la cual postula
		</div>
		<div class="panel-body">
			@if(count($sedes)>0)
				<div>
					<p><span class="text-danger">*</span>
					Escoge la(s) sede(s) a la(s) cual(es) deseas postularte:</p>
				</div>
				{{Form::open(array('route' => array('postulaciones-ponencias.agregar-sede.post', $ponencia->id)))}}
				@foreach($sedes as $id => $ciudad)
					<div class="col-md-12">
						{{Form::label('sede_'.$id, $ciudad)}}
						{{Form::checkbox('sede_'.$id, $id, NULL, array('class' => 'sede-check'))}}
					</div>
					<div class="col-md-12" style="margin-bottom:1em">
						<span id='origen_{{$id}}'>
							<p>
								¿Desde que ciudad partirá hacia esta sede?

							{{Form::select(null, $departamentos, NULL, array('class' => 'departamentos-select', 'id' => 'departamento_'.$id))}}
							{{Form::select('ciudad_'.$id, array(0 => '--- debes seleccionar un estado ---'), null,array('id' => 'ciudad_'.$id))}}
							</p>
						</span>
					</div>
				@endforeach
				<a class="btn btn-default" href="{{route('ponencias.mis_ponencias')}}">Cancelar</a>
				{{Form::submit('Enviar', array('class' => 'btn btn-primary'))}}
				{{Form::close()}}
			@else
				<p class="alert alert-danger">No hay más sedes para postular</p>
			@endif
		</div>
	</div>
@stop