@extends('layouts.admin')

@section('main')

<h1>Datos de ponente</h1>
{{Form::open(array('route' => 'ponentes.store', 'method' => 'post'))}}
	{{Form::label('resumen_curricular', 'Resumen curricular')}}
	<div class="alert alert-info">
		<p>Un pequeño texto que exprese como desea ser presentado, <strong>NO ES SU CURRICULUM</strong>, es solo un breve fragmento de texto que se leerá por el maestro de ceremonia previo al inicio de su ponencia.</p>
	</div>
	{{Form::textarea('resumen_curricular', NULL, array('class' => 'form-control textarea-mini', 'placeholder' => 'Coloque su resumen curricular acá...'))}}
	<div class="buttons-action">
		{{Form::submit('Enviar', array('class' => 'btn btn-primary'))}}
	</div>
{{Form::close()}}

@stop