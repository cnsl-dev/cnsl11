@extends('auth.account_info')

@section('more_js')
<script type="text/javascript" src="/js/modules/departamentos/ciudad.js"></script>
@stop

@section('main')
	<div class="col-md-10 col-md-offset-1">
		<div class="datos-personales panel panel-info">
			<div class="panel-heading">
			    <h3 class="panel-title">Formulario de modificación de datos personales</h3>
			</div>
			<div class="panel-body">
				{{Form::open(array('route' => 'edit_profile.post', 'files' => true))}}
				@if(Session::get('message'))
			        <p>{{ Session::get('message') }}</p>
			    @endif
				<table>
					<tbody>
						<tr>
							<th></th>
							<td><img src="{{route('avatar')}}" title="avatar"></td>
						</tr>
						<tr>
							<th>{{$perfil['avatar']['label']}}</th>
							<td>{{Form::file('avatar', array('class' => ''))}}</td>
						</tr>
						<tr>
							<th>{{$perfil['nombres']['label']}}</th>
							<td>{{$perfil['nombres']['form_control']}}</td>
						</tr>
						<tr>
							<th>{{$perfil['apellidos']['label']}}</th>
							<td>{{$perfil['apellidos']['form_control']}}</td>
						</tr>
						<tr>
							<th>{{Form::label('dni', 'Cédula')}}</th>
							<td>
								<div class="row">
									<div class="col-md-3">
									  {{$perfil['nacionalidad']['form_control']}}
									</div>
									<div class="col-md-9">
									  {{$perfil['dni']['form_control']}}
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>{{$perfil['sexo']['label']}}</th>
							<td>{{$perfil['sexo']['form_control']}}</td>
						</tr>
						<tr>
							<th>{{$perfil['fecha_nacimiento']['label']}}</th>
							<td>{{$perfil['fecha_nacimiento']['form_control']}}</td>
						</tr>
						<tr>
							<th>{{$perfil['twitter']['label']}}</th>
							<td>{{$perfil['twitter']['form_control']}}</td>
						</tr>
						<tr>
							<th>{{Form::label('telefono', 'Teléfono:')}}</th>
							<td>{{$perfil['telefono']['form_control']}}</td>
						</tr>
						<tr>
							<th>{{$perfil['ciudad']['label']}}</th>
							<td>
								{{$perfil['departamento']['form_control']}}
								{{$perfil['ciudad']['form_control']}}
							</td>
						</tr>
						<tr>
							<th>{{$perfil['direccion']['label']}}</th>
							<td>{{$perfil['direccion']['form_control']}}</td>
						</tr>
						<tr>
							<th>{{$perfil['profesion_u_oficio']['label']}}</th>
							<td>{{$perfil['profesion_u_oficio']['form_control']}}</td>
						</tr>
						<tr>
							<th>{{Form::label('organizacion', 'Organización a la que pertenece:')}}</th>
							<td>{{$perfil['organizacion']['form_control']}}</td>
						</tr>
						<tr>
							<th>{{Form::label('web', 'Sitio web:')}}</th>
							<td>{{$perfil['web']['form_control']}}</td>
						</tr>
					</tbody>
				</table>
				@if ($errors->any())
					<ul>
						{{ implode('', $errors->all('<li class="error">:message</li>')) }}
					</ul>
				@endif
				<div class="buttons-action">
					
				{{Form::submit('Guardar', array('class' => 'btn btn-info'))}}
				</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop