@extends('auth.guest.account')

@section('form')
<h2>Verificación</h2>
{{ Form::open(array('route' => 'find_cedula')) }}
	<table class="table">
		<tbody>
			<tr>
				<th>{{Form::label('dni', 'Cédula')}}</th>
				<td>
					<div class="row">
						<div class="col-md-3">
							{{Form::select('nacionalidad', array('V'=>'V','E'=>'E','P'=>'P'), 'V', array('class' => 'form-control'))}}
						</div>
						<div class="col-md-9">
							{{Form::text('dni', NULL, array('class' => 'form-control'))}}
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<th>{{HTML::image(Captcha::url())}}</th>
				<td>{{ Form::text('captcha', NULL,array('class' => 'form-control')) }}
					Ingrese el texto en la imagen de la izquierda
				</td>
			</tr>
		</tbody>
	</table>
<button class="btn btn-primary">Enviar</button>
{{ Form::close() }}

@stop