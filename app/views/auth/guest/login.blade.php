@extends('auth.guest.account')

@section('form')
	<div class="row">
        <div class="col-md-offset-2 col-md-8 login">
	        <h2>Ingresar</h2>
            {{ Form::open(array('route' => 'login.post', 'method' => 'POST')) }}
            	<div class="row">
	            	{{Form::label('email','Correo electronico:')}}
            	</div>
	            <div class="row">
	            	<div class="input-group">
		            	<div class="input-group-addon"><i class="fa fa-user"></i></div>
		            	<div>
		            		{{Form::email('email', '', array('class' => 'form-control', 'placeholder' => 'Ejemplo: micorreo@ejemplo.com'))}}
		            	</div>
	            	</div>
	            </div>
	            <div class="row">
	            	{{Form::label('password', 'Contraseña')}}
	            </div>
	            <div class="row">
	            	<div class="input-group">
		            	<div class="input-group-addon"><i class="fa fa-unlock-alt"></i></div>
		            	<div>
		            		{{Form::password('password', array('class' => 'form-control', 'placeholder' => 'ingresa tu contraseña'))}}
		            	</div>
	            	</div>
	            </div>
	            <div>
	            	{{Form::label('rememberme', 'Recordar mi cuenta', array('style' => 'display:inline'))}}
	            	{{Form::checkbox('rememberme', 'true', FALSE, array('style' => 'vertical-align:middle; margin:0')) }}
	            </div>
	            <div class="row">
	            	{{ Form::label('captcha', 'Ingrese el texto en la imagen de la izquerda') }}
	            </div>
	            <div class="row" style="margin-bottom:1em">
	            	<div class="col-md-6">
	            		{{HTML::image(Captcha::url(), 'captcha', array('class' => 'captcha')) }}
	            	</div>
	            	<div class="col-md-6">
	            		{{ Form::text('captcha', NULL, array('class' => 'form-control')) }}
	            	</div>
	            </div>
	            {{Form::submit('Ingresar', array('class'=>'btn btn-default'))}}
            {{ Form::close() }}
            <a href="/password/remind">¿Olvidaste tu contraseña?</a>
        </div>
    </div>

@stop