@extends('layouts.base')

@section('base')
<div class="col-md-6 col-md-offset-3">
			<ul class="nav nav-tabs">
			  <li role="presentation"><a href="{{route('login')}}">Ingresar</a></li>
			  <li role="presentation"><a href="{{route('find_cedula')}}">Registrarse</a></li>
			  <li role="presentation"><a href="{{route('remind')}}">Recuperar contraseña</a></li>
			</ul>
			<div class="panel panel-info">
				
				<div class="panel-body">
				@include('layouts.fragments.errors')
				@yield('form')
				</div>
			</div>
		</div>
@stop