@extends('auth.guest.account')

@section('form')
	<h2>Crear una cuenta</h2>
	{{ Form::open(array('route' => 'create_account')) }}
		<table class="table">
			<tbody>
				<tr>
					<th>{{Form::label('nombres', 'Nombres:')}}</th>
					<td>{{Form::text('nombres', $usuario->nombres, array('class' => 'form-control'))}}</td>
				</tr>
				<tr>
					<th>{{Form::label('apellidos', 'Apellidos:')}}</th>
					<td>{{Form::text('apellidos', $usuario->apellidos, array('class' => 'form-control'))}}</td>
				</tr>
				<tr>
					<th>{{Form::label('dni', 'Cédula')}}</th>
					<td>
						<div class="row">
							<div class="col-md-3">
								{{Form::select('nacionalidad', array('V'=>'V','E'=>'E','P'=>'P'), $usuario->nacionalidad, array('class' => 'form-control'))}}
							</div>
							<div class="col-md-9">
								{{Form::text('dni', $usuario->dni, array('class' => 'form-control'))}}
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<th>{{Form::label('sexo', 'Sexo')}}</th>
					<td>
						{{Form::select('sexo', array(0 => '----Escoja una opción---','Masculino'=>'Masculino','Femenino'=>'Femenino'), $usuario->sexo , array('class' => 'form-control'))}}
					</td>
				</tr>
				<tr>
					<th>{{Form::label('email', 'Correo electronico')}}</th>
					<td>{{ Form::email('email', $usuario->email, array('class' => 'form-control')) }}</td>
				</tr>
				<tr>
					<th>{{Form::label('password', 'Contraseña:')}}</th>
					<td>{{Form::password('password', array('class' => 'form-control'))}}</td>
				</tr>
				<tr>
					<th>{{Form::label('password-repeat', 'Repita la contraseña:')}}</th>
					<td>{{ Form::password('password-repeat', array('class' => 'form-control')) }}</td>
				</tr>
				<tr>
					<th>{{HTML::image(Captcha::url())}}</th>
					<td>{{ Form::text('captcha', NULL,array('class' => 'form-control')) }}
						Ingrese el texto en la imagen de la izquierda
					</td>
				</tr>
			</tbody>
		</table>
		{{Form::hidden('old', $old ? 1 : 0)}}
		{{Form::hidden('create', $create ? 1 : 0)}}
		<p><a href="{{route('resend_verify')}}">¿No te llego tu correo de confirmación?</a></p>
	<button class="btn btn-primary">Enviar</button>
	{{ Form::close() }}
@stop