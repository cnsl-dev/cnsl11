@extends('auth.guest.account')

@section('form')
	<h2>Reenviar correo de confirmación</h2>
	{{ Form::open(array('route' => 'resend_verify.post')) }}
		<table class="table">
			<tbody>
				<tr>
					<th>{{ Form::label('email', 'Email') }}</th>
					<td>{{ Form::email('email', '', array('class' => 'form-control' )) }}</td>
				</tr>
				<tr>
					<th>{{HTML::image(Captcha::url())}}</th>
					<td>{{ Form::text('captcha', NULL,array('class' => 'form-control')) }}
						Ingrese el texto en la imagen de la izquierda
					</td>
				</tr>
			</tbody>
		</table>
		{{ Form::submit('Enviar', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop