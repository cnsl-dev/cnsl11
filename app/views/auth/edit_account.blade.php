@extends('auth.account_info')

@section('main')
	<div class="col-md-10 col-md-offset-1">
		<div class="datos-personales panel panel-info">
			<div class="panel-heading">
			    <h3 class="panel-title">Formulario de modificación de correo electronico</h3>
			</div>
			<div class="panel-body">
				{{Form::open(array('route' => 'edit_email.post'))}}
				<table>
					<tbody>
						<tr>
							<th>{{Form::label('email', 'Dirección de correo:')}}</th>
							<td>{{Form::text('email', $email, array('class' => 'form-control'))}}</td>
						</tr>
					</tbody>
				</table>
				{{Form::submit('Guardar', array('class' => 'btn btn-warning'))}}
				{{Form::close()}}
			</div>
		</div>
	</div>
	<div class="col-md-10 col-md-offset-1">
		<div class="datos-personales panel panel-info">
			<div class="panel-heading">
			    <h3 class="panel-title">Formulario de modificación de contraseña</h3>
			</div>
			<div class="panel-body">
				{{Form::open(array('route' => 'edit_password.post'))}}
				<table>
					<tbody>
						<tr>
							<th>{{Form::label('password', 'Introdusca su nueva contraseña:')}}</th>
							<td>{{Form::password('password', array('class' => 'form-control'))}}</td>
						</tr>
						<tr>
							<th>{{Form::label('password-repeat', 'Introduzca nuevamente la contraseña:')}}</th>
							<td>{{Form::password('password-repeat', array('class' => 'form-control'))}}</td>
						</tr>
					</tbody>
				</table>
				{{Form::submit('Guardar', array('class' => 'btn btn-warning'))}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop