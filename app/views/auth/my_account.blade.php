@extends('auth.account_info')

@section('main')
	<div class="row">
		<div class="col-md-3 cnsl-avatar">
			<img src="{{route('avatar')}}" title="avatar">
		</div>
		<div class="col-md-9">
			<div class="datos-personales panel panel-info">
				<div class="panel-heading">
				    <h3 class="panel-title">Cuenta</h3>
				</div>
				<div class="panel-body">
					<table>
						<tbody>
							<tr>
								<th>Email:</th>
								<td>{{ $usuario->email }}</td>
							</tr>
							<tr>
								<th>Pasword:</th>
								<td>******</td>
							</tr>
						</tbody>
					</table>
					<a class="btn btn-primary btn-xs" href="{{route('edit_account')}}">
						<i class="fa fa-pencil-square-o"></i>
						Editar
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="datos-personales panel panel-info">
				<div class="panel-heading">
				    <h3 class="panel-title">Datos personales</h3>
				</div>
				<div class="panel-body">
					<table>
						<tbody>
							<tr>
								<th>Nombres:</th>
								<td>{{ $usuario->nombres }}</td>
							</tr>
							<tr>
								<th>Apellidos:</th>
								<td>{{ $usuario->apellidos }}</td>
							</tr>
							<tr>
								<th>Cédula:</th>
								<td>{{ $usuario->dni }}</td>
							</tr>
							<tr>
								<th>Sexo:</th>
								<td>{{ $usuario->sexo }}</td>
							</tr>
							<tr>
								<th>Teléfono:</th>
								<td>{{ $usuario->telefono }}</td>
							</tr>
							<tr>
								<th>Ciudad:</th>
								<td>{{ $usuario->ciudad }}</td>
							</tr>
							<tr>
								<th>Profesión u oficio:</th>
								<td>{{ $usuario->profesion }}</td>
							</tr>
							<tr>
								<th>Organización a la que pertenece:</th>
								<td>{{ $usuario->organizacion }}</td>
							</tr>
							<tr>
								<th>Sitio web:</th>
								<td>{{ $usuario->web }}</td>
							</tr>
						</tbody>
					</table>
					<a class="btn btn-primary btn-xs" href="{{route('edit_profile')}}">
						<i class="fa fa-pencil-square-o"></i>
						Editar
					</a>
				</div>
			</div>
		</div>
	</div>
@stop