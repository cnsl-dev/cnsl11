@extends('layouts.admin')

@section('menu_secundario')
	<a class="list-group-item" href="{{route('my_account')}}">
		Mi cuenta
	</a>
	<a class="list-group-item" href="{{route('edit_account')}}">
		Cambiar datos de la cuenta
	</a>
	<a class="list-group-item" href="{{route('edit_profile')}}">
		Cambiar datos personales
	</a>
@stop