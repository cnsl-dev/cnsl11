@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/modules/sedes/delete.js"></script>
@stop

@section('main')

<h1>Sede</h1>

<p><a class="btn btn-default" href="{{ route('evento.sedes.index') }}"><i class="fa fa-arrow-left"></i>
Regresar a la lista de todos los elementos</a></p>

	<table class="table table-bordered">
		<tbody>
			<tr>
				<th>Ciudad</th>
				<td>
					{{ $sede->ciudad->nombre }}, {{ $sede->ciudad->departamento->nombre }}
				</td>
			</tr>
			<tr>
				<th>Inicio</th>
				<td>
					{{ datetimeUtils::modelToView($sede->inicio) }}
				</td>
			</tr>
			<tr>
				<th>Fin</th>
				<td>
					{{ datetimeUtils::modelToView($sede->fin) }}
				</td>
			</tr>
			<tr>
				<th>Activo</th>
				<td>
					@if($sede->activo == 0)
						<span class="text-danger"><i class="fa fa-times"></i></span>
					@else
						<span class="text-success"><i class="fa fa-check"></i></span>
					@endif
				</td>
			</tr>
			<tr>
				<th>Actual</th>
				<td>
					@if($sede->actual == 0)
						<span class="text-danger"><i class="fa fa-times"></i></span>
					@else
						<span class="text-success"><i class="fa fa-check"></i></span>
					@endif
				</td>
			</tr>
		</tbody>
	</table>
<div>
<a class="btn btn-primary" href="{{ route('evento.sedes.edit', $sede->getKey()) }}">
	<i class="fa fa-pencil-square-o"></i> Editar
</a>
<button 
	class="btn btn-danger deleteConfirm" 
	data-route="{{ route('evento.sedes.destroy', $sede->getKey())}}" 
	data-id="{{ $sede->getKey() }}"
	data-token="{{csrf_token()}}"
>
    <i class="fa fa-trash-o"></i> Eliminar
</button>
</div>
@stop
