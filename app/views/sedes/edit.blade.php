@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="{{ asset('js/modules/departamentos/ciudad.js') }}"></script>
@stop

@section('main')

<h1>Editar sede</h1>
{{ Form::model($sede, array('method' => 'PATCH', 'route' => array('evento.sedes.update', $sede->getKey()))) }}
	<div class="row">
		{{ Form::label('estado', 'Estado:') }}
		{{ Form::select('estado', $departamentos, $sede->ciudad->departamento->id, array('class' => 'form-control departamentos-select'))}}
	</div>
	<div class="row">
		{{ Form::label('ciudad_id', 'Ciudad:') }}
		{{ Form::select('ciudad_id', $ciudades, NULL, array('class' => 'form-control', 'id' => 'ciudad_id'))}}
	</div>
	<div class="row">
		{{ Form::label('inicio', 'Fecha de inicio:')}}
		{{ Form::text('inicio', NULL, array('class' => 'form-control datepicker'))}}
	</div>
	<div class="row">
		{{ Form::label('fin', 'Fecha de finalización:')}}
		{{ Form::text('fin', NULL, array('class' => 'form-control datepicker'))}}
	</div>
	<div class="row">
		{{ Form::label('limite_ponencias', 'Cierre de ponencias:')}}
		{{ Form::text('limite_ponencias', NULL, array('class' => 'form-control datepicker'))}}
	</div>
	<div class="row">
		{{ Form::label('activo', 'Activo:')}}
		{{Form::checkbox('activo', '1', NULL)}}
	</div>
	<div class="row">
		{{ Form::label('actual', 'Actual:')}}
		{{Form::checkbox('actual', '1', NULL)}}
	</div>
	<div class="row buttons-action">
		<a class="btn btn-default" href="{{ route('evento.sedes.index') }}"><i class="fa fa-arrow-left"></i> Regresar</a>
		<button class="btn btn-primary" onclick="submit()">
			<i class="fa fa-database"></i> Almacenar
		</button>
	</div>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
