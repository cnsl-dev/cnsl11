@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/bootstrap-dialog.min.css">
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/bootstrap-dialog.min.js"></script>
<script type="text/javascript" src="/js/modules/organizadores/unlink.js"></script>
<script type="text/javascript" src="/js/modules/scaffold/delete.js"></script>
@stop

@section('main')

<h1>{{ $titlePage }}</h1>

<p><a class="btn btn-success" href="{{ route('evento.sedes.create') }}"><i class="fa fa-plus-circle"></i>
 Agregar nuevo elemento</a></p>

@if ($sedes->count())
	<div id="tabla" class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Ciudad</th>
					<th>Inicio</th>
					<th>Fin</th>
					<th>Cierre de ponencias</th>
					<th>Activa</th>
					<th>Actual</th>
					<th width="150">Operaciones</th>
					<th>Organizador</th>
				</tr>
			</thead>

			<tbody>
				@foreach ($sedes as $sede)
					<tr>
						<?php $propiedades =  $sede->getVisibles() ?>
						<td class="sede">{{$sede->ciudad->nombre}}</td>
						<td>
							{{ !empty($sede->inicio) ? datetimeUtils::modeltoView($sede->inicio) : ''}} 
						</td>
						<td>
							{{ !empty($sede->fin) ? datetimeUtils::modeltoView($sede->fin) : ''}}
						</td>
						<td>
							{{ !empty($sede->limite_ponencias) ? datetimeUtils::modeltoView($sede->limite_ponencias) : ''}}
						</td>
						<td>
							@if($sede->activo == 0)
								<span class="text-danger"><i class="fa fa-times"></i></span>
							@else
								<span class="text-success"><i class="fa fa-check"></i></span>
							@endif
						</td>
						<td>
							@if($sede->actual == 0)
								<span class="text-danger"><i class="fa fa-times"></i></span>
							@else
								<span class="text-success"><i class="fa fa-check"></i></span>
							@endif
						</td>
	                    <td>
	                    	<a class="btn btn-default" href="{{ route('evento.sedes.show', $sede->getKey()) }}"><i class="fa fa-eye"></i></a>
	                    	<a class="btn btn-primary" href="{{ route('evento.sedes.edit', $sede->getKey()) }}"><i class="fa fa-pencil-square-o"></i></a>
	                    	<button 
	                    		class="btn btn-danger deleteConfirm" 
	                    		data-route="{{ route('evento.sedes.destroy', $sede->getKey()) }}" 
	                    		data-id="{{ $sede->getKey() }}"
	                    		data-token="{{csrf_token()}}"
	                    	>
							    <i class="fa fa-trash-o"></i>
							</button>
	                    </td>
	                    <td>
	                    	@if($sede->organizador()->get()->count()>0)
	                    		<?php $usuario = $sede->organizador->usuario; ?>
	                    		<span class="organizador">{{ $usuario->primerNombre() }} {{ $usuario->primerApellido() }} </span>
	                    		<a href="#" class="btn btn-danger unlink-organizador" data-route="{{route('organizador.sede.delete', $sede->getKey())}}">
		                    		<i class="fa fa-times"></i>
		                    	</a>
	                    	@else
		                    	<a href="{{route('organizador.sede',$sede->getKey())}}" class="btn btn-success">
		                    		<i class="fa fa-user"></i> Agregar
		                    	</a>
	                    	@endif
	                    </td>
					</tr>
				@endforeach
			</tbody>
		</table>
		{{Form::hidden('token', csrf_token(), array('id' => 'token'))}}
	</div>
	<!-- Confirmación para eliminar -->
	<div class="modal fade" id="modalDelete" data-route="{{'evento.sedes'}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
	        <h4 class="modal-title" id="myModalLabel">Confimación</h4>
	      </div>
	      <div class="modal-body">
	        ¿Esta seguro que desea borrar este registro?
	        <div id="datasedeect"></div>
	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button class="btn btn-danger" id="deleteAccept">delete</button>
	      </div>
	    </div>
	  </div>
	</div>
@else
	<p>No se ha registrado ninguna sede aun.</p>
@endif

@stop
