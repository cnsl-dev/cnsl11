@extends('layouts.admin')

@section('main')

@if($preguntas->count()>0)
	<h1>Mis respuestas</h1>
	@foreach($preguntas as $pregunta)
		<div class="col-md-6">
			<div class="panel panel-default">
			  <div class="panel-heading">
			  	<h3 class="panel-title">{{$pregunta->titulo}}</h3>
			  </div>
			  <div class="panel-body">
			  @foreach($pregunta->respuestasParaUsuario(Auth::user()->id) as $opcion)
		    	<p>
		    		{{Form::label($opcion->id, $opcion->descripcion)}}
		    	</p>
			  @endforeach
			  </div>
			</div>
		</div>
	@endforeach
@endif

@stop