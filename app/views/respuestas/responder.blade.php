@extends('layouts.admin')

@section('main')
	
	@if($preguntas->count()>0)
		<h1>Encuesta</h1>
		{{Form::open(array('route' => 'encuesta.respuestas.responder.post'))}}
			@foreach($preguntas as $pregunta)
				<div class="col-md-12">
					<div class="panel panel-default">
					  <div class="panel-heading">
					  	<h3 class="panel-title">{{$pregunta->titulo}}</h3>
					  </div>
					  <div class="panel-body">
					  	@if($pregunta->seleccion == 'simple')
						    @foreach($pregunta->opciones()->orderBy('orden')->get() as $opcion)
						    	<p>
						    		{{Form::radio($pregunta->id, $opcion->id, NULL, array('id' => $opcion->id))}}
						    		{{Form::label($opcion->id, $opcion->descripcion)}}
						    	</p>
						    @endforeach
					    @else
					    	@foreach($pregunta->opciones()->orderBy('orden')->get() as $opcion)
						    	<p>
						    		{{Form::checkbox($pregunta->id.'[]', $opcion->id, NULL, array('id' => $opcion->id))}}
						    		{{Form::label($opcion->id, $opcion->descripcion)}}
						    	</p>
						    @endforeach
					    @endif
					  </div>
					</div>
				</div>
			@endforeach
			<div class="col-md-12">		
				{{Form::submit('Enviar', array('class' => 'btn btn-primary'))}}
			</div>
		{{Form::close()}}
	@endif

@stop
