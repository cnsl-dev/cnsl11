@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/modules/scaffold/delete.js"></script>
@stop

@section('main')

<h1>{{ $titlePage }}</h1>

<p><a class="btn btn-default" href="{{ route($route.'.index') }}"><i class="fa fa-arrow-left"></i>
Regresar a la lista de todos los elementos</a></p>

	<table class="table table-bordered">
		<tbody>
			<?php $propiedades = $obj->getVisibles() ?>
			<tr>
				<th>Nombre</th>
				<td>{{$propiedades['nombre']}}</td>
			</tr>
			<tr>
				<th>Abreviatura:</th>
				<td>{{$propiedades['abreviatura']}}</td>
			</tr>
			<tr>
				<th>Inicio:</th>
				<td>{{$propiedades['inicio']}}</td>
			</tr>
			<tr>
				<th>Fin:</th>
				<td>{{$propiedades['fin']}}</td>
			</tr>
			<tr>
				<th>Activo</th>
				<td>{{$propiedades['activo']}}</td>
			</tr>
			<tr>
				<th>Sedes</th>
				<td>
					@if(is_array($propiedades['sedes']))
						<ul>
						@foreach($obj->sedes()->get() as $sede)
							<li>{{$sede->ciudad->nombre}} - {{ date('d/m/Y',strtotime($sede->inicio))}}</li>
						@endforeach
						</ul>
					@endif
				</td>
			</tr>
		</tbody>
	</table>
<div>
<a class="btn btn-primary" href="{{ route($route.'.edit', $obj->getKey()) }}">
	<i class="fa fa-pencil-square-o"></i> Editar
</a>
<button 
	class="btn btn-danger deleteConfirm" 
	data-route="{{$route}}" 
	data-id="{{ $obj->getKey() }}"
	data-token="{{csrf_token()}}"
>
    <i class="fa fa-trash-o"></i> Eliminar
</button>
</div>
<!-- Confirmación para eliminar -->
	<div class="modal fade" id="modalDelete" data-route="{{$route}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
	        <h4 class="modal-title" id="myModalLabel">Confimación</h4>
	      </div>
	      <div class="modal-body">
	        ¿Esta seguro que desea borrar este registro?
	        <div id="dataObject"></div>
	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button class="btn btn-danger" id="deleteAccept">delete</button>
	      </div>
	    </div>
	  </div>
	</div>
@stop
