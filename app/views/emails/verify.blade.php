<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Confirmación de cuenta de correo electrónico</h2>
        <div>
        	<p>Gracias por registrarte en la web del Congreso Nacional de Software Libre</p>
        	<p>Por favor copia el siguiente enlace en la barra de dirección de tu navegador o has click en el:</p>
        	<p>
                <a href="{{ URL::route('confirm_email', $confirmation_code) }}">
                    {{ URL::route('confirm_email', $confirmation_code) }}
                </a>
            </p>
        </div>

    </body>
</html>