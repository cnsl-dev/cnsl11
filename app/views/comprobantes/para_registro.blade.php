<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Comprobante de registro para el CNSL11 [{{$registro->sede->ciudad->nombre}}]</title>
	<link rel="stylesheet" type="text/css" href="/css/comprobantes/style.css">
</head>
<body>
	<h3>Comprobante de registro para el CNSL11 [{{$registro->sede->ciudad->nombre}}]</h3>
	<div class="info">
		<p>Con esta planilla impresa usted no hará colas para ingresar al Congreso Nacional de Software Libre en su sede. Solo presentela en puerta y podrá acceder al evento. Para que el código de barra que está en la parte inferior sea leido correctamente se sugiere enérgicamente la impresión laser.</p>
		<p>Si usted optará por el certificado opcional, recuerde llevar junto con esta planilla el recibo de deposito o comprobante de transferencia, también tendra la opción de cancelar en efectivo a la entrada del evento.</p>
	</div>
	<div class="datos">
		<p>{{$registro->usuario->nombres}}</p>
		<p>{{$registro->usuario->apellidos}}</p>
		<p>{{$registro->usuario->dni}}</p>
	</div>
	<div class="barcode">
		{{$barcode}}
	</div>
	<div class="imprimir">
		<a href="#" onclick="window.print()">Imprimir</a>
	</div>
</body>
</html>