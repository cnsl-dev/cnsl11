@extends('layouts.admin')

@section('main')
    
    <h1>{{$ponencia->titulo_de_la_ponencia}}</h1>
    
    <h2>Notas anteriores</h2>
    @if($mensajes->count()>0)
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <td>Mensajes</td>
                <td>Fecha</td>
            </tr>
        </thead>
        <tbody>
            @foreach($mensajes->get() as $mensaje)
                @if($mensaje->leido == 0)
                    <tr>
                        <td>
                            <strong>
                                <a class="text-danger" href="{{route('mensajes.ponencias.show_own', $mensaje->id)}}">
                                    {{$mensaje->mensaje}}
                                </a>
                            </strong>
                        </td>
                        <td>{{ date('d/m/Y H:i',strtotime($mensaje->enviado)) }}</td>
                    </tr>
                @else
                    <tr>
                        <td>
                            <a class="text-muted" href="{{route('mensajes.ponencias.show_own', $mensaje->id)}}">
                                {{$mensaje->mensaje}}
                            </a>
                        </td>
                        <td>{{ date('d/m/Y H:i',strtotime($mensaje->enviado)) }}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
    @else
        <p>No se ha registrado ninguna nota para esta ponencia todavia</p>
    @endif
    
    <a href="{{route('ponencias.mis_ponencias', $ponencia->id)}}" class="btn btn-default">Regresar</a>
    
@stop