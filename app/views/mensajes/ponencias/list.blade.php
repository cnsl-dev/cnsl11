@extends('layouts.admin')

@section('main')
    
    <h1>{{$ponencia->titulo_de_la_ponencia}}</h1>
    
    <h2>Notas anteriores</h2>
    @if($mensajes->count()>0)
    <table class="table table-bordered">
        <thead>
            <tr>
                <td>Mensaje</td>
                <td>Visible</td>
            </tr>
        </thead>
        <tbody>
            @foreach($mensajes->get() as $mensaje)
                <tr>
                    <td>{{$mensaje->mensaje}}</td>
                    <td>{{$mensaje->enviado}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @else
        <p>No se ha registrado ninguna nota para esta ponencia todavia</p>
    @endif
    
    <h2>Nueva nota</h2>
    {{Form::open(array('route' => array('mensajes.ponencias.store', $ponencia->id)))}}
        {{Form::label('mensaje', 'Mensaje:')}}
        {{Form::textArea('mensaje', null, array('class' => 'form-control textarea-mini'))}}
        <div class="buttons-action">
            <a class="btn btn-default" href="{{route('ponencias.show', $ponencia->id)}}">Regresar</a>
            {{Form::submit('Guardar', array('class' => 'btn btn-primary'))}}
        </div>
    {{Form::close()}}
    
@stop