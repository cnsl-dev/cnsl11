@extends('layouts.admin')

@section('main')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4>Mensaje:</h4>
        </div>
        <div class="panel-body">
            @if($mensaje->responde()->get()->count())
                <p class="text-muted"><strong>En respuesta a:</strong> {{$mensaje->responde()->first()->mensaje}}</p>
                <p style="padding-left:1em">{{$mensaje->mensaje}}</p>
            @else
                <p>{{$mensaje->mensaje}}</p>
            @endif
            <div class="buttons-action">
                <a  class="btn btn-default"
                    href="{{route('mensajes.ponencias.list_own', $mensaje->ponencia->id)}}">Regresar
                </a>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Responder:</h4>
        </div>
        <div class="panel-body">
            {{Form::open(array('route' => array('mensajes.ponencias.store_own', $mensaje->ponencia->id)))}}
            {{Form::hidden('respuesta', $mensaje->id)}}
            {{Form::label('mensaje', 'Mensaje:')}}
            {{Form::textArea('mensaje', null, array('class' => 'form-control textarea-mini'))}}
            <div class="buttons-action">
                {{Form::submit('Enviar', array('class' => 'btn btn-primary'))}}
            </div>
        {{Form::close()}}
        </div>
    </div>
    
@stop