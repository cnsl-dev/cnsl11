@extends('layouts.admin')

@section('main')

	<a href="{{route('ponencias.create')}}" class="btn btn-danger btn-lg">Enviar una nueva ponencia</a>
	<h1>Mis ponencias</h1>
	@if($ponencias->count()>0)
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Título de la ponencia</th>
				<th>Status</th>
				<th>Sedes postuladas</th>
			</tr>
		</thead>
		<tbody>
			@foreach($ponencias as $ponencia)
				<tr class="{{$ponencia->getClassCss()}}">
					<td> 
						@if($ponencia->esEditable())
							<a 
								class="text-{{$ponencia->getClassCss()}}" 
								href="{{route('ponencias.mi_ponencia.edit',$ponencia->id)}}"
								data-toggle="tooltip" 
								title="Haga click sobre esta ponencia para editarla">
								{{$ponencia->titulo_de_la_ponencia}}
							</a>
						@else
							{{$ponencia->titulo_de_la_ponencia}}
						@endif
					</td>
					<td>{{$ponencia->status}}</td>
					<td>
						<ul>
						@foreach($ponencia->postulaciones()->get() as $postulacion)
							<li>{{$postulacion->sede->ciudad->nombre}}</li>
						@endforeach
						</ul>
					</td>
					<td>
						<a 	href="{{route('postulaciones-ponencias.agregar-sede', $ponencia->id)}}"
							class="btn btn-success"
							data-toggle="tooltip"
							title="Postular a una nueva sede"
							>
								<i class="fa fa-plus-circle"></i>
						<a/>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<p>
		<span class="text-info"><strong>Admitida:</strong> si su ponencia fue admitida en el CNSL</span>,
		<span class="text-warning"><strong>En revisión:</strong> si su ponencia se encuentra bajo revisión</span>,
		<span class="text-danger"><strong>No procede:</strong> si su ponencia no fue admitida en el CNSL</span>
	</p>
	<div class="paginate">
		{{$ponencias->links()}}
	</div>
	@else
		<p>No ha registro ninguna ponencia aún</p>
	@endif

@stop