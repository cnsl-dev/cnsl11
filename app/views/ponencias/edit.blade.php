@extends('layouts.admin')

@section('more_js')
@stop

@section('main')
{{Form::model($ponencia, array('route' => array('ponencias.mi_ponencia.update', $ponencia->id), 'method' => 'patch', 'files' => true))}}
	<div class="panel panel-primary">
		<div class="panel-heading">
			Datos de la ponencia
		</div>
		<div class="panel-body">
			<span class="text-danger">*</span>
			{{Form::label('titulo_de_la_ponencia', 'Título de la ponencia:')}}
			{{Form::text('titulo_de_la_ponencia', NULL, array('class' => 'form-control'))}}
			<span class="text-danger">*</span>
			{{Form::label('resumen_de_la_ponencia', 'Resumen de la ponencia')}}
			{{Form::textarea('resumen_de_la_ponencia', NULL, array('class' => 'form-control textarea-mini'))}}
			<span class="text-danger">*</span>
			{{Form::label('', 'Eje temático:')}}
			<div class="panel panel-default">
				<div class="panel-body">
					@foreach($ejes_tematicos as $id => $eje)
						<div>
							{{ Form::checkbox('ejes[]', $id, in_array($id, $ejes_de_ponencia), array('class' => 'checkbox-inline', 'id' => 'eje'.$id)) }}
							{{Form::label('eje'.$id, $eje, array('class' => 'nobold'))}}
						</div>
					@endforeach
				</div>
			</div>
			<span class="text-danger">*</span>
			{{Form::label('duracion', 'Duración de la ponencia (en minutos):')}}
			<div class="input-group">
				{{Form::text('duracion', NULL, array('class' => 'form-control', 'placeholder' => 'Ingrese acá la duración de su ponencia en minutos...'))}}
				<div class="input-group-addon">Minutos</div>
		    </div>
			<span class="text-danger">*</span>
			{{Form::label('laminas', 'Laminas de la ponencia:')}}
			{{Form::file('laminas')}}
			<a 
				href="{{route('ponencias.mis_laminas', $ponencia->id)}}" 
				class="btn btn-success" 
				style="margin-top:1em" 
				data-toggle="tooltip" 
				title="Haga click sobre este boton para examinar las laminas enviadas por usted para esta ponencia"
				>
				<i class="fa fa-cloud-download"></i>
				Laminas enviadas
			</a>
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading">
			Requisitos de su ponencia
		</div>
		<div class="panel-body">
			<div>
				<span class="text-danger">*</span>
				{{Form::label('proximidad', '¿Necesita operar manualmente su equipo?')}}
				{{ Form::label('si-proximidad', 'Si', array('class' => 'radio-inline')) }}
				{{ Form::radio('proximidad', '1', $requisitos->proximidad, array('id' => 'si-proximidad', 'class' => 'radio-inline')) }}
				{{ Form::label('no-proximidad', 'No', array('class' => 'radio-inline')) }}
				{{ Form::radio('proximidad', '0', !$requisitos->proximidad, array('id' => 'no-proximidad', 'class' => 'radio-inline')) }}
			</div>
			<div>
				<span class="text-danger">*</span>
				{{Form::label('conectividad', '¿Necesita conexión a internet?')}}
				{{ Form::label('si-conectividad', 'Si', array('class' => 'radio-inline')) }}
				{{ Form::radio('conectividad', '1', $requisitos->conectividad, array('id' => 'si-conectividad', 'class' => 'radio-inline')) }}
				{{ Form::label('no-conectividad', 'No', array('class' => 'radio-inline')) }}
				{{ Form::radio('conectividad', '0', !$requisitos->conectividad, array('id' => 'no-conectividad', 'class' => 'radio-inline')) }}
			</div>
			{{Form::label('hardware', 'Requisitos de hardware:')}}
			{{Form::textarea('hardware', $requisitos->hardware, array('class' => 'form-control textarea-mini', 'placeholder' => 'Coloque aqui los requisitos de hardware para su ponencia...'))}}
			{{Form::label('software', 'Requisitos de software:')}}
			{{Form::textarea('software', $requisitos->software, array('class' => 'form-control textarea-mini', 'placeholder' => 'Coloque aqui los requisitos de software para su ponencia...'))}}
			{{Form::label('otros', 'Requisitos de otro tipo:')}}
			{{Form::textarea('otros', $requisitos->otros, array('class' => 'form-control textarea-mini', 'placeholder' => 'Coloque aqui cualquier otro requisito que necesite especificar...'))}}
		</div>
	</div>
	<div class="buttons-action">
		<a href="{{route('ponencias.mis_ponencias')}}" class="btn btn-default">Regresar</a>
		{{Form::submit('Enviar', array('class' => 'btn btn-primary'))}}
	</div>
{{Form::close()}}
@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop