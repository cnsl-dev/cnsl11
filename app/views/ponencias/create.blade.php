@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/modules/ponencias/seleccionar_sedes.js"></script>
<script type="text/javascript" src="/js/modules/departamentos/ciudades.js"></script>
@stop

@section('main')

{{Form::open(array('route' => 'ponencias.store', 'files' => true))}}
	<div class="panel panel-primary">
		<div class="panel-heading">
			Sedes a la cual postula
		</div>
		<div class="panel-body">
			<div>
				<p><span class="text-danger">*</span>
				Escoge la(s) sede(s) a la(s) cual(es) deseas postularte:</p>
			</div>
			@foreach($sedes as $id => $ciudad)
				<div class="col-md-12">
					{{Form::label('sede_'.$id, $ciudad)}}
					{{Form::checkbox('sede_'.$id, $id, NULL, array('class' => 'sede-check'))}}
				</div>
				<div class="col-md-12" style="margin-bottom:1em">
					<span id='origen_{{$id}}'>
						<p>
							¿Desde que ciudad partirá hacia esta sede?

						{{Form::select(null, $departamentos, NULL, array('class' => 'departamentos-select form-control departamento', 'id' => 'departamento_'.$id))}}
						{{Form::select('ciudad_'.$id, array(0 => '--- debes seleccionar un estado ---'), null,array('id' => 'ciudad_'.$id, 'class' => 'form-control ciudad'))}}
						</p>
					</span>
				</div>
			@endforeach
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading">
			Datos de la ponencia
		</div>
		<div class="panel-body">
			<span class="text-danger">*</span>
			{{Form::label('titulo_de_la_ponencia', 'Título de la ponencia:')}}
			{{Form::text('titulo_de_la_ponencia', NULL, array('class' => 'form-control'))}}
			<span class="text-danger">*</span>
			{{Form::label('resumen_de_la_ponencia', 'Resumen de la ponencia')}}
			{{Form::textarea('resumen_de_la_ponencia', NULL, array('class' => 'form-control textarea-mini'))}}
			<span class="text-danger">*</span>
			{{Form::label('', 'Eje temático:')}}
			<div class="panel panel-default">
				<div class="panel-body">
					@foreach($ejes_tematicos as $id => $eje)
						<div>
							{{ Form::checkbox('ejes[]', $id, NULL, array('class' => 'checkbox-inline', 'id' => 'eje'.$id)) }}
							{{Form::label('eje'.$id, $eje, array('class' => 'nobold'))}}
						</div>
					@endforeach
				</div>
			</div>
			<span class="text-danger">*</span>
			{{Form::label('duracion', 'Duración de la ponencia (en minutos):')}}
			<div class="input-group">
		      {{Form::number('duracion', NULL, array('class' => 'form-control', 'placeholder' => 'Ingrese acá la duración de su ponencia en minutos...'))}}
		      <div class="input-group-addon">Minutos</div>
		    </div>
			<span class="text-danger">*</span>
			{{Form::label('laminas', 'Laminas de la ponencia:')}}
			{{Form::file('laminas')}}
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading">
			Requisitos de su ponencia
		</div>
		<div class="panel-body">
			<div>
				<span class="text-danger">*</span>
				{{Form::label('proximidad', '¿Necesita operar manualmente su equipo?')}}
				{{ Form::label('si-proximidad', 'Si', array('class' => 'radio-inline')) }}
				{{ Form::radio('proximidad', '1', NULL, array('id' => 'si-proximidad', 'class' => 'radio-inline')) }}
				{{ Form::label('no-proximidad', 'No', array('class' => 'radio-inline')) }}
				{{ Form::radio('proximidad', '0', NULL, array('id' => 'no-proximidad', 'class' => 'radio-inline')) }}
			</div>
			<div>
				<span class="text-danger">*</span>
				{{Form::label('conectividad', '¿Necesita conexión a internet?')}}
				{{ Form::label('si-conectividad', 'Si', array('class' => 'radio-inline')) }}
				{{ Form::radio('conectividad', '1', NULL, array('id' => 'si-conectividad', 'class' => 'radio-inline')) }}
				{{ Form::label('no-conectividad', 'No', array('class' => 'radio-inline')) }}
				{{ Form::radio('conectividad', '0', NULL, array('id' => 'no-conectividad', 'class' => 'radio-inline')) }}
			</div>
			{{Form::label('hardware', 'Requisitos de hardware:')}}
			{{Form::textarea('hardware', NULL, array('class' => 'form-control textarea-mini', 'placeholder' => 'Coloque aqui los requisitos de hardware para su ponencia...'))}}
			{{Form::label('software', 'Requisitos de software:')}}
			{{Form::textarea('software', NULL, array('class' => 'form-control textarea-mini', 'placeholder' => 'Coloque aqui los requisitos de software para su ponencia...'))}}
			{{Form::label('otros', 'Requisitos de otro tipo:')}}
			{{Form::textarea('otros', NULL, array('class' => 'form-control textarea-mini', 'placeholder' => 'Coloque aqui cualquier otro requisito que necesite especificar...'))}}
		</div>
	</div>
	<div class="buttons-action">
		<a href="{{route('ponencias.mis_ponencias')}}" class="btn btn-default">Regresar</a>
		{{Form::submit('Enviar', array('class' => 'btn btn-primary'))}}
	</div>
{{Form::close()}}
@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop