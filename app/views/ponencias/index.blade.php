@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="{{ asset('js/modules/ponencias/filtros.js') }}"></script>
@stop

@section('main')

	<h1>Ponencias postuladas</h1>
	<div class="row">
		<button id="reporte" class="btn btn-danger">
			<i class="fa fa-file-o"></i>
			Generar reporte
		</button>
	</div>
	<div class="col-md-6">
		{{Form::label('sedes', 'Filtrar por sede')}}
		{{Form::select('sedes', $sedes, null, array('class' => 'form-control', 'id' => 'sedes'))}}
	</div>	
	<div class="col-md-6">
		{{Form::label('status', 'Filtrar por status')}}
		{{Form::select('status', $status, null, array('class' => 'form-control', 'id' => 'status'))}}
	</div>
	@if($ponencias->count()>0)
	<div class="row" id="listado">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>Título de la ponencia</th>
					<th>Ponente</th>
					<th>Sedes postuladas</th>
				</tr>
			</thead>
			<tbody>
				@foreach($ponencias as $ponencia)
					<tr class="{{$ponencia->getClassCss()}}">
						<td> 
							{{$ponencia->titulo_de_la_ponencia}}
						</td>
						<td>{{$ponencia->usuario->nombres.' '.$ponencia->usuario->apellidos}}</td>
						<td>
							<ul>
							@foreach($ponencia->postulaciones()->get() as $postulacion)
								<li>{{$postulacion->sede->ciudad->nombre}}</li>
							@endforeach
							</ul>
						</td>
						<td>
							<a href="{{route('ponencias.show', $ponencia->id)}}" class="btn btn-default" data-toggle="tooltip" title="Ver detalles">
								<i class="fa fa-eye"></i>
							<a/>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>	
	<p>
		<span class="text-info"><strong>Admitida:</strong> si su ponencia fue admitida en el CNSL</span>,
		<span class="text-warning"><strong>En revisión:</strong> si su ponencia se encuentra bajo revisión</span>,
		<span class="text-danger"><strong>No procede:</strong> si su ponencia no fue admitida en el CNSL</span>
	</p>
	<div class="paginate">
		{{$ponencias->links()}}
	</div>
	@else
		<p>No ha registro ninguna ponencia aún</p>
	@endif

@stop