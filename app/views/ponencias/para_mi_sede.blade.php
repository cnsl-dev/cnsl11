@extends('layouts.admin')

@section('main')

	<h1>Ponencias postuladas para {{Auth::user()->organizador->sede->ciudad->nombre}}</h1>
	<a style="margin-bottom:1em" href="{{route('postulaciones-ponencias.laminas-completas')}}" class="btn btn-danger btn-lg">Descargar todas las las presentaciones</a>
	@if($postulaciones->count()>0)
	<div class="panel-group" id="postulaciones" role="tablist" aria-multiselectable="true">
	@foreach($postulaciones as $postulacion)
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingOne">
	      <h3 style="margin-top:.3em">
	        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$postulacion->id}}" aria-expanded="true" aria-controls="collapseOne">
	          {{$postulacion->ponencia->titulo_de_la_ponencia}}
	        </a>
	      </h3>
	      Ponente: {{$postulacion->ponencia->usuario->nombres.' '.$postulacion->ponencia->usuario->apellidos}}
	    </div>
	    <div id="collapse{{$postulacion->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
	      <div class="panel-body">
	      	<p><strong>Resumen:</strong></p>
	        <p>{{$postulacion->ponencia->resumen_de_la_ponencia}}</p>
	        <p><strong>Ciudad de procedencia:</strong> {{$postulacion->ciudad->nombre}}</p>
	        <p><strong>Duración:</strong> {{$postulacion->ponencia->duracion}} Minutos</p>
	      </div>
	    </div>
	  </div>
	@endforeach
	</div>
	<div class="paginate">
		{{$postulaciones->links()}}
	</div>
	@else
		<p>No ha registro ninguna ponencia aún</p>
	@endif

@stop