<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>Título de la ponencia</th>
			<th>Ponente</th>
			<th>Sedes postuladas</th>
		</tr>
	</thead>
	<tbody>
		@foreach($ponencias as $ponencia)
			<tr class="{{$ponencia->getClassCss()}}">
				<td> 
					{{$ponencia->titulo_de_la_ponencia}}
				</td>
				<td>{{$ponencia->usuario->nombres.' '.$ponencia->usuario->apellidos}}</td>
				<td>
					<ul>
					@foreach($ponencia->postulaciones()->get() as $postulacion)
						<li>{{$postulacion->sede->ciudad->nombre}}</li>
					@endforeach
					</ul>
				</td>
				<td>
					<a href="{{route('ponencias.show', $ponencia->id)}}" class="btn btn-default" data-toggle="tooltip" title="Ver detalles">
						<i class="fa fa-eye"></i>
					<a/>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>