<!DOCTYPE html>
<html lang="es">
<meta charset='utf-8'>
<head>
	<title>Ponencias</title>
</head>
<body>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Status</th>
				<th>Título de la ponencia</th>
				<th>Resumen de la ponencia</th>
				<th>Duración</th>
				<th>Eje Tematico</th>
				<th>Ponente</th>
				<th>Cédula</th>
				<th>Correo</th>
				<th>Télefono</th>
				<th>Resumen curricular del ponente</th>
				<th>Ciudad de origen del ponente</th>
			</tr>
		</thead>
		<tbody>
			@foreach($ponencias as $ponencia)
				<tr>
					<td>
						{{ $ponencia->status }}
					</td>
					<td> 
						{{$ponencia->titulo_de_la_ponencia}}
					</td>
					<td>
						{{$ponencia->resumen_de_la_ponencia}}
					</td>
					<td>
						{{ $ponencia->duracion }} minutos
					</td>
					<td>
						
					</td>
					<td>{{$ponencia->usuario->nombres.' '.$ponencia->usuario->apellidos}}</td>
					<td>
						{{$ponencia->usuario->dni}}
					</td>
					<td>
						{{ $ponencia->usuario->email }}
					</td>
					<td>
						{{ $ponencia->usuario->perfil->telefono }}
					</td>
					<td>
						{{ $ponencia->usuario->ponente->resumen_curricular }}
					</td>
					<td>
						@foreach($ponencia->postulaciones()->get() as $postulacion)
						<p>Sede: {{$postulacion->sede->ciudad->nombre}} - Salida:{{ $postulacion->ciudad->nombre }}</p>
						@endforeach
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>