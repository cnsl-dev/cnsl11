@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/modules/ponencias/modificar_status.js"></script>
<script type="text/javascript" src="/js/lib/bootstrap-dialog.min.js"></script>
@stop

@section('main')

	<h1>{{$ponencia->titulo_de_la_ponencia}}</h1>
	<div style="padding-bottom:1em">
		<a href="{{route('ponencias')}}" class="btn btn-default"><i class="fa fa-arrow-left"></i>   Regresar</a>		
	</div>
	<table class="table table-bordered">
		<tr>
			<th>Status</th>
			<td class="{{$ponencia->getClassCss()}}">
				<span id="status">
					{{$ponencia->status}}
				</span>
				{{Form::select('status', array('En revision'=>'En revisión', 'No procede'=>'No procede', 'Admitida'=>'Admitida'), $ponencia->status, array('style' => 'display: none;', 'id' => 'status-new'))}}
				<a href="#"
				   id="modificar-status"
				   class="btn btn-info"
				   data-toggle="tooltip"
				   title="Modificar Status"
				   >
					<i class="fa fa-pencil-square-o"></i>
				</a>
				<a href="#"
				   id="cancelar-cambios"
				   class="btn btn-default"
				   data-toggle="tooltip" title="Cancelar"
				   style="display:none"
				   >
					<i class="fa fa-arrow-left"></i>
				</a>
				<a href="#"
				   id="guardar-cambios"
				   class="btn btn-primary"
				   data-toggle="tooltip"
				   data-route="{{route('ponencias.status.update', $ponencia->id)}}"
				   data-token="{{csrf_token()}}"
				   title="Almacenar cambios"
				   style="display:none"
				   >
					<i class="fa fa-database"></i>
				</a>
			</td>
		</tr>
		<tr>
			<th>Resumen</th>
			<td>{{$ponencia->resumen_de_la_ponencia}}</td>
		</tr>
		<tr>
			<th>Laminas</th>
			<td><a href="{{ route('ponencias.laminas', $ponencia->id) }}">Descargar</a></td>
		</tr>
		<tr>
			<th>Duración</th>
			<td>{{$ponencia->duracion}} minutos</td>
		</tr>
		<tr>
			<th>Ponente</th>
			<td> 
				<p>{{$ponencia->usuario->nombres.' '.$ponencia->usuario->apellidos}}</p>
				<p>Cedula: {{$ponencia->usuario->dni}}</p>
				<p>Correo: {{$ponencia->usuario->email}}</p>
				<p>Télefono: {{$ponencia->usuario->perfil->telefono}}</p>
			</td>
		</tr>
	</table>
	<h2>Requisitos</h2>
	<table class="table table-bordered">
		<tr>
			<th>Necesita operar manualmente su equipo</th>
			<td>{{$ponencia->requisito->proximidad==1 ? 'Si' : 'No' }}</td>
		</tr>
		<tr>
			<th>Necesita conexión a internet</th>
			<td>{{$ponencia->requisito->conectividad==1 ? 'Si' : 'No' }}</td>
		</tr>
		<tr>
			<th>Requisitos de hardware</th>
			<td>{{$ponencia->requisito->hardware}}</td>
		</tr>
		<tr>
			<th>Requisitos de software</th>
			<td>{{$ponencia->requisito->software}}</td>
		</tr>
		<tr>
			<th>Otros requisitos</th>
			<td>{{$ponencia->requisito->otros}}</td>
		</tr>
	</table>
	<h2>Postulaciones</h2>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Sede</th>
				<th>Ciudad de salida</th>
			</tr>
		</thead>
		<tbody>
			@foreach($ponencia->postulaciones()->get() as $postulacion)
				<tr>
					<td>{{$postulacion->sede->ciudad->nombre}}</td>
					<td>{{$postulacion->ciudad->nombre}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div class="buttons-action">
		<a href="{{route('ponencias')}}" class="btn btn-default"><i class="fa fa-arrow-left"></i>  Regresar</a>		
	</div>
@stop