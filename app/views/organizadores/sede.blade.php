@extends('layouts.admin')


@section('more_js')
<script type="text/javascript" src="/js/lib/typeahead.bundle.js"></script>
<script type="text/javascript" src="/js/widgets/livesearch.widget.js"></script>
<script type="text/javascript" src="/js/modules/usuarios/livesearch.js"></script>
@stop

@section('main')

<dir class="row">
	<div class="col-md-6">
		<h2>{{$sede->ciudad->nombre}}</h2>
		<p>Inicio: {{$propiedades['inicio']}}</p>
		<p>Fin: {{$propiedades['fin']}}</p>
	</div>
	<div class="col-md-6">
		{{Form::open()}}
		{{Form::label('usuario', 'Username:')}}
		{{Form::text('usuario', null, array('class' => 'form-control typeahead', 'placeholder' => 'Escribe el nick del usuario...', 'id' => 'agregar'))}}
		{{Form::hidden('usuario_id', NULL, array('id' => 'usuario_id'))}}
		<table class="table">
			<tr>
				<th>Nombre:</th><td id="nombre"></td>
			</tr>
			<tr>
				<th>Apellido:</th><td id="apellido"></td>
			</tr>
			<tr>
				<th>Ciudad:</th><td id="ciudad"></td>
			</tr>
		</table>
		<div class="row buttons-action">
		<a class="btn btn-default" href="{{ route('evento.sedes.index') }}"><i class="fa fa-arrow-left"></i> Regresar</a>
		<button class="btn btn-primary" onclick="submit()"><i class="fa fa-database"></i> Asignar</button>
	</div>
		{{Form::close()}}
	</div>
</dir>

@stop