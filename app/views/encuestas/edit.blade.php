@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/modules/utils/addinput.js"></script>
<script type="text/javascript" src="/js/modules/scaffold/save.js"></script>
@stop

@section('main')

<h1>{{ $titlePage }}</h1>
<div class="message">

</div>
{{ Form::open(array('route' => array($route.'.update', $obj->id), 'method' => 'PATCH')) }}
	{{ $propiedades['eventos']['label'] }}
	{{ $propiedades['eventos']['form_control'] }}
	{{ $propiedades['titulo']['label'] }}
	{{ $propiedades['titulo']['form_control'] }}
	{{ $propiedades['descripcion']['label'] }}
	{{ $propiedades['descripcion']['form_control'] }}
	<h3>Opciones:</h3>
	{{ $propiedades['opciones']['label'] }}
	{{ $propiedades['opciones']['form_control'] }}
	<div id="opciones">
		{{ Form::text('opciones_1') }}
	</div>
		{{ Form::button('agregar', array('class' => 'button small', 'id' => 'agregar', 'data-container' => 'opciones')) }}
	<div class="row">
		{{ Form::button('submit', array('class' => 'button small', 'id' => 'submit'))}}
		{{ link_to_route($route.'.index', 'Regresar', null, array('class' => 'button small secondary')) }}
	</div>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


