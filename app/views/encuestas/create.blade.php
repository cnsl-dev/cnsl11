@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/modules/utils/addinput.js"></script>
<script type="text/javascript" src="/js/modules/scaffold/save.js"></script>
@stop

@section('main')

<h1>{{ $titlePage }}</h1>
<div class="message">
	
</div>
{{ Form::open(array('route' => $route.'.store')) }}
	{{ $propiedades['eventos']['label'] }}
	{{ $propiedades['eventos']['form_control'] }}
	{{ $propiedades['seleccion']['label'] }}
	{{ $propiedades['seleccion']['form_control'] }}
	{{ $propiedades['titulo']['label'] }}
	{{ $propiedades['titulo']['form_control'] }}
	{{ $propiedades['descripcion']['label'] }}
	{{ $propiedades['descripcion']['form_control'] }}
	<h3>Opciones:</h3>
	<div id="opciones">
		<div class="input-group">
			{{ Form::text('opciones_1', null, array('class' => 'form-control')) }}
			<span class="input-group-btn">
				<button class="btn btn-danger delete-option">
					<i class="fa fa-minus-square"></i>
				</button>
			</span>
		</div>
	</div>
		<a id="agregar" data-container="opciones" class="btn btn-success" href="#"><i class="fa fa-plus-circle"></i> Agregar otra opción</a>
	<div class="row">
		{{ Form::button('submit', array('class' => 'btn btn-primary', 'id' => 'submit'))}}
		{{ link_to_route($route.'.index', 'Regresar', null, array('class' => 'btn btn-default')) }}
	</div>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


