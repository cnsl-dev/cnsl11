@extends('auth.guest.account')

@section('form')
<h2>Recuperar contraseña</h2>
@if(Session::get('message'))
    <p>{{ Session::get('message') }}</p>
@endif
@if (Session::has('error'))
	<p>{{ Session::get('error') }}</p>
@endif
{{ Form::open(array('action' => 'RemindersController@postRemind')) }}
	<table class="table">
		<tbody>
			<tr>
				<th>{{ Form::label('email', 'Email') }}</th>
				<td>{{ Form::email('email', '', array('class' => 'form-control' )) }}</td>
			</tr>
			<tr>
				<th>{{HTML::image(Captcha::url())}}</th>
				<td>{{ Form::text('captcha', NULL,array('class' => 'form-control')) }}
					Ingrese el texto en la imagen de la izquierda
				</td>
			</tr>
		</tbody>
	</table>
	{{ Form::submit('Enviar', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}
@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif
@stop