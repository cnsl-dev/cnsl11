@extends('layouts.admin')

@section('main')

<h1>{{ $titlePage }}</h1>

{{ Form::open(array('route' => $route.'.store')) }}
	{{ Form::label('nombre', 'Nombre:') }}
	{{ Form::text('nombre', null, array('class' => 'form-control')) }}
	{{ Form::label('descripcion', 'Descripcion:') }}
	{{ Form::textarea('descripcion', null, array('class' => 'form-control')) }}
	<h2>Permisos</h2>
	<table class="table table-striped table-hover table-bordered">
		<thead>
			<th>Permiso</th>
			<th>Asignado</th>
		</thead>
		<tbody>
	@foreach($permisos as $nombre => $componente)
		<tr><td colspan="2"><h3>{{$nombre}}</h3></td></tr>
		@foreach($componente as $permiso)
			<tr>
				<td>
					{{ Form::label($permiso, $permiso, array('style' => 'display:inline')) }}
				</td>
				<td>
					{{ Form::checkbox('permisos[]', $permiso, null, array('id' => $permiso, 'style' => 'margin:0')) }}
				</td>
			</tr>
		@endforeach
	@endforeach
		</tbody>
	</table>
	<div class="row">
		{{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
		{{ link_to_route($route.'.index', 'Regresar', null, array('class' => 'btn btn-default')) }}
	</div>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


