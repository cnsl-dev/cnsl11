@extends('layouts.admin')

@section('base')
<h1>Bienvenidos al sistema de inscripción del CNSL</h1>
<p>A través de esta herramienta podrás inscribirte como participante en el Congreso Nacional de Software Libre.</p>
<p>Durante tu registro, estarás llenando un instrumento de participación (tipo encuesta), con el que obtendremos la condición y opinión de cada uno de ustedes, brindando una mejor perspectiva de los deseos y necesidades en las tecnologías libres del país, así como indicadores del proceso de migración e implementación de las tecnologías libres y sus aplicación social en Venezuela.</p>
<p>Por favor llene todos los datos que se le piden y ponga especial atención en aquellos que forman parte del Diagnóstico Participativo.</p>
<!-- <p>Para continuar, proceda a  <a href="{{route('find_cedula')}}">REGISTRARSE</a>.</p> -->
 <ul>
 	@if(Auth::guest())
 	<a class="btn btn-success btn-lg" href="{{route('login')}}">¿Ya tiene una cuenta?</a>
 	<a class="btn btn-danger btn-lg" href="{{route('remind')}}">¿Perdiste tu contraseña?</a>
 	@else
 	<a class="btn btn-success btn-lg" href="{{route('registros.mis_registros')}}">Registrarse para una sede</a>
 	<a class="btn btn-primary btn-lg" href="{{route('ponencias.create')}}">Proponer una ponencia</a>
 	@endif
 </ul>
@stop