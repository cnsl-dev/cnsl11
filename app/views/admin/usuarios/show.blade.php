@extends('layouts.admin')

@section('main')

<h1>{{ $usuario->username }}</h1>

	<p>
		<a class="btn btn-default" href="{{ route('auth.usuarios.index') }}">
			<i class="fa fa-arrow-left"></i> Regresar a la lista de todos los elementos
		</a>
	</p>

	<table class="table table-bordered">
		<tbody>
			<tr>
				<th>Username</th><td>{{ $usuario->username }}</td>
			</tr>
			<tr>
				<th>Email</th><td>{{ $usuario->email }}</td>
			</tr>
		</tbody>
	</table>
	{{ link_to_route('auth.usuarios.edit', 'Edit', array($usuario->id), array('class' => 'btn btn-primary')) }}
	{{ Form::open(array('method' => 'DELETE', 'route' => array('auth.usuarios.destroy', $usuario->id), 'class' => 'inline')) }}
        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
    {{ Form::close() }}

@stop