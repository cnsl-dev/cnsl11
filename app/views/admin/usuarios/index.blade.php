@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/modules/scaffold/delete.js"></script>
@stop

@section('main')

<h1>Usuarios</h1>

<p><a class="btn btn-success" href="{{ route('auth.usuarios.create') }}"><i class="fa fa-plus-circle"></i>
 Agregar nuevo usuario</a></p>

@if ($usuarios->count())
	<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th>Correo</th>
				<th>Activo</th>
				<th>Baneado</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($usuarios as $usuario)
				<tr>
					<td>{{{ $usuario->email }}}</td>
					<td>{{{ $usuario->activo }}}</td>
					<td>{{{ $usuario->baneado }}}</td>
                    <td>{{ link_to_route('auth.usuarios.edit', 'Edit', array($usuario->id), array('class' => 'btn btn-primary')) }}</td>
                    <td>
                        <a 
                        	data-id="{{ $usuario->id }}" 
                        	data-route="/auth/usuarios/{{ $usuario->id }}"
                        	href="#" 
                        	class="btn btn-danger deleteConfirm">
                        	Eliminar
                        </a> 
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<div class="modal fade" id="modalDelete" data-route="{{'auth.usuarios'}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
	        <h4 class="modal-title" id="myModalLabel">Confimación</h4>
	      </div>
	      <div class="modal-body">
	        ¿Esta seguro que desea borrar este registro?
	        <div id="dataObject"></div>
	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button class="btn btn-danger" id="deleteAccept">delete</button>
	      </div>
	    </div>
	  </div>
	</div>
@else
	No hay ningun usuario registrado
@endif

@stop
