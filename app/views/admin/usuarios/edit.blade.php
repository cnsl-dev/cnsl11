@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/chosen/chosen.css">
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/chosen.jquery.min.js" ></script>
<script type="text/javascript" src="/js/widgets/select.widget.js"></script>
@stop

@section('main')

<h1>Editar usuario ID:{{ $usuario->id }}</h1>

{{ Form::model($usuario, array('method' => 'PATCH', 'route' => array('auth.usuarios.update', $usuario->id))) }}
	<div class="row">
		{{ Form::label('nombres', 'Nombres:') }}
		{{ Form::text('nombres', null, array('class' => 'form-control')) }}
	</div>
	<div class="row">
		{{ Form::label('apellidos', 'Apellidos:') }}
		{{ Form::text('apellidos', null, array('class' => 'form-control')) }}
	</div>
	<div class="row">
		<div class="col-md-12">
			{{ Form::label('dni', 'Cedula:') }}
		</div>
		<div class="col-md-3">
			{{Form::select('nacionalidad', array('V'=>'V','E'=>'E','P'=>'P'), null, array('class'=>'form-control'))}}
		</div>
		<div class="col-md-9">
			{{ Form::text('dni', null, array('class' => 'form-control')) }}
		</div>
	</div>
	<div class="row">
		{{ Form::label('email', 'Correo electrónico:') }}
		{{ Form::text('email', null, array('class' => 'form-control')) }}
	</div>
	<div class="row">
		{{ Form::label('sexo', 'Sexo:') }}
		{{Form::select('sexo', array(0=>'Escoja una opción','Masculino'=>'Masculino','Femenino'=>'Femenino'), null, array('class' => 'form-control'))}}
	</div>
	<div class="row">
		{{Form::label('roles[]', 'Rol:')}}
		{{Form::select('roles[]', $roles, $usuario->roles->lists('id'), array('multiple' => 'multiple', 'data-placeholder' => 'Selecciona un rol', 'class' => 'form-control'))}}
	</div>
	<div class="row">
		{{ Form::label('password', 'Contraseña:') }}
		{{ Form::password('password', array('class' => 'form-control')) }}
	</div>
	<div class="row">
		{{ Form::label('password-repeat', 'Repita la contraseña:') }}
		{{ Form::password('password-repeat', array('class' => 'form-control')) }}
	</div>
	<div class="row">
		{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}
		{{ link_to_route('auth.usuarios.index', 'Regresar', array(), array('class' => 'btn btn-default')) }}
	</div>

{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif


@stop