@extends('layouts.admin')

@section('main')

<h1>{{ $titlePage }}</h1>

{{ Form::open(array('route' => $route.'.store')) }}
	@foreach($propiedades as $nombre_propiedad => $propiedad)
		@if($nombre_propiedad != 'id')
			<div class="row">
				{{ $propiedad['label'] }}
				{{ $propiedad['form_control'] }}
			</div>
		@endif
	@endforeach
	<div class="row buttons-action">
		<a class="btn btn-default" href="{{ route($route.'.index') }}"><i class="fa fa-arrow-left"></i> Regresar</a>
		<button class="btn btn-primary" onclick="submit()"><i class="fa fa-database"></i> Almacenar</button>
	</div>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop