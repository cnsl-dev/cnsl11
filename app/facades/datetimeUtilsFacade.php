<?php 

namespace H34\Facades;

use Illuminate\Support\Facades\Facade;

class datetimeUtilsFacade extends Facade
{
	
	protected static function getFacadeAccessor() { return 'datetimeUtils'; }

}