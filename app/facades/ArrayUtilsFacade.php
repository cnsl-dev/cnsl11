<?php 

namespace H34\Facades;

use Illuminate\Support\Facades\Facade;

class ArrayUtilsFacade extends Facade
{
	
	protected static function getFacadeAccessor() { return 'arrayUtils'; }

}