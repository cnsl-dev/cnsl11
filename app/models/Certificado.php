<?php

class Certificado extends BaseModel {
	
	protected $table = 'certificados';

	protected $primaryKey = 'registro_id';

	public $campo_principal = 'tipo';

	public static $rules = array(
								'registro_id' => 'required|exists:registros,id',
								'tipo' => 'required|in:Estudiante,Profesional',
								'impreso' => 'required|boolean'
							);

	protected $fillable = array('registro_id', 'tipo', 'impreso');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'registro' => array('modelo' => 'Registro'),
								'ingresos' => array('modelo' => 'Ingreso')
							);

	public function registro(){
		return $this->belongsTo('Registro');
	}

	public function ingresos(){
		return $this->belongsToMany('Ingreso');
	}



	public function deleteIfExists($id){
        $certificado = $this->find($id);
        
        if(! $certificado)
            return FALSE;

        if($ingreso = $certificado->ingresos()->first())
        	$ingreso->delete();

        $certificado->delete();

        return TRUE;
    }
}