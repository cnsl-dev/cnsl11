<?php 

use H34\Models\Observers\PreguntaObserver;

/**
* 
*/
class Pregunta extends BaseModel
{

	public static function boot(){
		parent::boot();

		Pregunta::observe(new PreguntaObserver);
	}

	protected $table = "preguntas";

	public $campo_principal = 'titulo';

	protected $fillable = array('titulo', 'seleccion', 'orden', 'encuesta_id');

	public static $rules = array(
			'titulo' => 'required|string_accent',
			'seleccion' => 'in:simple,multiple',
			'orden' => 'required|integer',
			'encuesta_id' => 'exists:encuestas,id'
		);

	/*
	 * Relaciones
	 */

	protected $relaciones = array(
								'opciones' => array('modelo' => 'Opcion'),
								'encuesta' => array('modelo' => 'Encuesta')
							);

	public function opciones(){
		return $this->hasMany('Opcion');
	}

	public function encuesta(){
		return $this->belongsTo('Encuesta');
	}

	public function respuestasParaUsuario($usuario_id){
		return $this->opciones()
					->join('respuestas', 'opciones.id', '=', 'respuestas.opcion_id')
					->where('respuestas.usuario_id', $usuario_id)
				   	->select('opciones.*')
				   	->get();
	}

}