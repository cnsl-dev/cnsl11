<?php

class EjeTematico extends \Eloquent {

	protected $table = 'ejes_tematicos';

	// Add your validation rules here
	public static $rules = array(
			'descripcion' => 'string_accent'
		);

	// Don't forget to fill this array
	protected $fillable = array('descripcion');

	public function ponencias(){
		return $this->belongsToMany('ponencias', 'eje_tematico_ponencia', 'eje_tematico_id', 'ponencia_id');
	}

}