<?php

class RazonExoneracion extends BaseModel {
	
	protected $table = 'razones_exoneraciones';

	public $campo_principal = 'titulo';

	public static $rules = array(
								'titulo' => 'required|alpha_accent',
								'texto_en_certificado' => 'required|alpha_accent'
							);

	protected $fillable = array('titulo','texto_en_certificado');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								//array()
							);
}