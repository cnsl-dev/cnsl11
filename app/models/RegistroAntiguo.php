<?php

class RegistroAntiguo extends BaseModel {
	
	protected $table = 'registros_antiguos';

	public $campo_principal = 'cedula';

	public static $rules = array(
								//...
							);

	protected $fillable = array();
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								//array()
							);


	public static function buscarPorCedula($cedula){
		$registro = RegistroAntiguo::where('cedula',$cedula)
									->get();

		if($registro->count()>0)
			return $registro->first();

		return FALSE;

	}
}