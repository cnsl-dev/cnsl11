<?php

class ConfigApp {

    /*
     *  Contruye el menu para cada usuario
     */
    public static function buildMenu(){
        if(!Session::has('menu')){
            $menu = array();
            foreach(ConfigApp::getComponentesId() as $componente_id){
                $funcionalidades = ConfigApp::getPermisosWithMenuByComponente($componente_id);
                $incluido_componente = false;
                foreach($funcionalidades as $funcionalidad){
                if(in_array($funcionalidad['permiso'], Auth::user()->permisos())){
                    if(!$incluido_componente){
                    $componente = ConfigApp::getComponente($componente_id);
                    $menu[(string)$componente_id] = array(
                                    'nombre' => (string)$componente[0]['nombre'],
                                    'menu' => (string)$componente[0]['menu'],
                                    'sub-menu' => array(
                                                    array(
                                                        'nombre' => (string)$funcionalidad[0]['nombre'],
                                                        'admin-menu' => (string)$funcionalidad[0]['admin-menu'],
                                                        'permiso' => (string)$funcionalidad[0]['permiso']
                                                    )
                                                  )
                                    );
                    $incluido_componente = true;
                    }else{
                        $menu[(string)$componente_id]['sub-menu'][] = array(
                                                        'nombre' => (string)$funcionalidad[0]['nombre'],
                                                        'admin-menu' => (string)$funcionalidad[0]['admin-menu'],
                                                        'permiso' => (string)$funcionalidad[0]['permiso']
                                                    );
                    }
                }
                }
            } 
            Session::set('menu', json_encode($menu));
        }
    }
    
    /*
     * Retorna los nombres de los componentes de la aplicación
     */
    public static function getComponentesNombre(){
        $app = self::load_file();
        return $app->xpath('//componente/@nombre');
    }
    
    /*
     * Retorna los IDs de los componentes de la aplicacipión
     */
    public static function getComponentesId(){
        $app = self::load_file();
        return $app->xpath('//componente/@id');
    }
    
    /*
     * Retorna un componente dado su id
     */
    public static function getComponente($componente_id){
        $app = self::load_file();
        return $app->xpath("//componente[@id='$componente_id']");
    }
    
    /*
     * Retorna la listas de permisos de cada funcionalidad asociada a un componente pasando
     * el id de este por parametro
     */
    public static function getPermisosByComponente($componente_id){
        $app = self::load_file();
        return $app->xpath("//componente[@id='$componente_id']/funcionalidad");
    }
    
    /*
     * Retorna los item de lo sub-menus a ser colocados en la barra lateral
     * del panel de administración
     */
    public static function getPermisosWithMenuByComponente($componente_id){
        $app = self::load_file();
        return $app->xpath("//componente[@id='$componente_id']/funcionalidad[@admin-menu]");
    }
    
    /*
     *  Retorna todos los permisos de la aplicación
     */
    public static function getPermisos(){
        $app = self::load_file();
        $permisos = array();
        $componentes = $app->xpath('//componente');
        foreach($componentes as $componente)
            foreach($componente as $funcionalidad)
                $permisos[(string)$componente['nombre']][] = (string)$funcionalidad['permiso'];
            
        return $permisos;
    }
    
    protected static function load_file(){
        return simplexml_load_file(app_path().'/config/app.xml');
    }
}
