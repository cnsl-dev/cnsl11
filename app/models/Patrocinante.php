<?php

class Patrocinante extends BaseModel {
	
	protected $table = 'patrocinantes';

	public $campo_principal='nombre';

	public static $rules = array(
								'nombre' => 'required|alpha_accent',
								'nacional' => 'boolean',
								'sede_id' => 'integer'
							);

	protected $fillable = array('nombre', 'nacional', 'sede_id');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'sede' => array('modelo' => 'Sede')
							);

	public function sede(){
		return $this->belongsTo('Sede');
	}

	public function beforeStore(&$input){
		parent::beforeStore($input);
		if(in_array('crear_patrocinantes_nacionales', Auth::user()->permisos())){
			$input['nacional'] = true;
		}elseif(in_array('crear_patrocinantes_locales', Auth::user()->permisos())){
			$input['nacional'] = false;
			$organizacion = Auth::user()->organizador();
			if($organizacion->count()>0)
				$input['sede_id'] = $organizacion->first()->sede_id;
			else{
				print Response::view('errors.403', array(), 403);
				exit();
			}
		}
	}
}