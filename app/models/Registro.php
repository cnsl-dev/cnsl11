<?php

class Registro extends BaseModel {
	
	protected $table = 'registros';

	public $timestamps = false;

	public $campo_principal = 'usuario_id';

	public static $rules = array(
								'usuario_id' => 'required|exists:usuarios,id',
								'sede_id' => 'required|exists:sedes,id',
								'fecha_registro' => 'required|date_format:Y-m-d H:i:s'
							);

	protected $fillable = array(
									'usuario_id', 
									'sede_id', 
									'fecha_registro', 
									'fecha_validacion',
									'fecha_modificacion'
								);
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'usuario' => array('modelo' => 'Usuario'),
								'sede' => array('modelo' => 'Sede')
							);

	public function usuario(){
		return $this->belongsTo('Usuario');
	}

	public function sede(){
		return $this->belongsTo('Sede');
	}

	public function certificado(){
		return $this->hasOne('Certificado');
	}

	public function exonerado(){
		return $this->hasOne('Exonerado');
	}


	/*
	 *	Busquedas
	 */
	public function registrosParaUsuario($id){
		return Registro::join('sedes', 'sedes.id', '=', 'registros.sede_id')
						->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
						->where('registros.usuario_id',$id)
						->where('eventos.activo', '1')
						->select('registros.*')
						->get();
	}

	public function registrosParaSede($id){
		return Registro::join('sedes', 'sedes.id', '=', 'registros.sede_id')
						->join('ciudades', 'ciudades.id', '=', 'sedes.ciudad_id')
						->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
						->where('registros.sede_id',$id)
						->where('eventos.activo', '1')
						->selectRaw('ciudades.nombre as ciudad, count(*) as registrados')
						->get();
	}

	public function registrosParaSedeActual(){
		return $this->registrosParaSede(Sede::actual()->id);
	}

	public function registrosPorSede(){
		return Registro::join('sedes', 'sedes.id', '=', 'registros.sede_id')
					   ->join('ciudades', 'ciudades.id', '=', 'sedes.ciudad_id')
					   ->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
					   ->where('eventos.activo', '1')
					   ->groupBy('sedes.id')
					   ->selectRaw('ciudades.nombre as ciudad, count(*) as registrados')
					   ->get();
	}

	public function registroDeUsuarioParaSede($usuario_id, $sede_id){
		return Registro::join('sedes', 'sedes.id', '=', 'registros.sede_id')
						->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
						->where('registros.sede_id',$sede_id)
						->where('registros.usuario_id',$usuario_id)
						->where('eventos.activo', '1')
						->select('registros.*')
						->get()
						->first()
						;
	}

	public function registroDeUsuarioParaSedeActual($usuario_id){
		return $this->registroDeUsuarioParaSede($usuario_id,Sede::actual()->id);
	}

	public function hayRegistroParaSedeUsuario($sede_id,$usuario_id){
		return $this->RegistroPorSedeUsuario($sede_id,$usuario_id)->count()>0;
	}
}