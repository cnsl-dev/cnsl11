<?php

class Respuesta extends BaseModel {
	
	protected $table = 'respuestas';

	public $campo_principal = 'opcion_id';

	public static $rules = array(
								'evento_id' => 'required|exists:eventos,id',
								'opcion_id' => 'required|exists:opciones,id',
								'usuario_id' => 'required|exists:usuarios,id'
							);

	protected $fillable = array('evento_id', 'opcion_id', 'usuario_id');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'evento' => array('modelo' => 'Evento'),
								'opcion' => array('modelo' => 'Opcion'),
								'usuario' => array('modelo' => 'Usuario')
							);

	public function evento(){
		return $this->belongsTo('Evento');
	}

	public function opcion(){
		return $this->belongsTo('Opcion');
	}

	public function usuario(){
		return $this->belongsTo('Usuario');
	}


	public function porPreguntasParaUsuario($usuario_id){
		return Respuesta::join('eventos', 'eventos.id', '=', 'respuestas.evento_id')
					   ->join('usuarios', 'usuarios.id', '=', 'respuestas.usuario_id')
					   ->join('opciones', 'opciones.id', '=', 'respuestas.opcion_id')
					   ->join('preguntas', 'preguntas.id', '=', 'opciones.pregunta_id')
					   ->where('eventos.id', Evento::activo()->id)
					   ->where('usuarios.id', $usuario_id)
					   ->select('preguntas.titulo', 'opciones.descripcion')
					   ->orderBy('preguntas.titulo')
					   ->get();
	}
}