<?php 

namespace H34\Models\Observers;

use \Sede;

/**
* IngresoObserver
*/
class ImpresoraObserver
{
	public function creating($model){
		if($sede_activa = Sede::activa())
			$model->sede_id = Sede::activa()->id;
	}

	public function created($model){
		// $model->donaciones()->save(new Donacion(array('')));
	}
}