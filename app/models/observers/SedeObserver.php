<?php 

namespace H34\Models\Observers;

use \Sede;

/**
* IngresoObserver
*/
class SedeObserver
{
	public function saving($model){
		if($model->actual == 1)
			Sede::noActualParaTodos();
	}
}