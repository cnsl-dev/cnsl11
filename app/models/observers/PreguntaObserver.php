<?php 

namespace H34\Models\Observers;

use \Evento;
use \Encuesta;
use \Config;

/**
* PreguntaObserver
*/
class PreguntaObserver
{
	public function creating($model){
		if($evento = Evento::activo()){
			if($evento->encuestas()->count()==0){
				$encuesta = new Encuesta(array(
						'titulo' => 'Encuesta '.Config::get('app.evento_nombre'),
						'descripcion' => 'Encuesta para asistentes al '.Config::get('app.evento_nombre')
					));
				$evento->encuestas()->save($encuesta);
			}else{
				$encuesta = $evento->encuestas()->first();
			}
			$model->encuesta_id = $encuesta->id;
		}
	}

	public function created($model){

	}
}