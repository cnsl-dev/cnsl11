<?php

class Plantilla extends BaseModel {
	
	protected $table = 'plantillas';

	public $campo_principal = 'descripcion';

	public static $rules = array(
								'descripcion' => 'alpha_accent',
								'activa' => 'required|boolean',
								'superior' => 'required|integer',
								'izquierdo' => 'required|integer',
								'tipo_x' => 'required|integer',
								'tipo_y' => 'required|integer',
								'ciudad_x' => 'required|integer',
								'ciudad_y' => 'required|integer',
								'fecha_x' => 'required|integer',
								'fecha_y' => 'required|integer',
								'organizador_x' => 'required|integer',
								'organizador_y' => 'required|integer',
								'impresora_id' => 'required|integer'
							);

	protected $fillable = array(
									'descripcion',
									'activa', 
									'superior', 
									'izquierdo', 
									'tipo_x', 
									'tipo_y', 
									'ciudad_x', 
									'ciudad_y', 
									'fecha_x', 
									'fecha_y', 
									'organizador_x', 
									'organizador_y', 
									'impresora_id');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'impresora' => array('modelo' => 'Impresora')
							);

	public function impresora(){
		return $this->belongsTo('Impresora');
	}
}