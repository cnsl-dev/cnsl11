<?php

class Ingreso extends BaseModel {
	
	protected $table = 'ingresos';

	public $campo_principal = 'descripcion';

	public static $rules = array(
								'tipo' => 'required|in:Efectivo,Deposito,Transferencia',
								'codigo_operacion' => 'required_if:tipo,Deposito,Transferencia',
								'descripcion' => 'required|string_accent',
								'monto' => 'required|float'
							);

	protected $fillable = array('tipo', 'codigo_operacion', 'descripcion', 'monto');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'donaciones' => array('modelo' => 'Donacion'),
								'certificados' => array('modelo' => 'Certificado')
							);

	public function donaciones(){
		return $this->belongsToMany('Donacion', 'donacion_ingreso', 'ingreso_id', 'donacion_id');
	}

	public function certificados(){
		return $this->belongsToMany('Certificado', 'certificado_ingreso', 'ingreso_id', 'certificado_id');
	}
}