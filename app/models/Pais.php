<?php 

class Pais extends BaseModel
{
	protected $table = 'paises';

	public $campo_principal = 'nombre';

	protected $fillable = array('nombre', 'visible');

	public static $rules = array(
								'nombre' => 'required|alpha_accent',
							);


	/*
	 * Relaciones
	 */
	protected $relaciones = array(	'departamentos' =>
									array(
										'modelo' => 'Departamento',
									),
							);

	public function departamentos(){
		return $this->hasMany('Departamento');
	}
}