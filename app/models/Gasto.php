<?php

class Gasto extends BaseModel {
	
	protected $table = 'gastos';

	public $campo_principal = 'monto';

	public static $rules = array(
								'tipo' => 'required|in:Regional,Nacional,Compartido',
								'monto' => 'required|float',
								'sede_id' => 'integer',
							);

	protected $fillable = array('tipo', 'monto', 'sede_id');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'sede' => array('modelo' => 'Sede')
							);

	public function sede(){
		return $this->belongsTo('Sede');
	}
}