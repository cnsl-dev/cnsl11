<?php

class Perfil extends BaseModel {
	
	protected $table = 'perfiles';

	protected $primaryKey = 'usuario_id';

	public $campo_principal = 'nombre';

	public static $rules = array(
			'avatar' 				=> 'mimes:jpg,jpeg,png,gif|max:512',
			'fecha_nacimiento'		=> 'date_format:d/m/Y',
			'twitter'				=> 'alpha_dash',
			'direccion'				=> 'string_accent',
			'telefono' 				=> 'alpha_num',
			'web' 					=> 'url',
			'profesion_u_oficio' 	=> 'alpha_accent',
			'organizacion' 			=> 'alpha_accent',
			'ciudad_id' 			=> 'exists:ciudades,id'
							);

	protected $fillable = array(
								'usuario_id',
								'avatar', 
								'fecha_nacimiento',
								'twitter', 
								'direccion',
								'telefono', 
								'web',
								'profesion_u_oficio',
								'organizacion',
								'ciudad_id'
							);
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'usuario' => array('modelo' => 'Usuario'),
								'ciudad' => array('modelo' => 'Ciudad')
							);


	public function usuario(){
		return $this->belongsTo('Usuario');
	}

	public function ciudad(){
		return $this->belongsTo('Ciudad');
	}


	public function buscarPorCedula($cedula){
		return Usuario::leftjoin('usuarios','usuarios.id', '=', 'perfiles.usuario_id')
						->where('perfiles.cedula', $cedula)
						->get();
	}


	public function beforeSave(&$input){
		if(isset($input['fecha_nacimiento']))
			$input['fecha_nacimiento'] = self::toDataBaseDate($input['fecha_nacimiento']);
	}
}