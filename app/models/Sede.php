<?php

use H34\Models\Observers\SedeObserver;

class Sede extends BaseModel {
	
	protected $table = 'sedes';

	public $campo_principal = 'id';

	public static $rules = array(
								'evento_id' => 'integer',
								'ciudad_id' => 'required|integer',
								'inicio' => 'date_format:d/m/Y',
								'fin' => 'date_format:d/m/Y',
								'limite_ponencias' => 'date_format:d/m/Y',
								'activo' => 'boolean',
								'actual' => 'boolean'
							);

	protected $fillable = array('inicio', 'fin', 'limite_ponencias','evento_id', 'ciudad_id', 'activo', 'actual');

	public static function boot(){
		parent::boot();

		Sede::observe(new SedeObserver);
	}

	/*
	 * Relaciones
	 */
	public function ciudad(){
		return $this->belongsTo('Ciudad');
	}

	public function evento(){
		return $this->belongsTo('Evento');
	}

	public function organizador(){
		return $this->hasOne('OrganizadorRegional');
	}

	protected $relaciones = array(
								'ciudad' =>
								array(
										'modelo' => 'Ciudad',
									),
								'evento' =>
								array(
										'modelo' => 'Evento',
									),
								'organizador' =>
								array(
										'modelo' => 'OrganizadorRegional',
									)
							);

	/*
	 *	Querys
	 */

	public function listarIdNombre(){
		$coleccion = Sede::join('ciudades', 'ciudades.id', '=', 'sedes.ciudad_id')
					->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
					->where('eventos.activo', 1)
					->select('sedes.id as id','ciudades.nombre as ciudad')
					->get();

		if($coleccion->count()==0)
			return array();

		return ArrayUtils::convertToList($coleccion, 'id', 'ciudad');
	}

	public function paraEventoActivo(){
		return Sede::join('eventos', 'eventos.id', '=', 'sedes.evento_id')
					->where('eventos.activo', 1)
					->select('sedes.*')
					->get();
	}

	public function paraEventoActivoParaSedesActivas(){
		return Sede::join('eventos', 'eventos.id', '=', 'sedes.evento_id')
					->where('eventos.activo', 1)
					->where('sedes.activas', 1)
					->select('sedes.*')
					->get();
	}

	public function paraEventoActivoParaSedesPostulables(){
		return Sede::join('eventos', 'eventos.id', '=', 'sedes.evento_id')
					->where('eventos.activo', 1)
					->where('sedes.activas', 1)
					->where('sedes.limite_ponencias', '>', (new DateTime())->format('Y-m-d'))
					->select('sedes.*')
					->get();
	}

	public function dondeUsuarioSeHaRegistrado($usuario_id){
		$coleccion = Sede::join('ciudades', 'ciudades.id', '=', 'sedes.ciudad_id')
					->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
					->join('registros', 'registros.sede_id', '=', 'sedes.id')
					->where('eventos.activo', 1)
					->where('registros.usuario_id' ,$usuario_id)
					->select('sedes.id as id','ciudades.nombre as ciudad')
					->get();

		if($coleccion->count()==0)
			return array();

		return ArrayUtils::convertToList($coleccion, 'id', 'ciudad');
	}

	public function dondeUsuarioNoSeHaRegistrado($usuario_id){
		return array_diff($this->listarIdNombre(), $this->dondeUsuarioSeHaRegistrado($usuario_id));
	}

	public function dondePonenciaSeHaPostulado($ponencia_id){
		$coleccion = Sede::join('ciudades', 'ciudades.id', '=', 'sedes.ciudad_id')
					->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
					->join('postulaciones_ponencias', 'postulaciones_ponencias.sede_id', '=', 'sedes.id')
					->where('eventos.activo', 1)
					->where('postulaciones_ponencias.ponencia_id' ,$ponencia_id)
					->select('sedes.id as id','ciudades.nombre as ciudad')
					->get();

		if($coleccion->count()==0)
			return array();

		return ArrayUtils::convertToList($coleccion, 'id', 'ciudad');
	}

	public function dondePonenciaNoSeHaPostulado($ponencia_id){
		return array_diff($this->listarIdNombre(), $this->dondePonenciaSeHaPostulado($ponencia_id));
	}

	public static function activa(){
		return Sede::join('eventos', 'eventos.id','=','sedes.evento_id')
					->where('eventos.activo', 1)
					->where('sedes.activo', 1)
					->get()
					->first();
	}

	public static function actual(){
		return Sede::join('eventos', 'eventos.id','=','sedes.evento_id')
					->where('eventos.activo', 1)
					->where('sedes.activo', 1)
					->where('sedes.actual', 1)
					->get()
					->first();
	}

	public function sedesActivasID(){
		return Sede::join('eventos', 'eventos.id','=','sedes.evento_id')
					->where('eventos.activo', 1)
					->where('sedes.activo',1)
					->select('id')
					->get()
					->toArray();
	}

	public static function noActualParaTodos(){
		Sede::where('actual', 1)->update(array('actual' => 0));
	}
}