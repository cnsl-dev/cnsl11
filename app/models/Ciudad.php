<?php

class Ciudad extends BaseModel {
	
	protected $table = 'ciudades';

	public $campo_principal = 'nombre';

	public static $rules = array(
								'nombre' => 'required|alpha_accent',
								'capital' => 'boolean',
								'visible' => 'required|boolean',
								'departamento_id' => 'integer'
							);

	protected $fillable = array('nombre', 'capital', 'visible', 'departamento_id');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'departamento' => 
								array(
										'modelo' => 'Departamento'
									)
							);

	public function departamento(){
		return $this->belongsTo('Departamento');
	}
}