 <?php 

 // use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Ponencia extends BaseModel {

	// use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
	
	protected $table = 'ponencias';

	public $campo_principal = 'titulo_de_la_ponencia';

	public static $rules = array(
								'titulo_de_la_ponencia' => 'required|string_accent',
								'resumen_de_la_ponencia' => 'required|string_accent',
								'laminas' => 'required|mimes:pdf,odp',
								'duracion' => 'required|integer|min:0',
							);

	public static $rules_update = array(
									'titulo_de_la_ponencia' => 'required|string_accent',
									'resumen_de_la_ponencia' => 'required|string_accent',
									'laminas' => 'mimes:pdf,odp',
									'duracion' => 'required|integer|min:0',
								);

	protected $fillable = array(
								'titulo_de_la_ponencia', 
								'resumen_de_la_ponencia', 
								'laminas',
								'duracion',
								'status',
								'usuario_id'
								);
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'usuario' => array('modelo' => 'Usuario'),
								'requisitos' => array('modelo' => 'RequisitosPonencia'),
							);

	public function usuario(){
		return $this->belongsTo('Usuario');
	}

	public function requisito(){
		return $this->hasOne('RequisitosPonencia');
	}

	public function postulaciones(){
		return $this->hasMany('PostulacionPonencia');
	}
	
	public function mensajes(){
		return $this->hasMany('MensajePonencia')->orderBy('enviado', 'des');
	}

	public function ejesTematicos(){
		return $this->belongsToMany('EjeTematico', 'eje_tematico_ponencia', 'ponencia_id', 'eje_tematico_id');
	}


	/*
	 *	Querys
	 */

	public function getPonenciasDeUsuario($usuario_id, $page=0){
		return Ponencia::join('usuarios', 'ponencias.usuario_id', '=', 'usuarios.id')
						->where('ponencias.usuario_id', $usuario_id)
						->select('ponencias.*')
						->paginate($page==0 ? Config::get('app.paginate') : $page);
	}

	public function getAll($page=0){
		return Ponencia::join('postulaciones_ponencias', 'postulaciones_ponencias.ponencia_id', '=', 'ponencias.id')
						->join('sedes', 'sedes.id', '=', 'postulaciones_ponencias.sede_id')
						->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
						->where('eventos.activo', 1)
						->distinct()
						->select('ponencias.*')
						->paginate($page==0 ? Config::get('app.paginate') : $page);
	}

	public function ponenciasParaSede($sede_id, $page=0){
		return Ponencia::join('postulaciones_ponencias', 'postulaciones_ponencias.ponencia_id', '=', 'ponencias.id')
						->join('sedes', 'sedes.id', '=', 'postulaciones_ponencias.sede_id')
						->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
						->where('eventos.activo', 1)
						->where('sedes.id', $sede_id)
						->select('ponencias.*')
						->paginate($page==0 ? Config::get('app.paginate') : $page);
	}

	public function ponenciasParaStatus($status, $page=0){
		return Ponencia::join('postulaciones_ponencias', 'postulaciones_ponencias.ponencia_id', '=', 'ponencias.id')
						->join('sedes', 'sedes.id', '=', 'postulaciones_ponencias.sede_id')
						->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
						->where('eventos.activo', 1)
						->where('ponencias.status', $status)
						->select('ponencias.*')
						->paginate($page==0 ? Config::get('app.paginate') : $page);
	}

	public function ponenciasParaSedeYParaStatus($sede_id, $status, $page=0){
		return Ponencia::join('postulaciones_ponencias', 'postulaciones_ponencias.ponencia_id', '=', 'ponencias.id')
						->join('sedes', 'sedes.id', '=', 'postulaciones_ponencias.sede_id')
						->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
						->where('eventos.activo', 1)
						->where('ponencias.status', $status)
						->where('sedes.id', $sede_id)
						->select('ponencias.*')
						->paginate($page==0 ? Config::get('app.paginate') : $page);
	}

	public function getClassCss(){
		switch ($this->status) {
			case 'En revision':
				return 'warning';
			case 'Admitida':
				return 'info';
			default:
				return 'danger';
		}
	}

	public function esEditable(){
		return $this->status == 'No procede';
	}
	

	public function mensajesSinLeer(){
		return $this->mensajes()
					->where('leido', 0);
	}

	public function mensajesRaiz(){
		return $this->mensajes()
					->where('respuesta', NULL);
	}

	public function mensajesDeAutorPonencia(){
		return $this->mensajes()
					->where('emisor', $this->usuario_id);
	}

	public function mensajesParaAutorPonencia(){
		return $this->mensajes()
					->where('receptor', $this->usuario_id);
	}
}