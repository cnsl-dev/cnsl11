<?php

class ResponsabilidadRegional extends BaseModel {
	
	protected $table = 'responsabilidades_regionales';

	public $campo_principal = 'nombre';

	public static $rules = array(
								'nombre' => 'required|alpha_accent'
							);

	protected $fillable = array('nombre');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								//array()
							);

	public function organizaciones(){
		return $this->hasMany('OrganizadorRegional');
	}
}