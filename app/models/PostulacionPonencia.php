<?php

class PostulacionPonencia extends BaseModel {
	
	protected $table = 'postulaciones_ponencias';

	public $campo_principal = 'ponencia_id';

	public static $rules = array(
								'sede_id' => 'required|exists:sedes,id',
								'ponencia_id' => 'required|exists:ponencias,id',
								'ciudad_id' => 'required|exists:ciudades,id',
								'aceptada' => 'required|boolean'
							);

	protected $fillable = array('sede_id', 'ponencia_id', 'ciudad_id', 'aceptada');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'sede' => array('modelo' => 'Sede'),
								'ponencia' => array('modelo' => 'Ponencia'),
								'ciudad_id' => array('modelo' => 'Ciudad', 'campo_relacion' => 'ciudad_id')
							);

	public function sede(){
		return $this->belongsTo('Sede');
	}

	public function ponencia(){
		return $this->belongsTo('Ponencia');
	}

	public function ciudad(){
		return $this->belongsTo('Ciudad', 'ciudad_id');
	}

	public function postulacionesParaSede($sede_id, $page=0){
		return PostulacionPonencia::join('ponencias', 'postulaciones_ponencias.ponencia_id', '=', 'ponencias.id')
									->join('sedes', 'sedes.id', '=', 'postulaciones_ponencias.sede_id')
									->join('eventos', 'eventos.id', '=', 'sedes.evento_id')
									->where('eventos.activo', 1)
									->where('sedes.id', $sede_id)
									->where('ponencias.status', 'Admitida')
									->select('postulaciones_ponencias.*')
									->paginate($page==0 ? Config::get('app.paginate') : $page);
	}
}