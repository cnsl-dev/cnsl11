<?php

// use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
// use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Usuario extends BaseModel implements UserInterface, RemindableInterface {

	// use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuarios';

	/**
	 * Campo principal sobre el cual actuaran las operaciones de BaseModel.
	 *
	 * @var string
	 */
	public $campo_principal = 'email';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token', 'confirmation_code');


	public static $rules = array(
		'nombres'			=> 'required|alpha_accent',
		'apellidos'			=> 'required|alpha_accent',
		'dni'				=> 'required|alpha_accent',
		'sexo'				=> 'required|in:Masculino,Femenino',
        'email'             => 'required|email|unique:usuarios,email',
        'password'          => 'required|alpha_accent|min:6',
        'password-repeat'   => 'required|same:password',
        'captcha'			=> 'required|captcha'
	);

	protected $fillable = array(
									'nombres', 
									'apellidos', 
									'sexo', 
									'dni', 
									'email', 
									'password', 
									'activo',
									'confirmation_code',
									'registrado_en_linea',
									'por_confirmar_en_linea'
								);

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}


	public function permisos(){
	    return Permiso::join('roles', 'roles.id', '=', 'permisos.rol_id')
			  ->join('rol_usuario', 'roles.id', '=', 'rol_usuario.rol_id')
			  ->join('usuarios', 'usuarios.id', '=', 'rol_usuario.usuario_id')
			  ->select('permiso')
			  ->distinct()
			  ->where('usuarios.id', $this->id)
			  ->lists('permiso');
	}


	public function roles(){
		return $this->belongsToMany('Rol', 'rol_usuario', 'usuario_id', 'rol_id');
	}

	public function organizador(){
		return $this->hasOne('OrganizadorRegional');
	}

	public function ponente(){
		return $this->hasOne('Ponente');
	}

	public function perfil(){
		return $this->hasOne('Perfil');
	}

	public function respuestas(){
		return $this->hasMany('Respuesta');
	}

	public function registros(){
		return $this->hasMany('Registro');
	}

	
	/*
	|---------------------------------------------------------------------------------------
	|	Busqueda
	|---------------------------------------------------------------------------------------
	|	
	|	Diferentes criterio de busqueda sobre los usuarios
	|
	*/

	public function f1nd($str){
		return Usuario::leftjoin('perfiles','usuarios.id', '=', 'perfiles.usuario_id')
						->leftjoin('ciudades', 'ciudades.id', '=', 'perfiles.ciudad_id')
						->where('usuarios.nombres', 'like', '%'.$str.'%')
						->selectRaw(
									   'concat(usuarios.nombres, ", ", usuarios.apellidos) as value'
									 . ',usuarios.id as id'
									 . ',usuarios.nombres as nombres'
									 . ',usuarios.apellidos as apellidos'
									 . ',ciudades.nombre as ciudad'
									)
						->get();
	}

	public function buscarPorCedula($cedula){
		$usuario = Usuario::where('dni', $cedula)
							->get();
		if($usuario->count()>0)
			return $usuario->first();

		return FALSE;
	}

	public function buscarPorEmail($email){
		$usuario = Usuario::where('email', $email)
							->get();
		if($usuario->count()>0)
			return $usuario->first();

		return FALSE;
	}

	/*
	|---------------------------------------------------------------------------------------
	|	Status
	|---------------------------------------------------------------------------------------
	|	
	|	Estado del usuario {activo|baneado|inactivo}
	|
	*/

	public function status(){
		if ($this->activo) {
			return 'activo';
		} elseif($this->baneado) {
			return 'baneado';
		} else{
			return 'inactivo'; // --> por confirmar
		}
		
	}

	public function esOrganizador(){
		return is_object($this->organizador()->first());
	}

	public function esPonente(){
		return is_object($this->ponente()->first());
	}

	public function respondioEncuesta(){
		return $this->respuestas()
					->join('eventos', 'eventos.id', '=', 'respuestas.evento_id')
					->where('eventos.id', Evento::activo()->id)
					->get()
					->count()>0;
	}

	public function estaRegistradoParaSede($sede_id){
		return $this->registros()
					->where('sede_id', $sede_id)
					->get()
					->count()>0;
	}

	public function camposCompletos(){
		return (   isset($this->dni) 
				&& isset($this->email)
				&& isset($this->perfil->telefono)
				&& isset($this->perfil->avatar)
				&& isset($this->perfil->ciudad_id)
				);
	}

	public function primerNombre(){
		$nombres = explode(' ', $this->nombres);
		return $nombres[0];
	}

	public function primerApellido(){
		$apellidos = explode(' ', $this->apellidos);
		return $apellidos[0];
	}

}
