<?php

class Opcion extends BaseModel {
	
	protected $table = 'opciones';

	public $campo_principal = 'descripcion';

	public static $rules = array(
								'descripcion' => 'required|alpha_accent',
								'orden' => 'required|integer',
								'pregunta_id' => 'required|integer'
							);

	protected $fillable = array('descripcion', 'orden', 'pregunta_id');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'pregunta' =>
								array(
									'modelo' => 'Pregunta',
								)
							);


	public function pregunta(){
		return $this->belongsTo('Pregunta');
	}

	public function respuestas(){
		return $this->hasMany('Respuesta');
	}
}