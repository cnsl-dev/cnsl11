<?php

class Encuesta extends BaseModel {

	protected $table = "encuestas";

	public $campo_principal = 'titulo';

	protected $fillable = array('titulo', 'descripcion');

	public static $rules = array(
			'titulo' => 'required|string_accent',
			'descripcion' => 'string_accent',
		);

	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'preguntas' => array('modelo' => 'Pregunta'),
								'eventos' => array('modelo' => 'Evento')
							);

	public function preguntas(){
		return $this->hasMany('Pregunta');
	}

	public function eventos(){
		return $this->belongsToMany('Evento');
	}

	public function afterSave(&$input){

		// if(isset($input['eventos']) && count($input['eventos'])>0)
		// 	$this->eventos()->sync($input['eventos']);
		
		// $opciones = array();
		// foreach ($input as $key => $value) {
		// 	if(strpos($key, 'opciones')!==false)
		// 		$opciones[] = new Opcion(array('descripcion' => $value));
		// }
		// if(count($opciones)>0)
		// 	$this->opciones()->saveMany($opciones);
	}

}