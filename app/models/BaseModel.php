<?php
class BaseModel extends \Eloquent {

    protected $relaciones = array();

    /**
     *
     *  Devuelve todos los nombres y tipos de los campos de la tabla de la base de datos
     *
     */
    public function getAllColumnsNames()
    {
        switch (DB::connection()->getConfig('driver')) {
            case 'pgsql':
                $query = "SELECT column_name FROM information_schema.columns WHERE table_name = '".$this->getTable()."'";
                $column_name = 'column_name';
                $reverse = true;
                break;

            case 'mysql':
                $query = 'SHOW COLUMNS FROM '.$this->getTable();
                $column_name = 'Field';
                $reverse = false;
                break;

            case 'sqlsrv':
                $parts = explode('.', $this->getTable());
                $num = (count($parts) - 1);
                $table = $parts[$num];
                $query = "SELECT column_name FROM ".DB::connection()->getConfig('database').".INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'".$table."'";
                $column_name = 'column_name';
                $reverse = false;
                break;

            default: 
                $error = 'Database driver not supported: '.DB::connection()->getConfig('driver');
                throw new Exception($error);
                break;
        }

        $columns = array();

        foreach(DB::select($query) as $column)
        {
            $columns[$column->$column_name] = $column->Type;
        }

        if($reverse)
        {
            $columns = array_reverse($columns);
        }

        //elimina los campos ocultos
        self::deleteByKey($columns, $this->getHidden());

        return $columns;
    }


    /**
     *
     *  Devuelve todos los valores visibles asi como las relaciones para un objeto
     * de esta clase
     *
     */
    public function getVisibles(){
        return $this->getAttributesVisibles() + $this->getRelaciones();
    }


    /**
     *
     *  Devuelve todos los atributos del modelo en una etiqueta de formulario conveniente
     *
     */
    public function getVisiblesForm(){
        return $this->getAttributesForm() + $this->getRelacionesForm();
    }


    /**
     *
     *  Devuelve solo los valores visibles de la clase
     *
     */
    public function getAttributesVisibles(){
        $atributos = $this->getAttributes();
        self::deleteByKey($atributos, $this->getHidden());
        foreach ($atributos as $field_name => $atributo) {
            if(strpos($field_name, '_id')){
                unset($atributos[$field_name]);
            }
        }
        $tipos = $this->getAllColumnsNames();
        foreach ($tipos as $field_name => $tipo) {
            if (strpos($tipo, 'tinyint(1)')!==false)
              $atributos[$field_name] = $atributos[$field_name] ?
                                          '<i class="fa fa-check text-success"></i>' :
                                          '<i class="fa fa-times text-danger"></i>';
            elseif(strpos($tipo, 'datetime')!==false)
              $atributos[$field_name] = date('d/m/Y H:i',strtotime($atributos[$field_name]));
            elseif(strpos($tipo, 'date')!==false)
              $atributos[$field_name] = date('d/m/Y',strtotime($atributos[$field_name]));
        }
        return $atributos;
    }


    /**
     *
     *  Devuelve los atributos visible en una etiqueta de formulario conveniente
     *
     */
    public function getAttributesForm(){
        $valores = $this->getAttributes();
        $tipos = $this->getAllColumnsNames();
        self::deleteByKey($tipos, $this->getHidden());

        foreach ($tipos as $field_name => $tipo) {

            if(strpos($field_name, '_id')===false){ // elimina las claves foraneas
                $valor = count($valores)>0 ? $valores[$field_name] : null;

                $atributos[$field_name] = array(
                        'label' => Form::label(
                                            $field_name, 
                                            ucfirst(str_replace('_', ' ',$field_name)).':'
                                        )
                    );

                if (strpos($tipo, 'tinyint(1)')!==false) {

                    $atributos[$field_name]['form_control'] = Form::checkbox($field_name, 1, $valor==1);

                }elseif(strpos($tipo, 'int')!==false
                     || strpos($tipo, 'float')!==false 
                     || strpos($tipo, 'numeric')!==false 
                     || strpos($tipo, 'double')!==false){

                    $atributos[$field_name]['form_control'] = Form::input('number', $field_name, $valor, array('class' => 'form-control'));

                }elseif(strpos($tipo, 'dec')!==false){

                    $atributos[$field_name]['form_control'] = Form::input('number', $field_name, $valor, array('class' => 'form-control', 'step' => 'any'));

                }elseif(strpos($tipo, 'var')!==false){

                    $atributos[$field_name]['form_control'] = Form::input('text', $field_name, $valor, array('class' => 'form-control'));

                }elseif(strpos($tipo, 'text')!==false){

                    $atributos[$field_name]['form_control'] = Form::textarea($field_name, $valor, array('class' => 'form-control'));

                }elseif(strpos($tipo, 'datetime')!==false){
                    $valor = is_null($valor) ? $valor : date("d/m/Y H:i", strtotime($valor));
                    $atributos[$field_name]['form_control'] = Form::input('text', $field_name, $valor, array('class' => 'form-control datetimepicker'));                    

                }elseif(strpos($tipo, 'date')!==false){
                    $valor = is_null($valor) ? $valor : date("d/m/Y", strtotime($valor));
                    $atributos[$field_name]['form_control'] = Form::input('text', $field_name, $valor, array('class' => 'form-control datepicker'));

                }elseif(strpos($tipo, 'enum')!==false){

                    $opciones = $this->enumToArray($tipo);
                    $atributos[$field_name]['form_control'] =  Form::select($field_name, $opciones, $valor, array('class' => 'form-control'));
                    
                }
            }
        }

        return $atributos;
    }


    /**
     *
     * Devuelve los valores para los campos de relación de la clase
     *
     */
    public function getRelaciones(){
        $atributos = array();
        foreach ($this->relaciones as $relation_name => $relacion) {
            $obj = (new ReflectionClass($relacion['modelo']))->newInstance();
            $metodoReflexionado = new ReflectionMethod(get_class($this), $relation_name);
            $asociacion = $metodoReflexionado->invoke($this);

            /*
             *  Conseguir la clase del metodo de la relacion BelongsTo, HasOne, HasMany, BelongsToMany...
             */
            $rol = explode('\\', get_class($asociacion));
            $rol = $rol[count($rol)-1];

            $campo_principal = isset($obj->campo_principal) ? $obj->campo_principal : 'id';

            /*
             *  En funcion del tipo de relacion se escoje el elemento del formulario que se mostrara
             */
            switch ($rol) {
                case 'BelongsTo':
                case 'HasOne':
                    $atributos[$relation_name] = $asociacion->count()>0 ? $asociacion->first()->{$campo_principal} : null;
                    break;
                
                default:
                    $atributos[$relation_name] = $asociacion->count()>0 ? $asociacion->lists($campo_principal) : null;
                    break;
            }
        }

        return $atributos;
    }


    /**
     *
     * Devuelve los campos de relación de la clase con una etiqueta de formulario conveniente
     *
     */
    public function getRelacionesForm(){
        $atributos = array();
        foreach ($this->relaciones as $relation_name => $relacion) {

            $obj = (new ReflectionClass($relacion['modelo']))->newInstance();
            $metodoReflexionado = new ReflectionMethod(get_class($this), $relation_name);
            $asociacion = $metodoReflexionado->invoke($this);

            /*
             *  Conseguir la clase del metodo de la relacion BelongsTo, HasOne, HasMany, BelongsToMany...
             */
            $rol = explode('\\', get_class($asociacion));
            $rol = $rol[count($rol)-1];

            /*
             *  Etiqueta del campo de la relacion
             */
            $atributos[$relation_name] = array(
                        'label' => Form::label(
                                        $relation_name, 
                                        ucfirst(str_replace('_', ' ',$relation_name)).':'
                                    )
                    );

            /*
             *  En funcion del tipo de relacion se escoge el elemento del formulario que se mostrara
             */
            switch ($rol) {
                case 'BelongsTo':
                    $atributos[$relation_name]['form_control'] = Form::select(
                                                                                isset($relacion['campo_relacion']) ? $relacion['campo_relacion'] : strtolower($relacion['modelo']).'_id', 
                                                                                $obj->lists(isset($obj->campo_principal) ? $obj->campo_principal : 'id','id'), 
                                                                                $asociacion->count()>0 ? $asociacion->first()->id : null,
                                                                                array('class' => 'form-control')
                                                                            );
                    break;

                case 'HasOne':
                    $atributos[$relation_name]['form_control'] = Form::select(
                                                                                $relation_name, 
                                                                                isset($obj->campo_principal) ? $obj->lists($obj->campo_principal, $obj->campo_principal) : array('0' => 'debe definir campo_principal'), 
                                                                                $asociacion->count()>0 ? $asociacion->first()->id : null,
                                                                                array('class' => 'form-control')
                                                                            );
                    break;
                
                default:
                    $atributos[$relation_name]['form_control'] = Form::select(
                                                                                $relation_name.'[]', 
                                                                                $obj->lists(isset($obj->campo_principal) ? $obj->campo_principal : $obj->getKey(), $obj->getKey()), 
                                                                                is_object($asociacion) && $asociacion->count()>0 ? $asociacion->lists('id') : null,
                                                                                array('multiple' => 'multiple', 'id' => $relation_name, 'class' => 'form-control')
                                                                            ); 
                    break;
            }
        }

        return $atributos;
    }

    /********************************************************
     *  Operación a realzar antes o despues de la creación, 
     * actualización o eliminación del objeto
     ********************************************************/

    public function beforeSave(&$input){}

    public function afterSave(&$input){}

    public function beforeStore(&$input){
        $this->beforeSave($input);
    }

    public function afterStore(&$input){
        $this->afterSave($input);
    }

    public function beforeUpdate(&$input){
        $this->beforeSave($input);
    }

    public function afterUpdate(&$input){
        $this->afterSave($input);
    }

    public function beforeDelete(){}

    public function afterDelete(){}


   

    /*****************************************************************
     *                            Utilidades
     *****************************************************************/


    /*
     *  A partir de la descripción de la base de datos tipo enum
     * devuelve los valores que puede tomar este campos en un array
     */
    public function enumToArray($enum){
        $enum = str_replace('enum(\'', '', $enum);
        $enum = str_replace('\')', '', $enum);
        $enum = explode('\',\'', $enum);
        $opciones = array(0 => '-- seleccione una opción --');
        foreach ($enum as $key => $value) {
            $opciones[$value] = $value;
        }

        return $opciones;
    }


    /**
     *
     *
     *
     */
    public static function deleteByKey(&$data, Array $keys){
        if(is_object($data)){
            $array = (array) json_decode(json_encode($data));
            foreach ($array as $key => $value) {
                if(in_array($key, $keys))
                    unset($data[$key]);
            }
        }elseif (is_array($data)) {
            foreach ($data as $key => $value) {
                if(in_array($key, $keys))
                    unset($data[$key]);
            }
        }
    }


    /*
     *
     * Convierte las fechas de d/m/Y a Y-m-d
     *
     */

    public static function toDataBaseDate($date){
      return DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    /*
     *
     * Convierte las fechas de d/m/Y H:i a Y-m-d H:i
     *
     */

    public static function toDataBaseDateTime($date){
      return DateTime::createFromFormat('d/m/Y H:i', $date)->format('Y-m-d H:i');
    } 


    public function deleteIfExists($id){
        $registro = $this->find($id);
        
        if(! $registro)
            return FALSE;

        $registro->delete();

        return TRUE;
    }

}