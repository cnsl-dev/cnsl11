<?php

class RequisitosPonencia extends BaseModel {
	
	protected $table = 'requisitos_ponencia';

	public $campo_principal = 'ponencia_id';

	public static $rules = array(
								'ponencia_id' => 'exists:ponencias,id',
								'hardware' => 'string_accent',
								'software' => 'string_accent',
								'conectividad' => 'required|boolean',
								'proximidad' => 'required|boolean',
								'otros' => 'string_accent'
							);

	protected $fillable = array('ponencia_id', 'hardware', 'software', 'conectividad', 'proximidad', 'otros');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'ponencias' => array('modelo' => 'Ponencia'),
							);

	public function ponencia(){
		return $this->belongsTo('Ponencia');
	}
}