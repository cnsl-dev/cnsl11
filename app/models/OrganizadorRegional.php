<?php

class OrganizadorRegional extends BaseModel {
	
	public $campo_principal = 'usuario_id';
	
	protected $table = 'organizadores_regionales';

	protected $primaryKey = 'usuario_id';

	public static $rules = array(
								'usuario_id' => 'required|unique:organizadores_regionales,usuario_id|exists:usuarios,id',
								'sede_id' => 'exists:sedes,id',
								'responsabilidad_id' => 'exists:responsabilidades_regionales,id'
							);

	protected $fillable = array('usuario_id', 'sede_id', 'responsabilidad_id');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'responsabilidad' => array('modelo' => 'ResponsabilidadRegional', 'campo_relacion' => 'responsabilidad_id'),
								'usuario' => array('modelo' => 'Usuario'),
								'sede' => array('modelo' => 'Sede')
							);

	public function responsabilidad(){
		return $this->belongsTo('ResponsabilidadRegional');
	}

	public function usuario(){
		return $this->belongsTo('Usuario');
	}

	public function sede(){
		return $this->belongsTo('Sede');
	}

	public function deleteOrganizadorSede($id){
		OrganizadorRegional::where('sede_id',$id)
							->where('responsabilidad_id',1)
							->delete();
	}
}