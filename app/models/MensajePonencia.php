<?php

class MensajePonencia extends BaseModel {
	
	protected $table = 'mensajes_ponencias';

	public $campo_principal = 'mensaje';

	public $timestamps = false;

	public static $rules = array(
								'ponencia_id' => 'required|exists:ponencias,id',
								'mensaje' => 'required|string_accent',
								'emisor' => 'required|exists:usuarios,id',
								'receptor' => 'required|exists:usuarios,id',
								'respuesta' => 'exists:mensajes_ponencias,id',
								'leido' => 'required|boolean'
							);

	protected $fillable = array(
									'ponencia_id',
									'mensaje', 
									'emisor',
									'receptor',
									'respuesta',
									'leido', 
									'enviado'
								);
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'ponencia' => array('modelo' => 'Ponencia')
							);


	public function ponencia(){
		return $this->belongsTo('Ponencia');
	}

	public function emisor(){
		return $this->belongsTo('Usuario', 'emisor');
	}

	public function receptor(){
		return $this->belongsTo('Usuario', 'receptor');
	}

	public function responde(){
		return $this->belongsTo('MensajePonencia', 'respuesta');
	}

	public function respuestas(){
		return $this->hasMany('MensajePonencia', 'respuesta');
	}
	
}