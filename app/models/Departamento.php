<?php

class Departamento extends BaseModel {

	protected $table = 'departamentos';

	public $campo_principal = 'nombre';

	public static $rules = array(
			'nombre' => 'required|alpha_accent',
			'visible' => 'boolean',
			'pais_id' => 'exists:paises,id'
		);

	protected $fillable = array('nombre', 'iso_3166-2', 'visible', 'pais_id');

	/*
	 * Relaciones
	 */
	protected $relaciones = array(	'pais' =>
									array(
										'modelo' => 'Pais', 
										'campo_principal' => 'nombre',
									),
									'ciudades' =>
									array(
										'modelo' => 'Ciudad',
										'campo_principal' => 'nombre',
									),
							);

	public function pais(){
		return $this->belongsTo('Pais', 'pais_id');
	}

	public function ciudades(){
		return $this->hasMany('Ciudad');
	}
}