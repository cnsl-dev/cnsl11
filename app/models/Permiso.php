<?php

class Permiso extends \Eloquent {
	protected $table = 'permisos';

	protected $fillable = ['rol_id', 'permiso'];
}