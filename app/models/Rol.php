<?php

class Rol extends BaseModel {

	/**************************************************************
	 *															  *
	 *					Atributos de configuración 				  *
	 *															  *
	 **************************************************************/

	protected $table = 'roles';

	public $campo_principal = 'nombre';

	public static $rules = array(
		'nombre' => 'required'
	);

	protected $fillable = array('nombre', 'descripcion');

	/**************************************************************
	 *															  *
	 *					  	   Relaciones 						  *
	 *															  *
	 **************************************************************/

	public function usuarios(){
		return $this->belongsToMany('User', 'rol_usuario', 'rol_id', 'usuario_id');
	}

	public function permisos(){
		return $this->hasMany('Permiso');
	}


	/**************************************************************
	 *															  *
	 *		 Operaciones de actualización de las relaciones 	  *
	 *															  *
	 **************************************************************/

	
	//	Agregar los permisos del rol luego de ser creado 	
	public function afterStore(&$input){
		parent::afterStore($input);
		foreach ($input['permisos'] as $key => $permiso) {
			Permiso::create(array('rol_id' => $this->id, 'permiso' => $permiso));
		}
	}

	
	//	Modificar los permisos del rol luego de ser actualizado
	public function afterUpdate(&$input){
		Permiso::where('rol_id', $this->id)->delete();
		foreach ($input['permisos'] as $key => $permiso) {
			Permiso::create(array('rol_id' => $this->id, 'permiso' => $permiso));
		}
	}

}