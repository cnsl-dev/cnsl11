<?php

class Impresora extends BaseModel {
	
	protected $table = 'impresoras';

	public $campo_principal = 'identificador';

	public static $rules = array(
								'identificador' => 'required|alpha_accent',
								'sede_id' => 'exists:sedes,id',
								'activa' => 'boolean',
								'modelo_id' => 'required|exists:modelos_impresoras,id'
							);

	protected $fillable = array('identificador', 'sede_id', 'activa', 'modelo_id');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'modelo' => array('modelo' => 'ModeloImpresora', 'campo_relacion' => 'modelo_id')
							);

	public function modelo(){
		return $this->belongsTo('ModeloImpresora');
	}
}