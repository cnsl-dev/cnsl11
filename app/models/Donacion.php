<?php

class Donacion extends BaseModel {
	
	protected $table = 'donaciones';

	public $campo_principal = 'descripcion';

	public static $rules = array(
								'descripcion' => 'required|string_accent',
								'patrocinante_id' => 'required|exists:patrocinantes,id'
							);

	protected $fillable = array('descripcion', 'patrocinante_id');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'patrocinante' => array('modelo' => 'Patrocinante'),
								'ingresos' => array('modelo' => 'Ingreso'),
							);

	public function patrocinante(){
		return $this->belongsTo('Patrocinante');
	}

	public function ingresos(){
		return $this->belongsToMany('Ingreso', 'donacion_ingreso', 'donacion_id', 'ingreso_id');
	}
}