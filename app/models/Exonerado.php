<?php

class Exonerado extends BaseModel {
	
	protected $table = 'exonerados';

	protected $primaryKey = 'registro_id';

	public $campo_principal = 'razon_id';

	public static $rules = array(
								'registro_id' => 'required|exists:registros,id',
								'razon_id' => 'required|exists:razones_exoneraciones,id',
								'impreso' => 'required|boolean'
							);

	protected $fillable = array('registro_id','razon_id','impreso');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'registro' => array('modelo' => 'Registro'),
								'razon' => array('modelo' => 'RazonExoneracion', 'campo_relacion' => 'razon_id')
							);

	public function registro(){
		return $this->belongsTo('Registro');
	}

	public function razon(){
		return $this->belongsTo('RazonExoneracion', 'razon_id');
	}
}