<?php

class Ponente extends BaseModel {
	
	protected $table = 'ponentes';

	protected $primaryKey = 'usuario_id';

	public $campo_principal = 'usuario_id';

	public static $rules = array(
								'usuario_id' => 'required|exists:usuarios,id',
								'resumen_curricular' => 'required|string_accent'
							);

	protected $fillable = array('usuario_id', 'resumen_curricular');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								//array()
							);

	public function usuario(){
		return $this->belongsTo('Usuario');
	}
}