<?php

class Puntuacion extends BaseModel {
	
	protected $table = 'puntuaciones';

	public $campo_principal = 'ip';

	public static $rules = array(
								'puntuacion' => 'required|integer',
								'ip' => 'required|ip',
								'usuario_id' => 'exists:usuarios,id'
							);

	protected $fillable = array('puntuacion', 'ip', 'usuario_id');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'usuario' => array('modelo' => 'Usuario'),
								'sede' => array('modelo' => 'Sede'),
								'postulacion' => array('modelo' => 'PostulacionPonencia')
							);

	public function usuario(){
		return $this->belongsTo('Usuario');
	}

	public function sede(){
		return $this->belongsToMany('Sede', 'puntuacion_sede', 'puntuacion_id', 'sede_id');
	}

	public function postulacion(){
		return $this->belongsToMany('PostulacionPonencia', 'postulacion_puntuacion', 'puntuacion_id', 'postulacion_id');
	}
}