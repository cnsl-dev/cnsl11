<?php

class ModeloImpresora extends BaseModel {
	
	protected $table = 'modelos_impresoras';

	public $campo_principal = 'modelo';

	public static $rules = array(
								'marca' => 'required|alpha_accent',
								'modelo' => 'required|alpha_accent'
							);

	protected $fillable = array('marca', 'modelo');
	
	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'impresoras' => array('modelo' => 'Impresora')
							);


	public function impresoras(){
		return $this->hasMany('Impresora', 'modelo_id');
	}
}