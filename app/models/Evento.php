<?php

class Evento extends BaseModel {
	
	protected $table = 'eventos';

	public $campo_principal = 'nombre';

	protected $fillable = array('nombre', 'abreviatura', 'inicio', 'fin', 'activo');

	public static $rules = array(
								'nombre' => 'required|alpha_accent',
								'abreviatura' => 'alpha_dash',
								'inicio' => 'date_format:d/m/Y',
								'fin' => 'date_format:d/m/Y',
								'activo' => 'boolean'
							);

	/*
	 * Relaciones
	 */
	protected $relaciones = array(
								'encuestas' => array('modelo' => 'Encuesta'),
								'sedes' => array('modelo' => 'Sede')
							);

	public function encuestas(){
		return $this->belongsToMany('Encuesta');
	}

	public function sedes(){
		return $this->hasMany('Sede');
	}


	/**
	 *
	 */

	public function beforeSave(&$input){

		if(!isset($input['activo']))
			$input['activo'] = 0;
		elseif($input['activo']==1)
			Evento::where('activo',1)
					->update(array('activo' => 0));

		if(isset($input['inicio']) && empty($input['inicio'])) unset($input['inicio']);
		if(isset($input['fin']) && empty($input['fin'])) unset($input['fin']);

		if(isset($input['inicio']))
			$input['inicio'] = DateTime::createFromFormat('d/m/Y', $input['inicio'])
										->format('Y-m-d');

		if(isset($input['fin']))
			$input['fin'] = DateTime::createFromFormat('d/m/Y', $input['fin'])
										->format('Y-m-d');

	}

	public function afterSave(&$input){
		if(isset($input['encuestas'])){
			$this->encuestas()->sync($input['encuestas']);
		}
	}

	public static function activos(){
		return Evento::where('activo', 1)->get();
	}

	public static function activo(){
		return self::activos()->first();
	}

}