<?php 

namespace H34\Utilidades;

use \DateTime;
use \DateInterval;

/**
* 
*/
class datetimeUtilidades
{
	
	public function viewToModel($input){
		if(is_object($datetime = DateTime::createFromFormat('d/m/Y H:i', $input)))
			$datetime->format('Y-m-d H:i');

		return DateTime::createFromFormat('d/m/Y', $input)->format('Y-m-d');
	}


	public function modelToView($input){
		if(is_object($datetime = DateTime::createFromFormat('Y-m-d H:i:s', $input)))
			return $datetime->format('d/m/Y H:i');

		return DateTime::createFromFormat('Y-m-d', $input)->format('d/m/Y');
	}

	public function limitePonencias($input){
		if(is_object($datetime = DateTime::createFromFormat('d/m/Y H:i', $input))){
			return $datetime->sub(new DateInterval("P3W"))->format('Y-m-d H:i');
		}

		return DateTime::createFromFormat('d/m/Y', $input)
						->sub(new DateInterval("P3W"))
						->format('Y-m-d')
						;
	}
}