<?php 

namespace H34\Utilidades;

/**
* 
*/
class arrayUtilidades
{
	
	public static function convertToList($array, $index, $value){
		$arrayReturn = array();
		foreach ($array as $key => $element) {
			$arrayReturn[$element[$index]] = $element[$value];
		}

		return $arrayReturn;
	}

	public static function startsWith($haystack, $needle) {
	    // search backwards starting from haystack length characters from the end
	    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
	}


	public static function endsWith($haystack, $needle) {
	    // search forward starting from end minus needle length characters
	    return $needle === "" || strpos($haystack, $needle, strlen($haystack) - strlen($needle)) !== FALSE;
	}
}