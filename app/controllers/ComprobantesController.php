<?php 

/**
* 
*/
class ComprobantesController extends \BaseController
{
	protected $registro;
	
	function __construct(Registro $registro)
	{
		$this->registro = $registro;
	}


	public function getParaRegistro($registro_id){
		$registro = $this->registro->find($registro_id);

		if(!$registro)
			return 'El registro especificado no pudo ser encontrado';

		$barcode = DNS1D::getBarcodeSVG($registro->usuario->dni, "C128",2,100);
		return View::make('comprobantes.para_registro', compact('registro','barcode'));
	}


	public function getRegistroBarcode($registro_id){
		$registro = $this->registro->find($registro_id);

		if(!$registro)
			return '';

		return DNS1D::getBarcodeSVG($registro->usuario->dni, 'C128',3,150);
	}
}