<?php 

class RazonesExoneracionesController extends ScaffoldController
{	

	function __construct(RazonExoneracion $Razon)
	{
		parent::__construct($Razon);

		$this->route = 'evento.razones-exoneracion';
		$this->titlePage = 'Razones de exoneración';
		
		$this->filtros['index'] = 'listar_razones_exoneracion';
		$this->filtros['create'] = 'crear_razones_exoneracion';
		$this->filtros['store'] = 'crear_razones_exoneracion';
		$this->filtros['show'] = 'listar_razones_exoneracion';
		$this->filtros['edit'] = 'modificar_razones_exoneracion';
		$this->filtros['update'] = 'modificar_razones_exoneracion';
		$this->filtros['delete'] = 'eliminar_razones_exoneracion';
		
		$this->attachFilters();
	}

}