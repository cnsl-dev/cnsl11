<?php 

class PuntuacionesController extends ScaffoldController
{	

	function __construct(Puntuacion $puntuacion)
	{
		parent::__construct($puntuacion);

		$this->route = 'votacion.puntuaciones';
		$this->titlePage = 'Puntuaciones';
		
		$this->filtros['index'] = 'listar_puntuaciones';
		$this->filtros['create'] = 'crear_puntuaciones';
		$this->filtros['store'] = 'crear_puntuaciones';
		$this->filtros['show'] = 'listar_puntuaciones';
		$this->filtros['edit'] = 'modificar_puntuaciones';
		$this->filtros['update'] = 'modificar_puntuaciones';
		$this->filtros['delete'] = 'eliminar_puntuaciones';
		
		$this->attachFilters();
	}

}