
<?php

class ScaffoldController extends BaseController {

	/**
	 * Colegio Repository
	 *
	 * @var Colegio
	 */
	
	// objeto a ser inyectado en el controlador
	protected $modelObj;
	/*
	 * ruta base de la cual se desprenden
	 * index, create, store, show, edit, update, delete
	 *
	 * ejemplo:
	 * $route = admin.scaffold
	 * index = admin.scaffold.index
	 */
	protected $route;
	/*
	 * rutas de las vistas a ser retornadas por los metodos: index, create, edit y show
	 * por defecto:
	 * $viewIndex = scaffold.index
	 * $viewCreate = scaffold.create
	 * $viewShow = scaffold.show
	 * $viewEdit = scaffold.edit
	 */
	protected $viewIndex;
	protected $viewCreate;
	protected $viewShow;
	protected $viewEdit;
	/*
	 * Filtros para cada uno de los recursos: index, create, store, show, edit, update, delete
	 * un array donde las claves poseen los valores d elos recursos y el valor el nombre del filtro.
	 * Ejemplo:
	 * $filtro['index'] = 'list_scaffold'
	 */
	protected $filtros;

	public function __construct($modelObj)
	{
		$this->modelObj = $modelObj;
		$this->titlePage = 'Scaffold';
		$this->route = 'scaffold';
		$this->viewIndex = 'scaffold.index';
		$this->viewCreate = 'scaffold.create';
		$this->viewShow = 'scaffold.show';
		$this->viewEdit = 'scaffold.edit';
		
		$this->fieldsToHide = array(
					    'created_at',
					    'updated_at',
					    'deleted_at'
					);
		
		$this->beforeFilter('csrf', array('only'=>array('store', 'update', 'destroy')));

		$this->filtros = array();
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$listObjs = $this->getListObjets();
		$headers = $listObjs->count()>0 ? $listObjs[0]->getVisibles() : array();

		$varContext = $this->getVarContextIndex();

		if(Request::ajax())
			return Response::json(compact('listObjs'));

		return View::make($this->viewIndex, array('listObjs' => $listObjs, 'headers' => $headers) + $varContext);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->titlePage = 'Agregar ' . get_class($this->modelObj);
		
		$varContext = $this->getVarContextCreate();

		if(Request::ajax())
			return Response::json($varContext);

		return View::make($this->viewCreate, $varContext);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$modelReflection = new ReflectionClass(get_class($this->modelObj));
		$validation = Validator::make($input, $modelReflection->getStaticPropertyValue('rules'));

		if ($validation->passes())
		{
			$this->modelObj->beforeStore($input);

			$obj = $this->modelObj->create($input);

			$obj->afterStore($input);

			if(Request::ajax())
				return Response::json(array('message' => 'success'));

			return Redirect::route($this->route.'.index');
		}

		if(Request::ajax())
			return Response::json(array('message' => 'error', 'errors' => $validation->messages(), 'fails' => $validation->failed()));

		return Redirect::route($this->route.'.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$obj = $this->getObj($id);
		BaseModel::deleteBykey($obj, $this->fieldsToHide);
		
		$varContext = $this->getVarContextShow($obj);

		if(Request::ajax()){
			return Response::json(array('obj' => $obj) + $varContext);
		}
		return View::make($this->viewShow, array('obj' => $obj) + $varContext);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$obj = $this->getObj($id);

		if (is_null($obj))
		{
			return Redirect::route($this->route.'.index');
		}
		$varContext = $this->getVarContextEdit($obj);

		return View::make($this->viewEdit, array('obj' => $obj) + $varContext);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$modelReflection = new ReflectionClass(get_class($this->modelObj));
		$validation = Validator::make($input, $modelReflection->getStaticPropertyValue('rules'));

		if ($validation->passes())
		{
			$obj = $this->getObj($id);
			
			$obj->beforeUpdate($input);

			$obj->update($input);

			$obj->afterUpdate($input);

			if(Request::ajax())
				return Response::json(array('message' => 'success'));

			return Redirect::route($this->route.'.index');
		}

		if(Request::ajax())
			return Response::json(array('message' => 'error', 'errors' => $validation->messages(), 'fails' => $validation->failed()));

		return Redirect::route($this->route.'.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->getObj($id)->delete();

		return Redirect::route($this->route.'.index');
	}

	

	/************************************************************
	 * 			UTILIDADES DEL CONTROLADOR SCAFFOLD
	 ************************************************************/


	protected function getVarContext(){
		return array(
					'titlePage' => $this->titlePage,
					'route' => $this->route
				);
	}


	protected function getObj($id){
		return $this->modelObj->find($id);
	}
	
	/*
	 *	Asocia cada filtro con un metodo del controlador
	 */
	protected function attachFilters(){
		foreach($this->filtros as $action => $filtro){
			$this->beforeFilter('permisos:'.$this->route.'.'.$action.','.$filtro, array('only' => array($action)));
		}
	}


	//INDEX
	public function getListObjets(){
		$listObjs = $this->modelObj->all();
		foreach ($listObjs as $key => &$obj) {
			BaseModel::deleteBykey($obj, $this->fieldsToHide);
		}
		return $listObjs;
	}

	protected function getVarContextIndex(){
		return $this->getVarContext();
	}

	//CREATE
	protected function getVarContextCreate(){
		$propiedades = $this->modelObj->getAttributesForm();
		$relaciones = $this->modelObj->getRelacionesForm();
		BaseModel::deleteBykey($propiedades, $this->fieldsToHide);
		return $this->getVarContext() +  array('propiedades' => $propiedades, 'relaciones' => $relaciones);
	}


	//SHOW
	protected function getVarContextEdit($obj){
		$propiedades = $obj->getAttributesForm();
		$relaciones = $obj->getRelacionesForm();
		BaseModel::deleteBykey($propiedades, $this->fieldsToHide);
		return $this->getVarContext() + array('propiedades' => $propiedades, 'relaciones' => $relaciones);
	}

	//EDIT
	protected function getVarContextShow($obj){
		return $this->getVarContext();
	}

	//DELETE
	// protected function getVarContextDelete($id){
	// 	return $this->getVarContext();
	// }

}
