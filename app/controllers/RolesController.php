<?php 

/**
* 
*/
class RolesController extends ScaffoldController
{	

	function __construct(Rol $rol)
	{
		parent::__construct($rol);

		$this->route = 'auth.roles';
		$this->titlePage = 'Roles';
		$this->viewCreate = 'roles.create';
		$this->viewEdit = 'roles.edit';
		
		
		$this->filtros['index'] = 'listar_roles';
		$this->filtros['create'] = 'crear_roles';
		$this->filtros['store'] = 'crear_roles';
		$this->filtros['show'] = 'listar_roles';
		$this->filtros['edit'] = 'modificar_roles';
		$this->filtros['update'] = 'modificar_roles';
		$this->filtros['delete'] = 'eliminar_roles';
		
		$this->attachFilters();
	}

	protected function getVarContextCreate(){
		$contextVars = parent::getVarContextCreate();
		return array('permisos' => ConfigApp::getPermisos()) + $contextVars;
	}

	protected function getVarContextEdit($obj){
		$contextVars = parent::getVarContextEdit($obj);
		$permisos_rol = $obj->permisos()->lists('permiso');
		return array('permisos' => ConfigApp::getPermisos(), 'permisos_rol' => $permisos_rol) + $contextVars;
	}
}