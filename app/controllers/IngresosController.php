<?php 

class IngresosController extends ScaffoldController
{	

	function __construct(Ingreso $ingreso)
	{
		parent::__construct($ingreso);

		$this->route = 'contabilidad.ingresos';
		$this->titlePage = 'Ingresos';
		
		$this->filtros['index'] = 'listar_ingresos';
		$this->filtros['create'] = 'crear_ingresos';
		$this->filtros['store'] = 'crear_ingresos';
		$this->filtros['show'] = 'listar_ingresos';
		$this->filtros['edit'] = 'modificar_ingresos';
		$this->filtros['update'] = 'modificar_ingresos';
		$this->filtros['delete'] = 'eliminar_ingresos';

		$this->viewCreate = 'ingresos.create';
		
		$this->attachFilters();
	}



}