<?php 

class RegistrosController extends BaseController
{	

	protected $certificado;
	protected $exonerado;
	protected $ingreso;
	protected $perfil;
	protected $registro;
	protected $sede;
	protected $usuario;

	function __construct(
							Certificado $certificado,
							Exonerado $exonerado,
							Ingreso $ingreso,
							Perfil $perfil,
							Registro $registro, 
							Sede $sede,
							Usuario $usuario
						)
	{
		$this->certificado = $certificado;
		$this->exonerado = $exonerado;
		$this->ingreso = $ingreso;
		$this->perfil = $perfil;
		$this->registro = $registro;
		$this->sede = $sede;
		$this->usuario = $usuario;

		$this->titlePage = 'Registros';

		$this->beforeFilter('permisos:registros.mis_registros,mis_registros',array('only' => array('getMisRegistros')));
        $this->beforeFilter('permisos:registros.registro_en_linea,registro_en_linea',array('only' => array('getRegistrarse','postRegistrarse')));
        $this->beforeFilter('permisos:registros.mi_registro.edit,edit_mi_registro',array('only' => array('getEditMiRegistro', 'patchEditMiRegistro')));
        $this->beforeFilter('permisos:registros.mi_registro.delete,delete_mi_registro',array('only' => array('deleteMiRegistro')));
        $this->beforeFilter('permisos:registros.todos_los_registros,todos_los_registros',array('only' => array('getTodosLosRegistros')));
        $this->beforeFilter('permisos:registros.registros_por_sede,registros_por_sede',array('only' => array('getRegistradosPorSede')));
        $this->beforeFilter('permisos:registros.puerta,registro_en_puerta',array('only' => array('getPuerta', 'postPuerta')));
        $this->beforeFilter('permisos:registros.validar,validar_en_puerta',array('only' => array('getValidar', 'postValidar')));
		
	}


	/*-------------------------------------------------------------------------
	 |			Todos los registros realizados por el usuario
	 |-------------------------------------------------------------------------
	 |
	 |
	 |
	 */
	public function getMisRegistros(){
		$registros = $this->registro->registrosParaUsuario(Auth::user()->id);
		return View::make('registros.mis_registros', compact('registros'));
	}


	/*-------------------------------------------------------------------------
	 |			Formulario de registro para sedes
	 |-------------------------------------------------------------------------
	 |
	 |
	 |
	 */
	public function getRegistrarse(){
		if(!Auth::user()->respondioEncuesta())
			return Redirect::route('encuesta.respuestas.responder');
		$registro = new Registro();
		$sedes = array(0 => '----- Selecciona una sede -----')
				+ $this->sede->dondeUsuarioNoSeHaRegistrado(Auth::user()->id);
		$certificado = FALSE;
		$ingreso = FALSE;
		$parametros_formulario = array('route' => 'registros.registro_en_linea', 'method' => 'POST');
		return View::make('registros.registrarse', 
							compact(
									'registro',
									'sedes', 
									'certificado', 
									'ingreso',
									'parametros_formulario'
									)
							);
	}


	/*-------------------------------------------------------------------------
	 |			Procesar los datos suministrados para el registro
	 |-------------------------------------------------------------------------
	 |
	 |
	 |
	 */
	public function postRegistrarse(){


		$input = Input::only('sede_id', 'certificado', 'tipo_certificado', 'tipo_pago', 'codigo_operacion');

		$rules = array(
						'sede_id' 			=> 'required|exists:sedes,id',
						'certificado'		=> 'in:0,1',
						'tipo_certificado' 	=> 'required_if:certificado,1|in:Estudiante,Profesional',
						'tipo_pago'			=> 'required_if:certificado,1|in:Efectivo,Deposito,Transferencia',
						'codigo_operacion' 	=> 'required_if:certificado,1|numeric',
		);

		$validator = Validator::make($input, $rules);

		if($validator->fails()){
			if(Request::ajax())
				return Response::json(array('message' => 'Error en los valores suministrados'));

			return Redirect::back()
							->withInput()
							->withErrors($validator)
							->with('message', Config::get('app.validation_errors'));
		}

		if(Auth::user()->estaRegistradoParaSede($input['sede_id'])){
			if(Request::ajax())
				return Response::json(array('message' => 'Ya se encuentra registrado para esta sede'));

			return Redirect::back()
							->withInput()
							->withErrors($validator)
							->with('message', 'Ya se encuentra registrado para esta sede');
		}


		$registro = $this->registro->create(array(
						'usuario_id' => Auth::user()->id,
						'sede_id' => $input['sede_id'],
						'fecha_registro' => date('Y-m-d H:i:s')
					));

		if($input['certificado'] === '1'){


			$this->certificado->create(array(
					'registro_id' => $registro->id,
					'tipo' => $input['tipo_certificado'],
					'impreso' => FALSE,
					'entregado' => FALSE
				));

			$config_certificado = Config::get('certificado');
			$ingreso = $this->ingreso->create(array(
									'tipo' => $input['tipo_pago'],
									'codigo_operacion' => $input['codigo_operacion'],
									'descripcion' => 'Pago de certificado',
									'monto' => $config_certificado[$input['tipo_certificado']]['monto']
							));

			$ingreso->certificados()->attach($registro->id);

		}
		if(Request::ajax())
			return Response::json(array('message' => 'Su registro fue completado con exito'));


		return Redirect::route('registros.mis_registros')
						->with('message', 'Su registro fue completado con exito');

	}


	/*
	 *
	 *
	 *
	 */
	public function getEditMiRegistro($registro_id){
		$registro = $this->registro->find($registro_id);

		if(!$registro)
			if(Request::ajax())
				return Response::json(array('message' => 'El registro especificado no pudo ser encontrado'), 404);
			else
				return Redirect::route('registros.mis_registros')
							->with('message', 'El registro especificado no pudo ser encontrado');

		if(!$registro->sede->activo)
			return Redirect::route('registros.mis_registros')
							->with('message', 'Esta sede no se encuentra disponible');

		if(Request::ajax())
			return Response::json(array('obj' => $registro));
		else{
			$sedes =   array(0 => '----- Selecciona una sede -----')
					 + array($registro->id => $registro->sede->ciudad->nombre)
					 + $this->sede->dondeUsuarioNoSeHaRegistrado(Auth::user()->id);

			$certificado = $registro->certificado()->first();
			$ingreso = is_object($certificado) ? $certificado->ingresos()->first() : FALSE;
			$parametros_formulario = array('route' => array('registros.mi_registro.edit', $registro_id), 'method' => 'PATCH');

			return View::make('registros.registrarse', 
								compact(
										'registro', 
										'sedes', 
										'certificado', 
										'ingreso',
										'parametros_formulario'
										)
								);
		}
	}


	/*
	 *
	 *
	 *
	 */
	public function patchEditMiRegistro($registro_id){
		$registro = $this->registro->find($registro_id);

		if(! $registro){
			if(Request::ajax())
				return Response::json(array('message' => 'El registro especificado no pudo ser encontrado'), 404);

			return Redirect::back(404)
							->with('message', 'El registro especificado no pudo ser encontrado');
		}

		if(!$registro->sede->activo)
			return Redirect::route('registros.mis_registros')
							->with('message', 'Esta sede no se encuentra disponible');

		$input = Input::only('sede_id', 'certificado', 'tipo_certificado', 'tipo_pago', 'codigo_operacion');

		$rules = array(
						'sede_id' 			=> 'required|exists:sedes,id',
						'certificado'		=> 'in:0,1',
						'tipo_certificado' 	=> 'required_if:certificado,1|in:Estudiante,Profesional',
						'tipo_pago'			=> 'required_if:certificado,1|in:Efectivo,Deposito,Transferencia',
						'codigo_operacion' 	=> 'required_if:certificado,1|numeric',
			);
		$validator = Validator::make($input, $rules);

		if($validator->fails())
			return Redirect::back()
						->with('message', Config::get('app.validation_errors'));


		/*
		 *	Comprobar si el usuario opto por certificado
		 */
		if($input['certificado'] == '0'){
			$this->certificado->deleteIfExists($registro->id);
			$this->exonerado->deleteIfExists($registro->id);

			return Redirect::route('registros.mis_registros')
							->with('message', 'El registro se completo satisfactoriamente');
		}


		/*
		 *	Comprobar si ya posee un certificado asignado sino crear uno
		 */
		$certificado = $this->certificado->find($registro->id);

		if(! $certificado)
			$certificado = $this->certificado->create(array(
									'registro_id' => $registro->id,
									'tipo'	=> $input['tipo_certificado'],
									'impreso' => FALSE,
									'entregado' => FALSE
							));
		else
			$certificado->update(array('tipo' => $input['tipo_certificado']));


		/*
		 *	Array con los datos del pago
		 */
		$config_certificado = Config::get('certificado');
		$pago_certificado = array(
									'tipo' => $input['tipo_pago'],
									'codigo_operacion' => $input['codigo_operacion'],
									'descripcion' => 'Pago de certificado',
									'monto' => $config_certificado[$input['tipo_certificado']]['monto']
							);

		/*
		 * Asociar el respectivo ingreso al certificado
		 */
		if($certificado->ingresos()->count()>0)
			$certificado->ingresos()->first()->update($pago_certificado);
		else{
			$ingreso = $this->ingreso->create($pago_certificado);
			$ingreso->certificados()->attach($registro->id);
		}


		return Redirect::route('registros.mis_registros')
						->with('message', 'Su registro ha sido actualizado con exito');

	}


	/*
	 *
	 *
	 *
	 */
	public function deleteMiRegistro($sede_id){

		$validator = Validator::make(array('sede_id' => $sede_id), array('sede_id' => 'exists:sedes,id'));

		if($validator->fails()){
			return Response::json(array('message' => 'La sede que ha indicado no pudo ser encontrada'), 404);
		}

		$registro = $this->registro->registroDeUsuarioParaSede(Auth::user()->id, $sede_id);

		if(! $registro){
			return Response::json(array('message' => 'Usted no posee registro para la sede que ha indicado'), 404);
		}

		if(!$registro->sede->activo)
			return Response::json(array('message' => 'Esta sede no se encuentra disponible'), 404);

		$registro->delete();

		return Response::json(array('message' => 'Se ha eliminado el registro'));

	}


	/*
	 *
	 *
	 *
	 */
	public function getRegistradosEnMiSede(){
		if(Auth::user()->esOrganizador()){
			$registros = $this->registro->registrosParaSede(Auth::user()->organizador()->first()->sede_id);
			return View::make('registros.registros_por_sede', compact('registros'));
		}
	}


	/*
	 *
	 *
	 *
	 */
	public function getTodosLosRegistros(){
		$registros = $this->registro->all();
		return View::make('registros.todos_los_registros', compact('registros'));
	}


	/*
	 *
	 *
	 *
	 */
	public function getRegistradosPorSede(){
		$registros = $this->registro->registrosPorSede();
		return View::make('registros.registros_por_sede', compact('registros'));
	}


	/*
	 *
	 *
	 *
	 */
	public function getPuerta(){
		return View::make('registros.puerta');
	}


	/*
	 *
	 *
	 *
	 */
	public function postPuerta(){
		$input = Input::only('nacionalidad','dni');

		$validator = Validator::make($input, array('dni' => 'required|alpha_dash', 'nacionalidad' => 'required|in:V,E,P'));

		if($validator->fails()){
			return Redirect::back()
							->withErrors($validator)
							->with('message','Error en los campos de entrada')
							->withInput();
		}


		if(!($usuario = $this->usuario->buscarPorCedula($input['nacionalidad'].'-'.$input['dni']))){
			if(($usuario = RegistroAntiguo::buscarPorCedula($input['dni']))!==FALSE){
				$usuario->id = 0;
				// $usuario->email            = $usuario->correo;
	            $usuario->nacionalidad     = $usuario->ve;
	            $usuario->dni              = $usuario->cedula;
			}else{
				$usuario = new Usuario();
				$usuario->id = 0;
			}
		}

		if(!($registro = $this->registro->registroDeUsuarioParaSedeActual($usuario->id))){
			$registro = new Registro();
			$registro->id = 0;
		}

		return Redirect::route('registros.validar')
						->with('usuario', serialize($usuario))
						->with('registro', serialize($registro))
						->withInput();
		
	}


	/*
	 *
	 *
	 *
	 */
	public function getValidar(){
		if(! Session::has('usuario') || ! Session::has('registro')){
			$usuario = new Usuario();
			$usuario->id = 0;
			$registro = new Registro();
			$registro->id = 0;
			$certificado = FALSE;
			$ingreso = FALSE;
			$exonerado = FALSE;
		}else{
			$usuario = unserialize(Session::get('usuario'));
			$registro = unserialize(Session::get('registro'));
			$certificado = $registro->certificado()->first();
			$ingreso = is_object($certificado) ? $certificado->ingresos()->first() : FALSE;
			$exonerado = $registro->exonerado()->first();
		}
		$razones = RazonExoneracion::lists('titulo', 'id');
		return View::make('registros.validar', 
							compact(
									'usuario', 
									'registro', 
									'certificado', 
									'ingreso',
									'exonerado',
									'razones'
							)
				);
	}


	/*-------------------------------------------------------------------------
	 |			Crear, actualizar y validar los datos suministrados
	 |-------------------------------------------------------------------------
	 |
	 |
	 |
	 */
	public function postValidar(){

		$input = Input::only(
								'usuario_id',
								'registro_id', 
								'nombres', 
								'apellidos', 
								'nacionalidad', 
								'dni', 
								'certificado',
								'tipo_certificado', 
								'tipo_pago',
								'codigo_operacion',
								'exonerado',
								'razon_exoneracion'
							);

		$rules = array(
						'usuario_id' 		=> 'integer|exists:usuarios,id',
						'registro_id' 		=> 'integer|exists:registros,id',
						'nombres' 			=> 'required|alpha_accent',
						'apellidos'			=> 'required|alpha_accent',
						'nacionalidad'		=> 'required|in:V,E,P',
						'dni'				=> 'required|alpha_dash',
						'certificado'		=> 'in:0,1',
						'tipo_certificado'	=> 'in:Profesional,Estudiante',
						'tipo_pago'			=> 'in:Efectivo,Deposito,Transferencia',
						'codigo_operacion'	=> 'required_if:tipo_pago,Deposito,Transferencia|alpha_dash',
						'exonerado'			=> 'in:0,1',
						'razon_exoneracion'	=> 'exists:razones_exoneraciones,id'
		);	

		$validator = Validator::make($input, $rules);

		if($validator->fails())
			return Redirect::back()
					->withInput()
					->withErrors($validator)
					->with('message', Config::get('app.validation_errors'));

		/*
		 *	Comprobar si el usuario ya se encuentra registrado
		 */
		if(isset($input['usuario_id'])){
			$usuario = $this->usuario->find($input['usuario_id']);

			if(! $usuario)
				return Redirect::back()
								->with('message', 'El usuario que ha especificado no puede ser encontrado');

		}else{
			// crear usuario
			$usuario = $this->usuario->create(array(
						'nombres' 					=> $input['nombres'],
						'apellidos'					=> $input['apellidos'],
						'dni' 						=> $input['nacionalidad'].'-'.$input['dni'],
						'activo'					=> FALSE,
						'registrado_en_linea' 		=> FALSE,
						'por_confirmar_en_linea' 	=> TRUE,
				));
			// verificar si existe en registros antiguos informacion para completar el perfil del usuario
			if($old_registro = RegistroAntiguo::buscarPorCedula($input['dni'])){

				$usuario->update(array(
								'sexo' => $old_registro['sexo'] == 'm' ? 'Masculino' : 'Femenino'
							));

				$perfil = new Perfil(array(
						'fecha_nacimiento' => $old_registro['fecha_nac'],
						'direccion' => $old_registro['direccion'],
						'telefono' => $old_registro['telefono'],
						'web' => $old_registro['web']
					));
				$usuario->perfil()->save($perfil);
			}
		}

		/*
		 * Comprobar si el usuario ya se registro en para esta sede
		 */
		if(isset($input['registro_id'])){

			$registro = $this->registro->find($input['registro_id']);
			
			if($registro->usuario_id != $input['usuario_id'])
				return Redirect::route('registros.puerta')
								->message('El registro no corresponde al usuario especificado');
			
			// datos para la actualización del registro
			$datos_actualizacion_registro = array('fecha_modificacion' => date('Y-m-d H:i:s')); 

			// si no ha sido validado se asigna la fecha actual para la validacion
			if(empty($registro->fecha_validacion)) 
				$datos_actualizacion_registro['fecha_validacion'] =  date('Y-m-d H:i:s');

			$registro->update($datos_actualizacion_registro);
		}else{
			$registro = $this->registro->create(array(
										'usuario_id' => $usuario->id,
										'sede_id' => $this->sede->actual()->id,
										'fecha_registro' => date('Y-m-d H:i:s'),
										'fecha_validacion' => date('Y-m-d H:i:s'),
				));
		}

		/*
		 *	Comprobar si el asistente opto por certificado
		 */
		if($input['certificado'] == '0'){
			$this->certificado->deleteIfExists($registro->id);
			$this->exonerado->deleteIfExists($registro->id);

			return Redirect::route('registros.puerta')
							->with('message', 'El registro se completo satisfactoriamente');
		}

		/*
		 *	Comprobar si el certificado debe ser exonerado
		 */
		if($input['exonerado'] == '1'){
			$exonerado = $this->exonerado->find($registro->id);

			if(! $exonerado)
				$this->exonerado->create(array(
							'registro_id' => $registro->id,
							'razon_id' => $input['razon_exoneracion'],
							'impreso' => FALSE,
							'entregado' => FALSE
					));
			else
				$exonerado->update(array('razon_id' => $input['razon_exoneracion']));

			$this->certificado->deleteIfExists($registro->id);	// Elimina un certificado asignado de existir

			return Redirect::route('registros.puerta')
							->with('message', 'El registro se completo satisfactoriamente');
		}

		/*
		 *	Comprobar si ya posee un certificado asignado sino crear uno
		 */
		$certificado = $this->certificado->find($registro->id);

		if(! $certificado)
			$certificado = $this->certificado->create(array(
									'registro_id' => $registro->id,
									'tipo'	=> $input['tipo_certificado'],
									'impreso' => FALSE,
									'entregado' => FALSE
							));
		else
			$certificado->update(array('tipo' => $input['tipo_certificado']));

		

		/*
		 *	Array con los datos del pago
		 */
		$config_certificado = Config::get('certificado');
		$pago_certificado = array(
									'tipo' => $input['tipo_pago'],
									'codigo_operacion' => $input['tipo_pago'] == 'Efectivo' ? NULL : $input['codigo_operacion'],
									'descripcion' => 'Pago de certificado',
									'monto' => $config_certificado[$input['tipo_certificado']]['monto']
							);

		/*
		 *
		 */
		if($certificado->ingresos()->count()>0)
			$certificado->ingresos()->first()->update($pago_certificado);
		else{
			$ingreso = $this->ingreso->create($pago_certificado);
			$ingreso->certificados()->attach($registro->id);
		}

		$this->exonerado->deleteIfExists($registro->id); // Elimina la exoneración de existir

		return Redirect::route('registros.puerta')
							->with('message', 'El registro se completo satisfactoriamente');

	}
}