<?php 

class ExoneradosController extends ScaffoldController
{	

	function __construct(Exonerado $exonerados)
	{
		parent::__construct($exonerados);

		$this->route = 'evento.exonerados';
		$this->titlePage = 'Exonerados';
		
		$this->filtros['index'] = 'listar_exonerados';
		$this->filtros['create'] = 'crear_exonerados';
		$this->filtros['store'] = 'crear_exonerados';
		$this->filtros['show'] = 'listar_exonerados';
		$this->filtros['edit'] = 'modificar_exonerados';
		$this->filtros['update'] = 'modificar_exonerados';
		$this->filtros['delete'] = 'eliminar_exonerados';
		
		$this->attachFilters();
	}

}