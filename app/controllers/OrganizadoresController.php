<?php 

class OrganizadoresController extends \BaseController {

	protected $sede;
	protected $usuario;
	protected $organizador;

	public function __construct(Sede $sede, 
								Usuario $usuario, 
								OrganizadorRegional $organizador
								){

		$this->sede = $sede;
		$this->usuario = $usuario;
		$this->organizador = $organizador;

		$this->titlePage = 'Asinar organizador';

		$this->beforeFilter('csrf', array('only'=>array('postSede')));
	}

	public function getSede($id){
		$sede = $this->sede->find($id);
		$propiedades = $sede->getAttributesVisibles();
		return View::make('organizadores.sede', compact('sede', 'propiedades'));
	}

	public function postSede($id){

		$input = Input::all();

		$input['sede_id'] = $id;
		$input['responsabilidad_id'] = 1;

		$validator = Validator::make($input, OrganizadorRegional::$rules);

		if($validator->fails()){
			return Redirect::back()
					->withErrors($validator)
					->with('message', 'Error en los valores suministrados');
		}

		$this->organizador->create($input);


		return Redirect::route('evento.sedes.index')
						->with('message', 'El organizador ha sido asignado con exito')
						->with('class', 'success');

	}

	public function deleteUnlink($id){

		$validator = Validator::make(array('sede_id' => $id), array('sede_id' => 'required|exists:sedes,id'));

		if($validator->fails())
			return Response::json(array('message' => 'Registro no encontrado'), 404);

		$this->organizador->deleteOrganizadorSede($id);

		$message = 'Ha sido retirado el cargo de organizador exitosamente';

		Session::flash('message', $message);
		Session::flash('class', 'success');

		return Response::json(array('message' => $message));

	}
}
