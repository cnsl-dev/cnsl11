<?php 

class DonacionesController extends ScaffoldController
{	

	function __construct(Donacion $donacion)
	{
		parent::__construct($donacion);

		$this->route = 'evento.donaciones';
		$this->titlePage = 'Donaciones';
		
		$this->filtros['index'] = 'listar_donaciones';
		$this->filtros['create'] = 'crear_donaciones';
		$this->filtros['store'] = 'crear_donaciones';
		$this->filtros['show'] = 'listar_donaciones';
		$this->filtros['edit'] = 'modificar_donaciones';
		$this->filtros['update'] = 'modificar_donaciones';
		$this->filtros['delete'] = 'eliminar_donaciones';
		
		$this->attachFilters();
	}

}