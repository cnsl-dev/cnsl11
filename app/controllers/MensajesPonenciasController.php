<?php 

class MensajesPonenciasController extends \BaseController
{	
	
	protected $mensaje;
	protected $ponencia;
	
	function __construct(MensajePonencia $mensaje, Ponencia $ponencia)
	{
		
		$this->mensaje = $mensaje;
		$this->ponencia= $ponencia;

		$this->route = 'evento.mensajes-ponencias';
		$this->titlePage = 'Incidencias';

	}
	
	
	/*-------------------------------------------------------------------------
	 |	   Formualdio de edición y listados de mensajes par una ponencia
	 |-------------------------------------------------------------------------
	 |
	 |
	 |
	 */
	public function getList($ponencia_id){
		
		$ponencia = $this->ponencia->find($ponencia_id);
		
		if(! $ponencia)
			return Redirect::back()
							->with('message', 'La ponencia que ha especificado no pudo ser encontrado');
							
		$mensajes = $ponencia->mensajes();
		
		return View::make('mensajes.ponencias.list', compact('ponencia','mensajes'));
	}
	
	/*-------------------------------------------------------------------------
	 |			Validación y almacenamiento de una nueva mensaje
	 |-------------------------------------------------------------------------
	 |
	 |
	 |
	 */
	public function postStore($ponencia_id){
		
		$ponencia = $this->ponencia->find($ponencia_id);
		
		if(! $ponencia)
			return Redirect::back()
							->with('message', 'La ponencia que ha especificado no pudo ser encontrado');
		
		$input = Input::only('mensaje', 'publico');
		
		$validator = Validator::make($input, array('mensaje' => 'required|string_accent', 'publico' => 'boolean'));
		
		if($validator->fails())
			return Redirect::back()
							->withErrors($validator)
							->withInput()
							->with('message', Config::get('app.validation_errors'));
							
		$ponencia->mensajes()->save(
									  new MensajePonencia(
											array(
												'mensaje' => $input['mensaje'],
												'receptor' => $ponencia->usuario_id,
												'enviado' => date('Y-m-d H:i:s')
										  )
									  )
								);
		
		return Redirect::route('mensajes.ponencias.list', $ponencia_id)
						->with('message', 'Su mensaje fue creado exitosamente');
	}
	
	/*-------------------------------------------------------------------------
	 |	  			Mostrar detalles de cualquier incidenicia
	 |-------------------------------------------------------------------------
	 |
	 |
	 |
	 */
	public function getShow($mensaje_id){
		$mensaje = $this->mensaje->find($mensaje_id);
		
		if(!$mensaje)
			return Redirect::back()
							->with('message', 'El mensaje que ha especificado no pudo ser encontrado');
							
		return View::make('mensajes.ponencias.show', compact('mensaje'));
	}
	
	/*-------------------------------------------------------------------------
	 |	  		  	Mostrar detalles de incidenicia propia
	 |-------------------------------------------------------------------------
	 |
	 |
	 |
	 */
	public function getShowOwn($mensaje_id){
		$mensaje = $this->mensaje->find($mensaje_id);
		
		if(!$mensaje && $mensaje->ponencia->usuario_id != Auth::user()->id)
			return Redirect::back()
							->with('message', 'El mensaje que ha especificado no pudo ser encontrado');
							
		if($mensaje->leido==0)
			$mensaje->update(array('leido' => 1));
							
		return View::make('mensajes.ponencias.show_own', compact('mensaje'));
	}
	
	
	/*-------------------------------------------------------------------------
	 |	   Formualdio de edición y listados de las indicencias
	 |	de la ponencia de un usuario especifico
	 |-------------------------------------------------------------------------
	 |
	 |
	 |
	 */
	public function getListOwn($ponencia_id){
		$ponencia = $this->ponencia->find($ponencia_id);
		
		if(! $ponencia && $ponencia->usuario_id != Auth::user()->id)
			return Redirect::back()
							->with('message', 'La ponencia que ha especificado no pudo ser encontrado');
							
		$mensajes = $ponencia->mensajes();
		
		return View::make('mensajes.ponencias.list_own', compact('ponencia','mensajes'));
	}
	
	/*-------------------------------------------------------------------------
	 |	   Validación y almacenamiento de nueva indicencia para
	 |	ponencia de un usuario especifico
	 |-------------------------------------------------------------------------
	 |
	 |
	 |
	 */
	public function postStoreOwn($ponencia_id){
		
		$ponencia = $this->ponencia->find($ponencia_id);
		
		if(! $ponencia && $ponencia->usuario_id != Auth::user()->id)
			return Redirect::back()
							->with('message', 'La ponencia que ha especificado no pudo ser encontrado');
		
		$input = Input::only('mensaje', 'respuesta');

		$rules = array(
						'mensaje' => 'required|string_accent', 
						'respuesta' => 'required|exists:mensajes_ponencias,id'
					);
		
		$validator = Validator::make($input, $rules);

		if($validator->fails())
			return Redirect::back()
							->withErrors($validator)
							->withInput()
							->with('message', Config::get('app.validation_errors'));
							
		$ponencia->mensajes()->save(
									  new MensajePonencia(
											array(
												'mensaje' => $input['mensaje'],
												'emisor' => $ponencia->usuario_id,
												'respuesta' => $input['respuesta'],
												'enviado' => date('Y-m-d H:i:s')
										  )
									  )
								);
		
		return Redirect::route('mensajes.ponencias.list_own', $ponencia_id)
						->with('message', 'Su mensaje fue creado exitosamente');
	}

}



