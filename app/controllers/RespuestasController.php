<?php 

class RespuestasController extends \BaseController
{	
	protected $respuesta;
	protected $pregunta;

	function __construct(Respuesta $respuesta, Pregunta $pregunta)
	{
		$this->respuesta = $respuesta;
		$this->pregunta = $pregunta;

		$this->route = 'encuesta.respuestas';
		$this->titlePage = 'Respuestas';
		
	}

	public function getResponder(){
		if(Auth::user()->respondioEncuesta())
			return Redirect::route('encuesta.respuestas.mis_respuestas');
		$preguntas = Evento::activo()->encuestas()->first()->preguntas()->orderBy('orden')->get();
		return View::make('respuestas.responder', compact('preguntas'));
	}


	public function getMisRespuestas(){
		if(!Auth::user()->respondioEncuesta())
			return Redirect::route('encuesta.respuestas.responder');
		$preguntas = Evento::activo()->encuestas()->first()->preguntas()->orderBy('orden')->get();
		return View::make('respuestas.mis_respuestas', compact('preguntas'));
	}


	public function postResponder(){
		$input = Input::all();
		unset($input['_token']);
		foreach ($input as $encuesta => $respuesta) {
			if(is_array($respuesta))
				foreach ($respuesta as $respuesta_multiple) {
					$this->respuesta->create(array(
						'usuario_id' => Auth::user()->id,
						'evento_id' => Evento::activo()->id,
						'opcion_id' => $respuesta_multiple
					));
				}
			else{
				$this->respuesta->create(array(
					'usuario_id' => Auth::user()->id,
					'evento_id' => Evento::activo()->id,
					'opcion_id' => $respuesta
				));
			}
		}
		return Redirect::route('registros.registro_en_linea');
	}

}