<?php 


/**
* 
*/
class SedesController extends BaseController
{

	protected $sede;
	protected $departamento;
	
	function __construct(Sede $sede, Departamento $departamento)
	{

		$this->sede = $sede;
		$this->departamento = $departamento;

		$this->route = 'evento.sedes';
		$this->titlePage = 'Sedes';

		$this->filtros['index'] = 'listar_sedes';
		$this->filtros['create'] = 'crear_sedes';
		$this->filtros['store'] = 'crear_sedes';
		$this->filtros['show'] = 'listar_sedes';
		$this->filtros['edit'] = 'modificar_sedes';
		$this->filtros['update'] = 'modificar_sedes';
		$this->filtros['delete'] = 'eliminar_sedes';

		$this->viewIndex = 'sedes.index';
		$this->viewCreate = 'sedes.create';
		$this->viewEdit = 'sedes.edit';
	}

	public function index(){
		$sedes = $this->sede->paraEventoActivo();
		$titlePage = $this->titlePage;
		return View::make('sedes.index', compact('sedes', 'titlePage'));
	}

	public function create(){
		$departamentos = array(0 => '--- seleccione un estado ---') + $this->departamento->lists('nombre', 'id');
		return View::make('sedes.create', compact('departamentos'));
	}

	public function store(){
		$input = Input::all();

		$validator = Validator::make($input, Sede::$rules);

		if($validator->fails())
			return Redirect::back()
							->with('message', Config::get('app.validation_errors'))
							->with('class', 'danger')
							->withInput()
							->withErrors($validator);

		$input['evento_id'] = Evento::activo()->id;

		if(isset($input['limite_ponencias']) && !empty($input['limite_ponencias']))
			$input['limite_ponencias'] = datetimeUtils::viewToModel($input['limite_ponencias']);
		elseif(isset($input['inicio']) && !empty($input['inicio'])){
			$input['limite_ponencias'] = datetimeUtils::limitePonencias($input['inicio']);
		}

		if(isset($input['inicio']) && !empty($input['inicio']))
			$input['inicio'] = datetimeUtils::viewToModel($input['inicio']);
		if(isset($input['fin']) && !empty($input['fin']))
			$input['fin'] = datetimeUtils::viewToModel($input['fin']);

		$this->sede->create($input);

		return Redirect::route('evento.sedes.index')
						->with('message', 'La sede ha sido creada satisfactoriamente')
						->with('class', 'success');
	}

	public function show($id){
		$sede = $this->sede->find($id);

		if( !$sede )
			return Redirect::route('evento.sedes.index')
							->with('message', 'La sede que ha especificado no ha podido ser encontrada')
							->with('class', 'danger');

		if(Request::ajax()){
			return Response::json(array('obj' => $sede));
		}
							
		return View::make('sedes.show', compact('sede'));
	}

	public function edit($id){
		$sede = $this->sede->find($id);

		if( !$sede )
			return Redirect::route('evento.sedes.index')
							->with('message', 'La sede que ha especificado no ha podido ser encontrada')
							->with('class', 'danger');

		if(!empty($sede->inicio))
			$sede->inicio = datetimeUtils::modelToView($sede->inicio);
		if(!empty($sede->fin))
			$sede->fin = datetimeUtils::modelToView($sede->fin);

		$departamentos = array(0 => '--- seleccione un estado ---') + $this->departamento->lists('nombre', 'id');
		$ciudades = $sede->ciudad->departamento->ciudades()->lists('nombre', 'id');

		return View::make('sedes.edit', compact('sede', 'departamentos', 'ciudades'));
	}

	public function update($id){
		$sede = $this->sede->find($id);

		if( !$sede )
			return Redirect::route('evento.sedes.index')
							->with('message', 'La sede que ha especificado no ha podido ser encontrada')
							->with('class', 'danger');

		$input = Input::all();
		if(isset($input['_token'])) unset($input['_token']);
		if(isset($input['_method'])) unset($input['_method']);
		if(isset($input['estado'])) unset($input['estado']);
		$sede = $this->sede->find($id);

		if( !$sede )
			return Redirect::route('evento.sedes.index')
							->with('message', 'La sede que ha especificado no ha podido ser encontrada')
							->with('class', 'danger');
		if(!isset($input['activo'])) $input['activo'] = 0;
		if(!isset($input['actual'])) $input['actual'] = 0;

		$validator = Validator::make($input, Sede::$rules);

		if($validator->fails())
			return Redirect::back()
							->with('message', Config::get('app.validation_errors'))
							->with('class', 'danger')
							->withInput()
							->withErrors($validator);

		$input['inicio'] = ( isset($input['inicio']) && !empty($input['inicio']) ) ? datetimeUtils::viewToModel($input['inicio']) : null;
		$input['fin'] = ( isset($input['fin']) && !empty($input['fin']) ) ? datetimeUtils::viewToModel($input['fin']) : null;
		$input['limite_ponencias'] = ( isset($input['limite_ponencias']) && !empty($input['limite_ponencias']) ) ? datetimeUtils::viewToModel($input['limite_ponencias']) : null;

		$sede->update($input);

		return Redirect::route('evento.sedes.index')
						->with('message', 'La información sobre la sede se ha actualizado satisfactoriamente.')
						->with('class', 'success');
	}


	public function destroy($id){
		$sede = $this->sede->find($id);

		if( !$sede )
			return Redirect::route('evento.sedes.index')
							->with('message', 'La sede que ha especificado no ha podido ser encontrada')
							->with('class', 'danger');

		$sede->delete();

		$message = 'La sede ha sido eliminada satisfactoriamente.';

		if(Request::ajax()){
			Session::flash('message', $message);
			Session::flash('class', 'success');
			return Response::json(array('message' => $message));
		}

		return Redirect::route('evento.sedes.index')
						->with('message', $message)
						->with('class', 'success');
	}
}