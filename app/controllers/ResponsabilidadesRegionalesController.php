<?php 

/**
* 
*/
class ResponsabilidadesRegionalesController extends ScaffoldController
{	

	function __construct(ResponsabilidadRegional $responsabilidad)
	{
		parent::__construct($responsabilidad);

		$this->route = 'evento.responsabilidades-regionales';
		$this->titlePage = 'Responsabilidades';
		
		$this->filtros['index'] = 'listar_responsabilidades_regionales';
		$this->filtros['create'] = 'crear_responsabilidades_regionales';
		$this->filtros['store'] = $this->filtros['create'];
		$this->filtros['show'] = $this->filtros['index'];
		$this->filtros['edit'] = 'modificar_responsabilidades_regionales';
		$this->filtros['update'] = $this->filtros['edit'];
		$this->filtros['delete'] = 'eliminar_responsabilidades_regionales';
		
		$this->attachFilters();
	}

}