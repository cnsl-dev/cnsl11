<?php 

/**
* 
*/
class PonentesController extends \BaseController
{
	protected $ponente;
	
	function __construct(Ponente $ponente)
	{
		$this->ponente = $ponente;

		$this->beforeFilter('permisos:ponentes.create,mi_resumen_curricular',array('only' => array('getCreate')));
		$this->beforeFilter('permisos:ponentes.store,mi_resumen_curricular',array('only' => array('postStore')));
		$this->beforeFilter('permisos:ponentes.edit,mi_resumen_curricular',array('only' => array('getEdit')));
		$this->beforeFilter('permisos:ponentes.update,mi_resumen_curricular',array('only' => array('patchUpdate')));
	}

	public function getCreate(){
		if(Auth::user()->esPonente())
			return Redirect::route('ponentes.edit');
		return View::make('ponentes.create');
	}

	public function postStore(){

		if(Auth::user()->esPonente())
			return Redirect::route('ponentes.edit');

		$input = Input::only('resumen_curricular');

		$input['usuario_id'] = Auth::user()->id;

		$validator = Validator::make($input, Ponente::$rules);

		if($validator->fails())
			return Redirect::back()
							->with('message', Config::get('app.validation_errors'));

		$this->ponente->create($input);

		return Redirect::route('ponencias.create')
							->with('message', 'Sus datos han sido almacenados con éxito');
	}


	public function getEdit(){

		if(!Auth::user()->esPonente())
			return View::make('ponentes.create');

		$ponente = Auth::user()->ponente()->first();
		return View::make('ponentes.edit', compact('ponente'));
	}


	public function patchUpdate(){

		if(!Auth::user()->esPonente())
			return Redirect::back()
							->with('message', 'El usuario especificado no pudo ser encontrado');


		$input = Input::only('resumen_curricular');

		$input['usuario_id'] = Auth::user()->id;

		$validator = Validator::make($input, Ponente::$rules);

		if($validator->fails())
			return Redirect::back()
							->with('message', Config::get('app.validation_errors'));


		$ponente = Auth::user()->ponente()->first();

		$ponente->update($input);

		return Redirect::route('ponencias.mis_ponencias')
							->with('message', 'Sus datos han sido actualizado con éxito');

	}
}