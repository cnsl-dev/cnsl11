<?php 

	/**
	* 
	*/
	class CiudadesController extends ScaffoldController
	{
		
		function __construct(Ciudad $ciudad)
		{
			parent::__construct($ciudad);

			$this->route = 'regionalizacion.ciudades';
			$this->titlePage = 'Ciudades';

			$this->filtros['index'] = 'listar_ciudades';
			$this->filtros['create'] = 'crear_ciudades';
			$this->filtros['store'] = 'crear_ciudades';
			$this->filtros['show'] = 'listar_ciudades';
			$this->filtros['edit'] = 'modificar_ciudades';
			$this->filtros['update'] = 'modificar_ciudades';
			$this->filtros['delete'] = 'eliminar_ciudades';
			
			$this->attachFilters();
		}
	}