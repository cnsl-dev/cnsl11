<?php 

class ImpresorasController extends ScaffoldController
{	

	function __construct(Impresora $impresora)
	{
		parent::__construct($impresora);

		$this->route = 'evento.impresoras';
		$this->titlePage = 'Impresoras';
		
		$this->filtros['index'] = 'listar_impresoras';
		$this->filtros['create'] = 'crear_impresoras';
		$this->filtros['store'] = 'crear_impresoras';
		$this->filtros['show'] = 'listar_impresoras';
		$this->filtros['edit'] = 'modificar_impresoras';
		$this->filtros['update'] = 'modificar_impresoras';
		$this->filtros['delete'] = 'eliminar_impresoras';
		
		$this->attachFilters();
	}

}