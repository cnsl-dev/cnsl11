<?php

class PonenciasController extends \BaseController {

    protected $ponencia;
    protected $sede;
    protected $ciudad;
    protected $departamentos;
    protected $eje_tematico;

    function __construct(Ponencia $ponencia, 
                         Sede $sede, 
                         Departamento $departamento, 
                         Ciudad $ciudad,
                         EjeTematico $eje_tematico){

        $this->ponencia = $ponencia;
        $this->sede = $sede;
        $this->departamento = $departamento;
        $this->ciudad = $ciudad;
        $this->eje_tematico = $eje_tematico;

        $this->titlePage = 'Ponencias';

        $this->beforeFilter('permisos:ponencias,todas_las_ponencias',array('only' => array('getIndex')));
        $this->beforeFilter('permisos:ponencias.filtros,todas_las_ponencias',array('only' => array('getFiltros')));

        $this->beforeFilter('permisos:ponencias.create,crear_ponencia',array('only' => array('getCreate')));
        $this->beforeFilter('permisos:ponencias.store,crear_ponencia',array('only' => array('postStore')));

        $this->beforeFilter('permisos:ponencias.show,show_ponencia',array('only' => array('getShow')));
        $this->beforeFilter('permisos:ponencias.laminas,show_ponencia',array('only' => array('getLaminas')));
        $this->beforeFilter('permisos:ponencias.status.update,show_ponencia',array('only' => array('patchModificarStatus')));

        $this->beforeFilter('permisos:ponencias.mis_ponencias,mis_ponencias',array('only' => array('getMisPonencias')));

        $this->beforeFilter('permisos:ponencias.mi_ponencia.edit,edit_mi_ponencia',array('only' => array('getEditMiPonencia')));
        $this->beforeFilter('permisos:ponencias.mi_ponencia.update,edit_mi_ponencia',array('only' => array('patchUpdateMiPonencia')));
        $this->beforeFilter('permisos:ponencias.mis_laminas,edit_mi_ponencia',array('only' => array('getMisLaminas')));

        $this->beforeFilter('permisos:ponencias.mi_ponencia.delete,delete_mi_ponencia',array('only' => array('deleteMiPonencia')));

    }


    /*-------------------------------------------------------------------------
     |              Lista de todas la ponencias
     |-------------------------------------------------------------------------
     |
     |
     |
     */
    public function getIndex(){
        $ponencias = $this->ponencia->getAll();
        $sedes_obj = $this->sede->all();
        $sedes = array(0 => '--- seleccione una sede ---');
        foreach ($sedes_obj as $sede) {
            $sedes[$sede->id] = $sede->ciudad->nombre;
        }
        $status = array(
                        0 => '--- seleccione un status ---',
                        'en-revision' => 'En revision',
                        'no-procede' => 'No procede',
                        'admitida' => 'Admitida'
                        );
        return View::make('ponencias.index', compact('ponencias', 'sedes', 'status'));
    }



    /*-------------------------------------------------------------------------
     |              Filtros del index
     |-------------------------------------------------------------------------
     |
     |
     |
     */
    public function getFiltros($sede_id, $status, $response = 'html'){
        
        $validator = Validator::make(
                        array(
                            'sede_id' => $sede_id, 
                            'status' => $status,
                            'response' => $response
                            ), 
                        array(
                            'sede_id' => 'required|integer', 
                            'status' => 'required|in:en-revision,no-procede,admitida,0',
                            'response' => 'required|in:html,ods'
                            )
                        );

        if($validator->fails())
            return Response::json(array('message' => Config::get('app.validation_errors')));

        switch ($status) {
            case 'en-revision':
                $status = 'En revision';
                break;
            case 'no-procede':
                $status = 'No procede';
                break;
            case 'admitida':
                $status = 'Admitida';
                break;
            default:
                $status = 0;
                break;
        }

        if($sede_id == 0 && $status === 0){
            $ponencias = $this->ponencia->getAll();
        }

        elseif($sede_id != 0 && $status === 0){
            $ponencias = $this->ponencia->ponenciasParaSede($sede_id);
        }

        elseif($sede_id == 0 && $status !== 0){
            $ponencias = $this->ponencia->ponenciasParaStatus($status);
        }

        elseif($sede_id != 0 && $status !== 0){
            $ponencias = $this->ponencia->ponenciasParaSedeYParaStatus($sede_id, $status);
        }

        if($response === 'ods'){
            $respuesta = Response::make(View::make('ponencias.partial.index_ods', compact('ponencias')), 200, 
                                        array(
                                                'Content-type' => 'application/vnd.oasis.opendocument.spreadsheet; charset=utf-8',
                                                'Content-Disposition' => 'attachment; filename=prueba.ods'
                                            )
                                        );
            return $respuesta;
        }

        return View::make('ponencias.partial.index', compact('ponencias'));
    }


    /*-------------------------------------------------------------------------
     |              Detalles de una ponencia
     |-------------------------------------------------------------------------
     |
     |
     |
     */
     public function getShow($ponencia_id){

        $ponencia = $this->ponencia->find($ponencia_id);

        if(!$ponencia)
            return Redirect::back(404)
                            ->with('message','La ponencia que ha especificado no ha podido ser encontrada');

        return View::make('ponencias.show',compact('ponencia'));

     }


     /*-------------------------------------------------------------------------
     |              Laminas de la ponencia
     |-------------------------------------------------------------------------
     |
     |
     |
     */
     public function getLaminas($ponencia_id){
        $ponencia = $this->ponencia->find($ponencia_id);

        if(!$ponencia)
            return Redirect::back(404)
                            ->with('message','La ponencia que ha especificado no ha podido ser encontrada');

        return Response::download(Config::get('usuarios.directory_laminas').$ponencia->laminas);

     }


     /*-------------------------------------------------------------------------
     |              Laminas de la ponencia
     |-------------------------------------------------------------------------
     |
     |
     |
     */
     public function getMisLaminas($ponencia_id){
        $ponencia = $this->ponencia->find($ponencia_id);

        if(!$ponencia || $ponencia->usuario_id != Auth::user()->id)
            return Redirect::back(404)
                            ->with('message','La ponencia que ha especificado no ha podido ser encontrada');

        return Response::download(Config::get('usuarios.directory_laminas').$ponencia->laminas);

     }


    /*-------------------------------------------------------------------------
     |                  Formulario de creación de ponencias
     |-------------------------------------------------------------------------
     |
     |
     |
     */
    public function getCreate(){

        if(!Auth::user()->esPonente())
            return Redirect::route('ponentes.edit')
                        ->with('message', 'Debe completar primero sus datos como ponente antes de poder enviar una ponencia');

        if(!Auth::user()->camposCompletos()){
            return Redirect::route('edit_profile')
                        ->with('message', 'Debe completar su perfil para poder postular una ponencia: <strong>télefono, avatar y ciudad</strong>')
                        // ->with('come-from', 'ponencias.create')
                        ;
        }

        $sedes = $this->sede->listarIdNombre();
        $departamentos =    array(0 => '--- seleccione un estado ---')
                                             + $this->departamento->lists('nombre', 'id');
        $ejes_tematicos = $this->eje_tematico->lists('descripcion', 'id');
        return View::make('ponencias.create', compact('sedes', 'departamentos', 'ejes_tematicos'));
    }


    /*-------------------------------------------------------------------------
     |              Validación de datos y carga de nuevas ponencias
     |-------------------------------------------------------------------------
     |
     |
     |
     */
    public function postStore(){
        $input = Input::all();
        unset($input['_token']);

        $validator = Validator::make($input, Ponencia::$rules + RequisitosPonencia::$rules + array('ejes' => 'required|array'));

        if($validator->fails())
            return Redirect::back()
                            ->withInput()
                            ->withErrors($validator)
                            ->with('message', Config::get('app.validation_errors'));

        if (Input::hasFile('laminas')) {
            if(Input::file('laminas')->isValid()){
                Input::file('laminas')->move(Config::get('usuarios.directory_laminas'), 
                                            $filename = str_random(64).'.'.Input::file('laminas')->getClientOriginalExtension());
                $input['laminas'] = $filename;
            }
        }

        $postulaciones = array();
        foreach ($input as $key => $element) {
            if(ArrayUtils::startsWith($key, 'sede')){
                $sede = explode('_', $key);
                $sede_id = $sede[1]
                $postulaciones[] = array(
                                            'sede_id' => $sede_id,
                                            'ciudad_id' => $input['ciudad_'.$sede_id]
                                );
            }
        }

        $input['usuario_id'] = Auth::user()->id;

        $requisitos = new RequisitosPonencia($input);
        $ponencia = $this->ponencia->create($input);

        $ponencia->ejesTematicos()->sync($input['ejes']);

        $ponencia->requisito()->save($requisitos);

        foreach ($postulaciones as $postulacion) {
            $ponencia->postulaciones()->save(new PostulacionPonencia($postulacion));
        }

        return Redirect::route('ponencias.mis_ponencias')
                        ->with('message', 'Su ponencia fue creada exitosamente');
    }


    /*-------------------------------------------------------------------------
     |              Listado de ponencias para el usuarios autenticado
     |-------------------------------------------------------------------------
     |
     |
     |
     */
    public function getMisPonencias(){
        $ponencias = $this->ponencia->getPonenciasDeUsuario(Auth::user()->id, 5);
        return View::make('ponencias.mis_ponencias', compact('ponencias'));
    }


    /*-------------------------------------------------------------------------
     |              Formualdio de edición de ponencia propia
     |-------------------------------------------------------------------------
     |
     |
     |
     */
    public function getEditMiPonencia($ponencia_id){
        $ponencia = $this->ponencia->find($ponencia_id);

        if(!$ponencia || $ponencia->usuario_id != Auth::user()->id)
            return Redirect::back(404)
                            ->with('message', 'La ponencia que ha especificado no ha podido ser encontrada');

        if(!( $requisitos = $ponencia->requisito()->first() ))
            $requisitos = new RequisitosPonencia();

        $ejes_tematicos = $this->eje_tematico->lists('descripcion', 'id');
        $ejes_de_ponencia = $ponencia->ejesTematicos()->get()->lists('id');

        return View::make('ponencias.edit', compact('ponencia', 'requisitos', 'ejes_tematicos', 'ejes_de_ponencia'));

    }


    /*-------------------------------------------------------------------------
     |          Validación de datos y actualización de ponencia propia
     |-------------------------------------------------------------------------
     |
     |
     |
     */
    public function patchUpdateMiPonencia($ponencia_id){

        $ponencia = $this->ponencia->find($ponencia_id);

        if(!$ponencia || $ponencia->usuario_id != Auth::user()->id)
            return Redirect::back(404)
                            ->with('message', 'La ponencia que ha especificado no ha podido ser encontrada');

        $input_ponencias = Input::only(
                                'titulo_de_la_ponencia',
                                'resumen_de_la_ponencia',
                                'duracion',
                                'laminas',
                                'ejes'
                            );

        $input_requisitos = Input::only(
                                'proximidad',
                                'conectividad',
                                'hardware',
                                'software',
                                'otros'
                            );

        $validator_ponencias = Validator::make($input_ponencias, Ponencia::$rules_update + array('ejes' => 'required|array'));
        $validator_requisitos = Validator::make($input_requisitos, RequisitosPonencia::$rules);

        if($validator_ponencias->fails() || $validator_requisitos->fails())
            return Redirect::back()
                            ->withInput()
                            ->withErrors($validator_ponencias)
                            ->withErrors($validator_requisitos)
                            ->with('message', Config::get('app.validation_errors'));


        if(empty($input_ponencias['laminas'])) unset($input_ponencias['laminas']);

        if (Input::hasFile('laminas')) {
            if(Input::file('laminas')->isValid()){
                if(!empty($ponencia->laminas) && file_exists(Config::get('usuarios.directory_laminas').$ponencia->laminas)){
                    unlink(Config::get('usuarios.directory_laminas').$ponencia->laminas);
                }
                Input::file('laminas')->move(Config::get('usuarios.directory_laminas'), 
                                            $filename = Mascame\Urlify::transliterate($input_ponencias['titulo_de_la_ponencia'].' - '.Auth::user()->nombres.' '.Auth::user()->apellidos.' - '.time().'.'.Input::file('laminas')->getClientOriginalExtension()));
                $input_ponencias['laminas'] = $filename;
            }
        }

        $input_ponencias['status'] = "En revision";

        $ponencia->update($input_ponencias);
        $ponencia->ejesTematicos()->sync($input_ponencias['ejes']);
        $ponencia->requisito()->update($input_requisitos);

        return Redirect::route('ponencias.mis_ponencias')
                        ->with('message', 'Su ponencia fue creada exitosamente');

    }

    /*-------------------------------------------------------------------------
     |          Modificación de status de la ponencia
     |-------------------------------------------------------------------------
     |
     |
     |
     */
    public function patchModificarStatus($ponencia_id){
        $ponencia = $this->ponencia->find($ponencia_id);

        if(!$ponencia)
            return Redirect::back(404)
                            ->with('message', 'La ponencia que ha especificado no ha podido ser encontrada');

        $input = Input::only('status');

        $validator = Validator::make($input, array('status' => 'required|in:En revision,No procede,Admitida'));

        if($validator->fails())
            return Response::json(array('message' => 'error', 'errors' => $validator->messages()));

        $ponencia->status = $input['status'];
        $ponencia->save();

        return Response::json(array('message' => 'success', 'class' => $ponencia->getClassCss()));
    }


    /*-------------------------------------------------------------------------
     |              Eliminación de ponencia
     |-------------------------------------------------------------------------
     |
     |
     |
     */
    public function deleteMiPonencia(){

    }

}
