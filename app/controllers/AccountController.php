<?php

class AccountController extends \BaseController {

	protected $usuario;
	protected $perfil;
	protected $ciudad;
	protected $departamento;

	public function __construct(Usuario $usuario, 
								Perfil $perfil, 
								Ciudad $ciudad, 
								Departamento $departamento){

		$this->usuario = $usuario;
		$this->perfil = $perfil;
		$this->ciudad = $ciudad;
		$this->departamento = $departamento;

		$this->beforeFilter('auth', array('only' => array('getMyAccount', 'getEditAccount', 'postEditEmail', 'getEditProfile', 'postEditProfile', 'getAvatar')));
		$this->beforeFilter('csrf', array('only'=>array('postLogin', 'postCreateAccount', 'postResendEmailCreateAccount', 'postEditEmail', 'postEditProfile')));
	}

	/**
	 *
	 *	Permite a los usuarios visualizar los datos de su propia cuenta
	 *
	 */
	public function getMyAccount(){
		$usuario = new stdClass();
		$usuario->nombres = Auth::user()->nombres;
		$usuario->apellidos = Auth::user()->apellidos;
		$usuario->dni = Auth::user()->dni;
		$usuario->sexo = Auth::user()->sexo;
		$usuario->email = Auth::user()->email;
		if(Auth::user()->perfil()->count()>0){
			$perfil = Auth::user()->perfil()->first();
			$usuario->avatar = empty($perfil->avatar) ? Config::get('usuarios.avatar-default') : $perfil->avatar;
			$usuario->telefono = $perfil->telefono;
			$usuario->ciudad = is_object($perfil->ciudad) ? $perfil->ciudad->nombre : '';
			$usuario->twitter = $perfil->twitter;
			$usuario->profesion = $perfil->profesion_u_oficio;
			$usuario->organizacion = $perfil->organizacion;
			$usuario->web = $perfil->web;
		}else{
			$usuario->avatar = Config::get('usuarios.avatar-default');
			$usuario->telefono = '';
			$usuario->ciudad = '';
			$usuario->twitter = '';
			$usuario->profesion = '';
			$usuario->organizacion = '';
			$usuario->web = '';
		}
		return View::make('auth.my_account', compact('usuario'));
	}

	/**
	 *
	 *	Formulario de edición de cuenta
	 *
	 */
	public function getEditAccount(){
		$email = Auth::user()->email;
		return View::make('auth.edit_account',compact('email'));
	}


	/**
	 *
	 *	Edición de correo electronico
	 *
	 */
	public function postEditEmail(){

		$input = Input::only('email');

		$input['email'] = mb_strtolower(trim($input['email']));

		$rules = array(
            'email'             => 'required|email|unique:usuarios,email',
        );

        $validation = Validator::make($input, $rules);
        if($validation->passes()){

        	$usuario = Auth::user();

        	$usuario->email = $input['email'];
        	$usuario->save();

            return Redirect::route('edit_account')
            		->with('email', $usuario->email)
            		->with('message', 'Su dirección de correo ha sido modificada exitosamente');

        }

        return Redirect::route('edit_account')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
	}


	/**
	 *
	 *	Edición de password
	 *
	 */
	public function postEditPassword(){

		$input = Input::only('password', 'password-repeat');

		$rules = array(
            'password'          => 'required|alpha_accent|min:6',
            'password-repeat'   => 'required|same:password',
        );

        $validation = Validator::make($input, $rules);
        if($validation->passes()){

        	$usuario = Auth::user();

        	$usuario->password = Hash::make($input['password']);
        	$usuario->save();

            return Redirect::route('edit_account')
            		->with('message', 'Su contraseña ha sido modificada exitosamente');

        }

        return Redirect::route('edit_account')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
	}


	/**
	 *
	 *	Formulario de edición de perfil
	 *
	 */
	public function getEditProfile(){
		$perfil = Auth::user()->perfil();
		if($perfil->count()==0){
			$perfil = $this->perfil->getVisiblesForm();
			
			$twitter = "<div class='input-group'>";
			$twitter .= "  <span class='input-group-addon'>@</span>";
			$twitter .= "  <input value='".$this->perfil->twitter."' type='text' class='form-control' name='twitter'>";
			$twitter .= "</div>";
			
			$departamento = Form::select('departamento_id', 
								  array(0 => '--- Seleccione un estado ---') + $this->departamento->lists('nombre', 'id'), 
								  null, 
								  array('class' => 'form-control departamento departamentos-select', 'id' => 'departamento_id'));
			$ciudad = Form::select('ciudad_id', 
								  array(0 => '--- Seleccione una ciudad ---'), 
								  null, 
								  array('class' => 'form-control ciudad', 'id' => 'ciudad_id'));
		}else{
			$twitter  = "<div class='input-group'>";
			$twitter .= "  <span class='input-group-addon'>@</span>";
			$twitter .= "  <input value='".$perfil->first()->twitter."' type='text' class='form-control' name='twitter'>";
			$twitter .= "</div>";

			if($perfil->first()->ciudad()->count()>0)
				$departamento_id = $perfil->first()->ciudad()->first()->departamento()->first()->id;
			$departamento = Form::select('departamento_id', 
								  array(0 => '--- Seleccione un estado ---') + $this->departamento->lists('nombre', 'id'), 
								  !empty($perfil->first()->ciudad_id) ? $departamento_id : NULL, 
								  array('class' => 'form-control departamento departamentos-select', 'id' => 'departamento_id'));
			$ciudad = Form::select('ciudad_id', 
								  array(0 => '--- Seleccione un estado ---') + (isset($departamento_id) ? $this->departamento->find($departamento_id)->ciudades()->lists('nombre', 'id') : array()),
								  !empty($perfil->first()->ciudad_id) ? $perfil->first()->ciudad_id : NULL, 
								  array('class' => 'form-control ciudad', 'id' => 'ciudad_id'));
			
			$perfil = $perfil->first()->getVisiblesForm();
		}
		$perfil['twitter']['form_control'] = $twitter;
		$perfil['departamento']['form_control'] = $departamento;
		$perfil['ciudad']['form_control'] = $ciudad;
		$perfil += Auth::user()->getVisiblesForm();
		list($nacionalidad, $dni) = explode('-', Auth::user()->dni);
		$perfil['dni']['form_control'] = Form::text('dni', $dni, array('class' => 'form-control'));
		$perfil['nacionalidad']['form_control'] = Form::label('nacionalidad', 'Nacionalidad:');
		$perfil['nacionalidad']['form_control'] = Form::select('nacionalidad', array('V'=>'V','E'=>'E','P'=>'P'), $nacionalidad, array('class' => 'form-control'));
		return View::make('auth.edit_profile', compact('perfil'));
	}


	/**
	 *
	 *	Edición de perfil
	 *
	 */
	public function postEditProfile(){
		$input = Input::all();

		if(empty($input['avatar']))
			unset($input['avatar']);

        $validation = Validator::make($input, Perfil::$rules);
        if($validation->passes()){
        	$perfil = Auth::user()->perfil()->first();
        	if (Input::hasFile('avatar') and isset($input['avatar'])) {
        		if(Input::file('avatar')->isValid()){
        			// si posee avatar borra el archivo actual
        			if(!empty($perfil->avatar))
        				unlink(Config::get('usuarios.directory_avatars').$perfil->avatar);
        			// guarda el archivo en el directorio destinado para ello
        			Input::file('avatar')
        				->move(Config::get('usuarios.directory_avatars'), 
        					   $filename = str_random(64).'.'.Input::file('avatar')->getClientOriginalExtension());
        			// almacena la path del archivo almacenado en input
        			$input['avatar'] = $filename;
        			// redimenciona el archivo a 150X150
        			Image::make(Config::get('usuarios.directory_avatars').$filename)
        					->fit(150,150)
        					->save();
        		}else{
        			unset($input['avatar']);
        		}
        		unset($input['departamento_id']);
        	}

        	$input['dni'] = $input['nacionalidad'].'-'.$input['dni'];
        	if(isset($input['fecha_nacimiento']))
			$input['fecha_nacimiento'] = BaseModel::toDataBaseDate($input['fecha_nacimiento']);

        	Auth::user()->update($input);
        	$perfil->update($input);

            return Redirect::route('edit_profile')
            		->withInput()
            		->with('message', 'Su perfil ha sido modificado exitosamente');

        }

        return Redirect::route('edit_profile')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
	}


	/**
	 *
	 *	Devuelve la imagen de avatar en un response
	 *
	 */
	public function getAvatar(){
		$perfil = Auth::user()->perfil()->first();
		if(! $perfil || empty($perfil->avatar)){
			$avatar = Config::get('usuarios.directory_avatars').Config::get('usuarios.avatar_default');
		}else{
			$avatar = Config::get('usuarios.directory_avatars').$perfil->avatar;
		}
		
		return Image::make($avatar)->response('jpeg');
	}


}