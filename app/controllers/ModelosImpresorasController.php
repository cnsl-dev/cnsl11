<?php 

class ModelosImpresorasController extends ScaffoldController
{	

	function __construct(ModeloImpresora $modelo)
	{
		parent::__construct($modelo);

		$this->route = 'evento.modelos-impresoras';
		$this->titlePage = 'Modelos de impresoras';
		
		$this->filtros['index'] = 'listar_modelos_impresoras';
		$this->filtros['create'] = 'crear_modelos_impresoras';
		$this->filtros['store'] = 'crear_modelos_impresoras';
		$this->filtros['show'] = 'listar_modelos_impresoras';
		$this->filtros['edit'] = 'modificar_modelos_impresoras';
		$this->filtros['update'] = 'modificar_modelos_impresoras';
		$this->filtros['delete'] = 'eliminar_modelos_impresoras';
		
		$this->attachFilters();
	}

}