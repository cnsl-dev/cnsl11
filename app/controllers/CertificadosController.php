<?php 

class CertificadosController extends ScaffoldController
{	

	function __construct(Certificado $certificado)
	{
		parent::__construct($certificado);

		$this->route = 'evento.certificados';
		$this->titlePage = 'Certificados';
		
		$this->filtros['index'] = 'listar_certificados';
		$this->filtros['create'] = 'crear_certificados';
		$this->filtros['store'] = 'crear_certificados';
		$this->filtros['show'] = 'listar_certificados';
		$this->filtros['edit'] = 'modificar_certificados';
		$this->filtros['update'] = 'modificar_certificados';
		$this->filtros['delete'] = 'eliminar_certificados';
		
		$this->attachFilters();
	}

}