<?php 

/**
* 
*/
class PatrocinantesController extends ScaffoldController
{	

	function __construct(Patrocinante $patrocinante)
	{
		parent::__construct($patrocinante);

		$this->route = 'evento.patrocinantes';
		$this->titlePage = 'Patrocinantes';
		
		$this->filtros['index'] = 'listar_patrocinantes';
		$this->filtros['create'] = 'crear_patrocinantes_nacionales|crear_patrocinantes_locales';
		$this->filtros['store'] = $this->filtros['create'];
		$this->filtros['show'] = 'listar_patrocinantes';
		$this->filtros['edit'] = 'modificar_patrocinantes';
		$this->filtros['update'] = 'modificar_patrocinantes';
		$this->filtros['delete'] = 'eliminar_patrocinantes';
		
		$this->attachFilters();

		$this->fieldsToHide[] = 'nacional';
		$this->fieldsToHide[] = 'sede';
	}

}