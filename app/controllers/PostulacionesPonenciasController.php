<?php 

class PostulacionesPonenciasController extends BaseController
{	

	protected $sede;
	protected $departamento;
	protected $ponencia;
	protected $postulacion;

	function __construct(	
							Sede $sede,
							Departamento $departamento,
							Ponencia $ponencia, 
							PostulacionPonencia $postulacion
						)
	{
		
		$this->sede = $sede;
		$this->departamento = $departamento;
		$this->ponencia = $ponencia;
		$this->postulacion = $postulacion;

		$this->titlePage = 'Postulaciones';

		$this->beforeFilter('permisos:postulaciones-ponencias.agregar-sede,postulacion_ponencia_agregar_sede',array('only' => array('getAgregarSede')));
		$this->beforeFilter('permisos:postulaciones-ponencias.agregar-sede.post,postulacion_ponencia_agregar_sede',array('only' => array('postAgregarSede')));
		// $this->beforeFilter('permisos:postulaciones-ponencias.laminas-completas,postulacion_ponencia_todas_las_laminas',array('only' => array('getLaminasCompletas')));

	}


	public function getAgregarSede($ponencia_id){

		$ponencia = $this->ponencia->find($ponencia_id);

		if(!$ponencia || $ponencia->usuario_id != Auth::user()->id)
			return Redirect::back(404)
							->with('message', 'La ponencia que ha especificado no ha podido ser encontrada');

		$departamentos = $this->departamento->lists('nombre', 'id');
		$sedes = $this->sede->dondePonenciaNoSeHaPostulado($ponencia->id);
		return View::make('postulaciones_ponencias.agregar_sede', compact('ponencia', 'sedes', 'departamentos'));
	}
	
	public function postAgregarSede($ponencia_id){
		
		$ponencia = $this->ponencia->find($ponencia_id);

		if(!$ponencia || $ponencia->usuario_id != Auth::user()->id)
			return Redirect::back(404)
							->with('message', 'La ponencia que ha especificado no ha podido ser encontrada');
							
		$input = Input::all();
		unset($input['_token']);
		
		$postulaciones = array();
		foreach ($input as $key => $element) {
			if(ArrayUtils::startsWith($key, 'sede')){
				$sede_id = explode('_', $key)[1];
				$postulaciones[] = array(
											'sede_id' => $sede_id,
											'ciudad_id' => $input['ciudad_'.$sede_id]
								);
			}
		}
		
		foreach ($postulaciones as $postulacion) {
			$ponencia->postulaciones()->save(new PostulacionPonencia($postulacion));
		}

		return Redirect::route('ponencias.mis_ponencias')
						->with('message', 'Sus modificaciones han sido almacenadas exitosamente');
		
	}


	public function getParaMiSede(){
        if(Auth::user()->esOrganizador()){
            $sede_id = Auth::user()->organizador()->first()->sede_id;
            $postulaciones = $this->postulacion->postulacionesParaSede($sede_id);
            return View::make('ponencias.para_mi_sede', compact('postulaciones'));
        }
    }

    public function getLaminasCompletas(){
    	if(Auth::user()->esOrganizador()){
    		$sede_id = Auth::user()->organizador()->first()->sede_id;
    		$postulaciones = $this->postulacion->postulacionesParaSede($sede_id);
    		$laminas = array();
    		foreach ($postulaciones as $postulacion) {
    			$laminas[] = Config::get('usuarios.directory_laminas').$postulacion->ponencia->laminas;
    		}
    		$zip = Config::get('usuarios.directory_laminas').
    						  $this->sede
	    						  ->find($sede_id)
	    						  ->ciudad
	    						  ->nombre.'.zip';

	    	if(file_exists($zip))
	    		unlink($zip);
    		Zipper::make($zip)
    			  ->add($laminas)
    			  ->close();

    		return Response::download($zip);
    	}
    }



}