<?php 

class PaisesController extends ScaffoldController
{	

	function __construct(Pais $pais)
	{
		parent::__construct($pais);

		$this->route = 'regionalizacion.paises';
		$this->titlePage = 'Paises';
		
		$this->filtros['index'] = 'listar_paises';
		$this->filtros['create'] = 'crear_paises';
		$this->filtros['store'] = 'crear_paises';
		$this->filtros['show'] = 'listar_paises';
		$this->filtros['edit'] = 'modificar_paises';
		$this->filtros['update'] = 'modificar_paises';
		$this->filtros['delete'] = 'eliminar_paises';
		
		$this->attachFilters();
	}

}