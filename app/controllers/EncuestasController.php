<?php

class EncuestasController extends ScaffoldController {

	function __construct(Encuesta $encuesta){
		parent::__construct($encuesta);

		$this->route = 'encuesta.encuestas';
		$this->titlePage = 'Encuestas';

		$this->viewCreate = 'encuestas.create';
		$this->viewEdit = 'encuestas.edit';

		$this->attachFilters();
	}

}
