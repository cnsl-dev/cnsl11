<?php 

/**
* 
*/
class EventosController extends ScaffoldController
{
	
	function __construct(Evento $evento)
	{
		parent::__construct($evento);

		$this->route = 'evento.eventos';
		$this->titlePage = 'Evento';

		$this->filtros['index'] = 'listar_eventos';
		$this->filtros['create'] = 'crear_eventos';
		$this->filtros['store'] = 'crear_eventos';
		$this->filtros['show'] = 'listar_eventos';
		$this->filtros['edit'] = 'modificar_eventos';
		$this->filtros['update'] = 'modificar_eventos';
		$this->filtros['delete'] = 'eliminar_eventos';
		
		$this->attachFilters();

		$this->viewIndex = 'eventos.index';
		$this->viewShow = 'eventos.show';
		$this->viewCreate = 'eventos.create';
		$this->viewEdit = 'eventos.edit';
	}
}