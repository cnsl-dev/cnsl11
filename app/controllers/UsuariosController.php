<?php 

/**
* 
*/
class UsuariosController extends BaseController
{

    protected $usuario;
    protected $rol;
    
    function __construct(Usuario $usuario, Rol $rol)
    {
        $this->usuario = $usuario;
        $this->rol = $rol;
        
        $this->beforeFilter('permisos:auth.usuarios.index,listar_usuarios',array('only' => array('index')));
        $this->beforeFilter('permisos:auth.usuarios.show,listar_usuarios',array('only' => array('show')));
        $this->beforeFilter('permisos:auth.usuarios.create,crear_usuarios',array('only' => array('create')));
        $this->beforeFilter('permisos:auth.usuarios.store,crear_usuarios',array('only' => array('store')));
        $this->beforeFilter('permisos:auth.usuarios.edit,modificar_usuarios',array('only' => array('edit')));
        $this->beforeFilter('permisos:auth.usuarios.update,modificar_usuarios',array('only' => array('update')));
        $this->beforeFilter('permisos:auth.usuarios.delete,eliminar_usuarios',array('only' => array('delete')));

        $this->beforeFilter('csrf', array('only'=>array('store', 'update', 'destroy')));
    }


    public function index(){

        $usuarios = $this->usuario->all();

        return View::make('admin.usuarios.index', compact('usuarios'));

    }

    public function create(){
        $roles = $this->rol->lists('nombre', 'id');

        return View::make('admin.usuarios.create', compact('roles'));
    }


    public function store(){
        $input = Input::all();

        $rules = array(
            'email'             => 'required|email|unique:usuarios,email',
            'nombres'           => 'required|alpha_accent',
            'apellidos'         => 'required|alpha_accent',
            'nacionalidad'      => 'required|in:V,E,P',
            'dni'               => 'required|alpha_num',
            'sexo'              => 'required|in:Masculino,Femenino',
            'password'          => 'required|min:6',
            'password-repeat'   => 'required|same:password'
        );

        $validation = Validator::make($input, $rules);
        if($validation->passes()){

            if(isset($input['roles'])){
                $roles = $input['roles'];
                unset($input['roles']);
            }

            $input['dni'] = $input['nacionalidad'].'-'.$input['dni'];
            $input['email'] = mb_strtolower(trim($input['email']));
            $input['password'] = Hash::make($input['password']);

            if($this->usuario->buscarPorCedula($input['dni']))
                return Redirect::back()
                            ->with('message', 'Esta cédula ya ha sido registrada');

            $usuario = $this->usuario->create($input);

            if(isset($roles)) $usuario->roles()->sync($roles);

            return Redirect::route('auth.usuarios.index');
        }

        return Redirect::route('auth.usuarios.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    public function show($id){
        $usuario = $this->usuario->find($id);

        if(Request::ajax())
            return Response::json($usuario);

        return View::make('admin.usuarios.show', compact('usuario'));
    }

    public function edit($id){

        $usuario = $this->usuario->find($id);
        list($usuario->nacionalidad, $usuario->dni) =  explode('-', $usuario->dni);
        $roles = $this->rol->lists('nombre', 'id');

        return View::make('admin.usuarios.edit', compact('usuario', 'roles'));
        
    }


    public function update($id){

        $input = array_except(Input::all(), '_method');

        $rules = array(
            'email'             => 'required|email',
            'nombres'           => 'required|alpha_accent',
            'apellidos'         => 'required|alpha_accent',
            'nacionalidad'      => 'required|in:V,E,P',
            'dni'               => 'required|alpha_num',
            'sexo'              => 'required|in:Masculino,Femenino',
            'password'          => 'same:password-repeat|min:6',
            'password-repeat'   => 'same:password'
        );

        $validation = Validator::make($input, $rules);

        if($validation->passes()){
            $usuario = $this->usuario->find($id);

            if(isset($input['roles'])){
                $roles = $input['roles'];
                unset($input['roles']);
            }

            $input['email'] = mb_strtolower(trim($input['email']));
            $input['dni'] = $input['nacionalidad'].'-'.$input['dni'];

            if($usuario->email != $input['email'] && $this->usuario->buscarPorEmail($input['email']))
                return Redirect::back()
                            ->with('message', 'Esta dirección de correo ya ha sido registrada');
            if($usuario->dni != $input['dni'] && $this->usuario->buscarPorCedula($input['dni']))
                return Redirect::back()
                            ->with('message', 'Esta cédula ya ha sido registrada');

            if(empty($input['password']))
                unset($input['password']);
            else
                $input['password'] = Hash::make($input['password']);

            $usuario->update($input);

            if(isset($roles)) 
                $usuario->roles()->sync($roles);
            else
                $usuario->roles()->sync(array());

            return Redirect::route('auth.usuarios.index');
        }

        return Redirect::route('auth.usuarios.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');

    }


    public function destroy($id){
        
        $this->usuario->find($id)->delete();

        return Redirect::route('auth.usuarios.index');
    }

    public function find($str){
        return Response::json($this->usuario->f1nd($str));
    }
}
