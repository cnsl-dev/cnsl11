<?php 

class GastosController extends ScaffoldController
{	

	function __construct(Gasto $gasto)
	{
		parent::__construct($gasto);

		$this->route = 'contabilidad.gastos';
		$this->titlePage = 'Gastos';
		
		$this->filtros['index'] = 'listar_gastos';
		$this->filtros['create'] = 'crear_gastos';
		$this->filtros['store'] = 'crear_gastos';
		$this->filtros['show'] = 'listar_gastos';
		$this->filtros['edit'] = 'modificar_gastos';
		$this->filtros['update'] = 'modificar_gastos';
		$this->filtros['delete'] = 'eliminar_gastos';
		
		$this->attachFilters();
	}

}