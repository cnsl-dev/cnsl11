<?php 

/**
* 
*/
class OrganizadoresRegionalesController extends ScaffoldController
{	

	function __construct(OrganizadorRegional $organizador)
	{
		parent::__construct($organizador);

		$this->route = 'evento.organizadores-regionales';
		$this->titlePage = 'Organizadores';
		
		$this->filtros['index'] = 'listar_organizadores_regionales';
		$this->filtros['create'] = 'crear_organizadores_regionales';
		$this->filtros['store'] = $this->filtros['create'];
		$this->filtros['show'] = $this->filtros['index'];
		$this->filtros['edit'] = 'modificar_organizadores_regionales';
		$this->filtros['update'] = $this->filtros['edit'];
		$this->filtros['delete'] = 'eliminar_organizadores_regionales';
		
		$this->attachFilters();
	}

	protected function getObj($id){
		return $this->modelObj->where('usuario_id', $id)->get()->first();
	}

}