<?php 

class LoginController extends \BaseController {

	protected $usuario;
	protected $perfil;

	public function __construct(Usuario $usuario, Perfil $perfil){

		$this->usuario = $usuario;
		$this->perfil = $perfil;

		$this->beforeFilter('auth', array('only' => array('getLogout', 'getAdmin', 'getMyAccount', 'getEditAccount', 'postEditEmail', 'postEditProfile', 'getAvatar')));
		$this->beforeFilter('guest', array('only' => array('getLogin', 'postLogin', 'getFindCedula','getCreateAccount')));
		$this->beforeFilter('csrf', array('only'=>array('postLogin', 'postCreateAccount', 'postResendEmailCreateAccount', 'postEditEmail', 'postEditProfile')));
	}

	/**
	 *
	 *	Formulario de login
	 *
	 */
	public function getLogin(){
		return View::make('auth.guest.login');
	}
	

	/**
	 *
	 *	Confirmación de datos de identificación
	 *
	 */
	public function postLogin(){
		
		$input = Input::all();

		$rules = array(
	        'email'					=> 'required|exists:usuarios,email',
	        'password'					=> 'required|alpha_accent',
	        'captcha' 					=> 'required|captcha'
	    );

	    $validation = Validator::make($input, $rules);

	    if ($validation->passes()){

	    	$email = mb_strtolower(trim($input['email']));
			$password = $input['password'];
			$remenberme = isset($input['rememberme']) ? TRUE : FALSE;

			$credenciales = array(
									'email' => $email, 
									'password' => $password,
									'confirmado' => 1,
									'activo' => 1,
									'baneado' => 0
							);

			if (Auth::attempt($credenciales, $remenberme))
	        {
	            return Redirect::intended('/');
	        }

	        return Redirect::back()->with('message', 'Datos incorrectos, vuelve a intentarlo.');
	    }
	    return Redirect::back()
	    	->withInput()
			->withErrors($validation)
			->with('message', 'Campos rellenados de forma incorrecta.');
	}


	/**
	 *
	 *	Cerrar sesion
	 *
	 */
	public function getLogout(){
		Session::flush();
		Auth::logout();
		return Redirect::to('/')->with('message', 'Gracias por visitarnos!.');
	}


	/**
	 *
	 *	Raiz del panel de administración
	 *
	 */
	public function getAdmin(){
		return View::make('auth.admin');
	}

}