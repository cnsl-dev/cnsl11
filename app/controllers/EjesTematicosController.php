<?php

class EjesTematicosController extends \BaseController {

	/**
	 * Display a listing of ejetematicos
	 *
	 * @return Response
	 */
	public function index()
	{
		$ejetematicos = Ejetematico::all();

		return View::make('ejetematicos.index', compact('ejetematicos'));
	}

	/**
	 * Show the form for creating a new ejetematico
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('ejetematicos.create');
	}

	/**
	 * Store a newly created ejetematico in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Ejetematico::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Ejetematico::create($data);

		return Redirect::route('ejetematicos.index');
	}

	/**
	 * Display the specified ejetematico.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$ejetematico = Ejetematico::findOrFail($id);

		return View::make('ejetematicos.show', compact('ejetematico'));
	}

	/**
	 * Show the form for editing the specified ejetematico.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ejetematico = Ejetematico::find($id);

		return View::make('ejetematicos.edit', compact('ejetematico'));
	}

	/**
	 * Update the specified ejetematico in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$ejetematico = Ejetematico::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Ejetematico::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$ejetematico->update($data);

		return Redirect::route('ejetematicos.index');
	}

	/**
	 * Remove the specified ejetematico from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Ejetematico::destroy($id);

		return Redirect::route('ejetematicos.index');
	}

}
