<?php 

/**
* 
*/
class PreguntasController extends ScaffoldController
{
	protected $pregunta;
	
	function __construct(Pregunta $pregunta)
	{
		parent::__construct($pregunta);
		// $this->pregunta = $pregunta;

		$this->route = 'encuesta.preguntas';
		$this->titlePage = 'Pregunta';

		// $this->filtros['index'] = 'listar_preguntas';
		// $this->filtros['create'] = 'crear_preguntas';
		// $this->filtros['store'] = 'crear_preguntas';
		// $this->filtros['show'] = 'listar_preguntas';
		// $this->filtros['edit'] = 'modificar_preguntas';
		// $this->filtros['update'] = 'modificar_preguntas';
		// $this->filtros['delete'] = 'eliminar_preguntas';
		
		// $this->attachFilters();

		// $this->viewIndex = 'eventos.index';
		// $this->viewShow = 'eventos.show';
		// $this->viewCreate = 'eventos.create';
		// $this->viewEdit = 'eventos.edit';
	}
}