<?php 

/**
* 
*/
class DepartamentosController extends ScaffoldController
{

	function __construct(Departamento $departamento)
	{
		parent::__construct($departamento);

		$this->route = 'regionalizacion.departamentos';
		$this->titlePage = 'Departamentos';
		
		$this->filtros['index'] = 'listar_departamentos';
		$this->filtros['create'] = 'crear_departamentos';
		$this->filtros['store'] = 'crear_departamentos';
		$this->filtros['show'] = 'listar_departamentos';
		$this->filtros['edit'] = 'modificar_departamentos';
		$this->filtros['update'] = 'modificar_departamentos';
		$this->filtros['delete'] = 'eliminar_departamentos';
		
		$this->attachFilters();

	}

	public function find($departamento_id){

		$validator = Validator::make(
							array('departamento_id' => $departamento_id), 
							array('departamento_id' => 'required|integer|exists:departamentos,id')
					);

		if($validator->fails())
			return Response::json(array('message' => 'Erroes en los campos de entrada', 'errors' => $validator->messages()));

		$departamento = $this->modelObj->find($departamento_id);
		if(! $departamento)
			return Response::json(array('message' => 'los datos que ha especificado no pudieron ser encontrados'), 404);

		return Response::json(array('obj' => $departamento->ciudades()->lists('nombre', 'id')));
	}
}