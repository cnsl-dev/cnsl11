<?php

class SignUpController extends \BaseController {

	protected $usuario;
	protected $perfil;

	public function __construct(Usuario $usuario, Perfil $perfil){

		$this->usuario = $usuario;
		$this->perfil = $perfil;

		$this->beforeFilter('auth', array('only' => array('getLogout', 'getAdmin', 'getMyAccount', 'getEditAccount', 'postEditEmail', 'postEditProfile', 'getAvatar')));
		$this->beforeFilter('guest', array('only' => array('getLogin', 'postLogin', 'getFindCedula','getCreateAccount')));
		$this->beforeFilter('csrf', array('only'=>array('postLogin', 'postCreateAccount', 'postResendEmailCreateAccount', 'postEditEmail', 'postEditProfile')));
	}

	/**
	 *
	 *	Vista de busqueda por cedula
	 *
	 */
	public function getFindCedula(){
		return View::make('auth.guest.buscar_cedula');
	}

	/**
	 *
	 *	Vista de busqueda por cedula
	 *
	 */
	public function postFindCedula(){

		$input = Input::only('nacionalidad', 'dni', 'captcha');

		$rules = array(
            'dni'				=> 'required|integer',
            'nacionalidad'		=> 'required|in:V,E,P',
            'captcha'			=> 'required|captcha'
        );

		$validation = Validator::make($input, $rules);
		if($validation->fails())
            return Redirect::back()
                    ->withInput()
                    ->withErrors($validation)
                    ->with('message', Config::get('app.validation_errors'))
                    ->with('class', 'danger');

    	if($message = $this->estaRegistrado($input['nacionalidad'].'-'.$input['dni']))
    		return Redirect::back()
    						->with('message',$message);

        // busca si el usuario se registro en puerta antes de registarse en linea
        $usuario = $this->usuario->buscarPorCedula($input['nacionalidad'].'-'.$input['dni']);

        $old = TRUE;
        $create = TRUE;
        if(is_object($usuario)){

            $old = FALSE;
            $create = FALSE;
            
        }elseif(($usuario = RegistroAntiguo::buscarPorCedula($input['dni']))!==FALSE){

            $usuario->email            = $usuario->correo;
            $usuario->dni              = $usuario->ve.'-'.$usuario->cedula;

        }else{
            $old = FALSE;
            $usuario = new RegistroAntiguo();
            $usuario->dni              = $input['nacionalidad'].'-'.$input['dni'];
        }

    	if(!empty($usuario->sexo) && $old)
    		$usuario->sexo = $usuario->sexo == 'm' ? 'Masculino' : 'Femenino';
		
		return Redirect::route('create_account')
						->with('usuario', serialize($usuario))
                        ->with('create', serialize($create))
						->with('old', serialize($old));
	}


	/**
	 *
	 *	Formulario de registro de usuarios
	 *
	 */
	public function getCreateAccount(){
		if(Session::has('usuario') && Session::has('create') && Session::has('old')){
        	$usuario = unserialize(Session::get('usuario'));
            list($usuario->nacionalidad, $usuario->dni) = explode('-', $usuario->dni);
            $create = unserialize(Session::get('create'));
        	$old = unserialize(Session::get('old'));

    	}else{
    		$usuario = new RegistroAntiguo();
    		$old = FALSE;
            $create = TRUE;
    	}
		
		return View::make('auth.guest.create_account', compact('usuario', 'create', 'old'));
	}


	/**
	 *
	 *	Creación de la cuenta
	 *
	 */
	public function postCreateAccount(){
		$input = Input::only('create', 'old','nombres', 'apellidos', 'nacionalidad', 'dni', 'sexo', 'email', 'password', 'password-repeat', 'captcha');

		$input['email'] = mb_strtolower(trim($input['email']));

		$rules = array(
            'create'            => 'required|in:0,1',
            'old'				=> 'required|in:0,1',
            'nombres' 	        => 'required|alpha_accent',
            'apellidos'         => 'required|alpha_accent',
            'dni'				=> 'required|integer',
            'sexo'				=> 'required|in:Masculino,Femenino',
            'nacionalidad'		=> 'required|in:V,E,P',
            'email'             => 'required|email|unique:usuarios,email',
            'password'          => 'required|alpha_accent|min:6',
            'password-repeat'   => 'required|same:password',
            'captcha'			=> 'required|captcha'
       	);

        $validation = Validator::make($input, $rules);
        if($validation->passes()){

        	if($message = $this->estaRegistrado($input['nacionalidad'].'-'.$input['dni']))
        		return Redirect::back()
        						->with('message',$message);

        	$confirmation_code = str_random(64);

            $input['password'] = Hash::make($input['password']);
            $input['confirmation_code'] = $confirmation_code;
            $input['dni'] = $input['nacionalidad'].'-'.$input['dni'];

            /*
             *  Comprueba si debe crear un nuevo registro o modificar uno existente
             */
            if($input['create'] == 1){
                // Comprueba si debe tomar datos de los antiguos registros
                if($input['old']=='1'){
                    list($nacionalidad, $dni) = explode('-', $input['dni']);
                    $old_registro = RegistroAntiguo::buscarPorCedula($dni);
                    $input['fecha_nacimiento'] = $old_registro['fecha_nac'];
                    $input['direccion'] = $old_registro['direccion'];
                    $input['telefono'] = $old_registro['telefono'];
                    $input['web'] = $old_registro['web'];
                }
                // Crea el nuevo usuario
                $usuario = $this->usuario->create($input);
                $input['usuario_id'] = $usuario->id;
                $this->perfil->create($input);
            }else{
                // Actualiza los datos del usuario
                $usuario = $this->usuario->buscarPorCedula($input['dni']);
                $input['por_confirmar_en_linea'] = FALSE;
                $input['activo'] = TRUE;
                $usuario->update($input);
            }

            if(isset($roles)) $usuario->roles()->sync($roles);

            Mail::send('emails.verify', compact('confirmation_code'), function($message) use ($input) {
	            $message->to($input['email'], $input['nombres'].' '.$input['apellidos'])
	                ->subject('Confirmación de dirección de correo');
	        });

            return Redirect::route('login')->with('message', 'Su cuenta ha sido creada, revise su cuenta de correo para confirmarla');
        }

        return Redirect::route('create_account')
            ->withInput()
            ->withErrors($validation)
            ->with('message', Config::get('app.validation_errors'));
	}


	/**
	 *
	 *	Validar codigo de confirmación y activar cuenta
	 *
	 */
	public function getCofirmEmail($confirmation_code){
		if( ! $confirmation_code)
        {
            return Redirect::route('create_account');
        }

        $usuario = $this->usuario->whereConfirmationCode($confirmation_code)->first();

        if ( ! $usuario)
        {
            return Redirect::route('create_account')
            ->with('message', 'Su código de verificació es invalido.');
        }

        $usuario->confirmado = 1;
        $usuario->confirmation_code = null;
        $usuario->save();

        $usuario->roles()->attach(7);

        return Redirect::route('login')
        	->with('message', 'Has confirmado tu cuenta satisfactoriamente.');
	}


	/**
	 *
	 *	Formulario de reenvio de correo de verificicación
	 *
	 */
	public function getResendEmailCreateAccount(){
		return View::make('auth.guest.resend_verify');
	}


	/**
	 *
	 *	Reevio de correo de verificación
	 *
	 */
	public function postResendEmailCreateAccount(){
		$input = Input::only('email');

		$input['email'] = mb_strtolower(trim($input['email']));

		$rules = array(
            'email'             => 'required|email|exists:usuarios,email',
            'captcha'			=> 'required|captcha',
        );

        $validation = Validator::make($input, $rules);
        if($validation->passes()){

        	$usuario = $this->usuario
        					->where('email',$input['email'])
        					->where('confirmado', 0)
        					->first();

        	if(! $usuario){
        		return Redirect::route('resend_verify')
            		->with('message', 'Este usuario ya se encuentra activo');
        	}

        	if(empty($usuario->confirmation_code)){
        		$usuario->confirmation_code = str_random(64);
        		$usuario->save();
        	}

        	$confirmation_code = $usuario->confirmation_code;

        	Mail::send('emails.verify', compact('confirmation_code'), function($message) use ($usuario) {
	            $message->to($usuario->email, $usuario->username)
	                ->subject('Confirmación de dirección de correo');
	        });

            return Redirect::route('login')->with('message', 'Su correo de confirmación ha sido enviado, revise su cuenta de correo para confirmarla');

        }

        return Redirect::route('resend_verify')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
	}


	
	/**
	 *
	 *	Busca coincidencia de usuarios a partir del numero de cedula
	 *
	 */
	protected function estaRegistrado($cedula){

		$usuario = $this->usuario->buscarPorCedula($cedula);

		if(is_object($usuario) && $usuario->por_confirmar_en_linea == 0){
			switch ($usuario->status()) {
				case 'inactivo':
					return 'Este usuario ya se encuentra registrado, falta la confirmación de la misma';
				case 'baneado':
					return 'Esta cuenta ha sido suspendida';
				default:
					return 'Este usuario ya se encuentra registrado';
			}
		}
		return FALSE;
	}
}
