<?php 

class PlantillasController extends ScaffoldController
{	

	function __construct(Plantilla $plantilla)
	{
		parent::__construct($plantilla);

		$this->route = 'evento.plantillas';
		$this->titlePage = 'Plantillas';
		
		$this->filtros['index'] = 'listar_plantillas';
		$this->filtros['create'] = 'crear_plantillas';
		$this->filtros['store'] = 'crear_plantillas';
		$this->filtros['show'] = 'listar_plantillas';
		$this->filtros['edit'] = 'modificar_plantillas';
		$this->filtros['update'] = 'modificar_plantillas';
		$this->filtros['delete'] = 'eliminar_plantillas';
		
		$this->attachFilters();
	}

}