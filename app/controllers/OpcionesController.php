<?php

class OpcionesController extends ScaffoldController {

	function __construct(Opcion $opcion){
		parent::__construct($opcion);

		$this->route = 'encuesta.opciones';
		$this->titlePage = 'Opciones';

		$this->attachFilters();
	}

}