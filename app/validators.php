<?php


Validator::extend('alpha_accent', function($attribute, $value, $parameters)
{
    return preg_match("/^([-a-z0-9_-ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöùúûüýøþÿÐdŒ\s-])+$/i", $value);
});


Validator::extend('string_accent', function($attribute, $value, $parameters)
{
    return preg_match("/^([-a-z0-9_-ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöùúûüýøþÿÐdŒ\s\W-])+$/i", $value);
});

Validator::extend('required_with', function($attribute, $value, $parameters){
	
});

Validator::extend('float', function($attribute, $value, $parameters){
	return preg_match("/^\d{0,5}\.?\d{0,2}$/", $value);
});
