<?php 

return array(

	'General' => array(

		'es_administrador',

	),
	'Autenticacion' => array(

		'crear_roles',
		'listar_roles',
		'modificar_roles',
		'eliminar_roles',


		'crear_usuarios',
		'listar_usuarios',
		'modificar_usuarios',
		'eliminar_usuarios',


		'asignar_permisos',
		'listar_permisos',
		'modificar_permisos',
		'remover_permisos',

	),
	'Encuestas' => array(

		'crear_encuestas',
		'listar_encuestas',
		'modificar_encuestas',
		'eliminar_encuestas'

	),
	'Eventos' => array(

		'crear_sedes',
		'listar_sedes',
		'modificar_sedes',
		'eliminar_sedes',


		'asignar_organizador',

		
		'crear_registro_para_sede',
		'listar_registro_para_sede',
		'modificar_registro_para_sede',
		'elminar_registro_para_sede',
		
		
		'crear_reportes_de_sedes',
		'listar_reportes_de_sedes',
		'modificar_reporte_de_sedes',
		'eliminar_reporte_de_sedes',


		'crear_ponencias',
		'listar_ponencias',
		'modificar_ponencias',
		'eliminar_ponencias',
		'aprobar_ponencias',

		
		'imprimir_certificados',

	),
	'Recursos' => array(

		'registrar_ingresos',
		'listar_ingresos',
		'modificar_ingresos',
		'confirmar_ingresos',
		'eliminar_ingresos',


		'registrar_gastos',
		'listar_gastos',
		'modificar_gastos',
		'eliminar_gastos',

	)


);


