<?php 

return array(

	'avatar_default' => 'unknown_user.jpg',
	'directory_avatars' => app_path().'/private/avatars/',
	'directory_laminas' => app_path().'/private/laminas/',
);