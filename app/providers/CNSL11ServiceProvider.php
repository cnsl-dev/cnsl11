<?php 

namespace H34\Providers;

use Illuminate\Support\ServiceProvider;
use H34\Utilidades\arrayUtilidades;
use H34\Utilidades\datetimeUtilidades;

class CNSL11ServiceProvider extends ServiceProvider
{
	
	public function register()
    {
        $this->app->bind('arrayUtils', function()
        {
            return new arrayUtilidades;
        });
        $this->app->bind('datetimeUtils', function()
        {
            return new datetimeUtilidades;
        });
    }
}