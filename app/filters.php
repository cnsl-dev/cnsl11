<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	setlocale(LC_ALL, Config::get('app.phplocale'));
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest(route('login'));
		}
	}

	ConfigApp::buildMenu();
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		Log::info(Input::all());
		throw new Illuminate\Session\TokenMismatchException;
	}
});


Route::filter('evento_activo', function(){
	if(!Evento::activo())
		return View::make('errors.evento_inactivo');
});

Route::filter('has_perfil', function(){
	if (Auth::guest()) return Redirect::guest(route('login'));

	$perfil = Auth::user()->perfil();
	if( $perfil->count()==0 || 
		empty($perfil->first()->dni) ||
		empty($perfil->first()->nombre) || 
		empty($perfil->first()->apellido)
	)
		Redirect::route('my_account');
});

/*
 *	Filtro de acceso por permisos
 */
Route::filter('permisos', function($route, $request, $ruta, $permisos=''){
    if (Auth::guest()) return Redirect::guest(route('login'));
    
    if(!empty($permisos)){
		if($route->getName()==$ruta){
			$permisos = explode('|', $permisos);
			foreach ($permisos as $key => $permiso) {
				if(in_array($permiso, Auth::user()->permisos()))
					return;
			}
			
			return Response::view('errors.403', array(), 403);
		}
    }
});