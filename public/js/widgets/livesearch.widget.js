H34.createNS("H34.WIDGETS");

H34.WIDGETS.livesearch = function(column, url, input, onSelected){

	onSelected || ( onSelected = null );

	var colection = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace(column),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: url,
	});

	colection.initialize();

	var typeahead = $(input).typeahead(null, {
		displayKey: column,
		source: colection.ttAdapter(),
	})

	if(onSelected === null) return;

	typeahead.on('typeahead:selected', onSelected);


}