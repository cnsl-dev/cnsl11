H34.createNS("H34.WIDGETS");

H34.WIDGETS.datepicker = function(){
	jQuery('.datepicker').datetimepicker({
	  format:'d/m/Y',
	  lang:'es',
	  timepicker:false,
	});
};


H34.WIDGETS.datetimepicker = function(){
	jQuery('.datetimepicker').datetimepicker({
		format:'d/m/Y H:i',
		lang:'es',
		allowTimes:[
			'13:00',
			'13:30',
			'14:00',
			'14:30',
			'15:00',
			'15:30',
			'16:00',
			'16:30',
			'17:00',
			'17:30',
			'18:00',
			'18:30',
			'19:00'
		],
		defaultTime:'16:00'
	});
};

H34.CONFIG.coleccion.agregar(H34.WIDGETS.datetimepicker);
H34.CONFIG.coleccion.agregar(H34.WIDGETS.datepicker);