H34.createNS("H34.WIDGETS");

H34.WIDGETS.check = function(){
	
	class_size = 'btn-xs'

	$('.checkbox').checkboxpicker({
					'onLabel':'Si',
					'defaultClass': 'btn-default '+class_size,
					'offClass': 'btn-danger '+class_size,
					'onClass': 'btn-success '+class_size
					});
}

H34.CONFIG.coleccion.agregar(H34.WIDGETS.check);