// create the root namespace and making sure we're not overwriting it
var H34 = H34 || {};

H34.createNS = function (namespace) {
    var nsparts = namespace.split(".");
    var parent = H34;
    // we want to be able to include or exclude the root namespace
    // So we strip it if it's in the namespace
    if (nsparts[0] === "H34") {
        nsparts = nsparts.slice(1);
    }

    // loop through the parts and create
    // a nested namespace if necessary
    for (var i = 0; i < nsparts.length; i++) {
        var partname = nsparts[i];
        // check if the current parent already has
        // the namespace declared, if not create it
        if (typeof parent[partname] === "undefined") {
            parent[partname] = {};
        }
        // get a reference to the deepest element
        // in the hierarchy so far
        parent = parent[partname];
    }
    // the parent is now completely constructed
    // with empty namespaces and can be used.
    return parent;
}


/*
 *  Se crea el namespace para config de la aplicacion
 */

H34.createNS("H34.CONFIG");

H34.CONFIG.funciones = function(){

	var funciones = [];

	this.agregar = function(funcion){
		funciones.push(funcion);
	};

	this.ejecutar = function(){
		for (var i = 0; i < funciones.length; i++) {
			funciones[i]()
		};
	};
}

H34.CONFIG.coleccion = new H34.CONFIG.funciones();

/*
 *  Se crea el namespace para utilities
 */
H34.createNS("H34.UTILITIES");

// retorna el fragmento de la url que se le especifique
// en el parametro "n" de no especificar ninguno se retorna
// el ultimo
H34.UTILITIES.url = function(n){
    n || ( n = null );
    var url = window.location.href;
    url = url.split('/');
    if(n===null) 
        return url[url.length-1];
    return (n <= url.length-1 && n > 0) ? url[n] : null;
}

// agrega la clase "active" a los enlaces que sean igual a la url actual
H34.UTILITIES.activeUrls = function(){
    url_actual = document.URL
    $.each($('a'), function(key, value){
        if(value.href == url_actual){
            $(value).addClass('active');
            if(value.parentNode.tagName === 'LI')
                $(value.parentNode).addClass('active');
        }
    })
}

H34.CONFIG.coleccion.agregar(H34.UTILITIES.activeUrls);

// Agrega un evento on click al select que se pase en el primer parametro
// cuando se produce el evento se dispara a función trigger
H34.UTILITIES.selectSearch = function(select, trigger){
    $(select +' option').on('click', trigger);
}

// devuelve los valores de los opciones seleccinados en una etiqueta select
H34.UTILITIES.getOptionsSelected = function(select_id){
    options = $('#'+select_id+ ' option')
    simple = !document.getElementById(select_id).multiple
    selected = simple ? '' : []
    $.each(options, function(key, option){
        if(option.selected)
            simple ? selected = option.value : selected.push(option.value)
    })
    return selected;
}

/*
 *  Inicializar tolltips
 */

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
