H34.createNS("H34.LOGIC");

H34.LOGIC.ciudad = function(){
	
	$('.departamentos-select').on('change', function(){

		option = $('.departamentos-select option:selected')

		estado_id = option.val()

		$.ajax({
			url: '/regionalizacion/departamentos/'+ estado_id +'/ciudades',
		})
		.done(function(response){
			lista = construir_lista(response.obj)
			$('#ciudad_id').html(lista)
		})
		.fail(function(response){
		})
	})

	construir_lista = function(obj){
		out = '<option>--- Selecciona una ciudad ---</option>'
		$.each(obj, function(key, value){
			out += '<option value="'+ key +'">'+ value +'</option>'
		})
		return out
	}

}

H34.CONFIG.coleccion.agregar(H34.LOGIC.ciudad);