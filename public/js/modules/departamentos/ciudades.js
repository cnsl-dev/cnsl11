H34.createNS("H34.LOGIC");

H34.LOGIC.ciudades = function(){
	
	$('.departamentos-select').on('change', function(){

		option = this.options[this.selectedIndex]

		estado_id = option.value
		select = this
		id = select.id.split('_')[1]

		$.ajax({
			url: '/regionalizacion/departamentos/'+ estado_id +'/ciudades',
		})
		.done(function(response){
			lista = construir_lista(response.obj)
			$('#ciudad_'+id).html(lista)
		})
		.fail(function(response){
			console.log("ha ocurrido un error :-(");
		})
	})


	construir_lista = function(obj){
		out = '<option>--- Selecciona una ciudad ---</option>'
		$.each(obj, function(key, value){
			out += '<option value="'+ key +'">'+ value +'</option>'
		})
		return out
	}

}

H34.CONFIG.coleccion.agregar(H34.LOGIC.ciudades);