H34.createNS("H34.LOGIC.organizadores");

H34.LOGIC.organizadores.unlink = function(){

	$('.unlink-organizador').on('click', function(e){
		e.preventDefault();

		parent = this.parentNode.parentNode;
		organizador = $('.organizador', parent).text()
		route = $(this).attr('data-route')
		token = $('#token').val()
		sede = $('.sede', parent).text()

		mensaje = '¿Confirma que desea eliminar a <strong>' + organizador + '</strong> como organizador de la sede de <strong>' + sede + '</strong>?'
		BootstrapDialog.confirm(mensaje, function(result){
			if(result){
				$.ajax({
					type: 'post',
					url : route,
					data: {_method:'delete', _token:token}
				})
				.done(function(resp){
					location.reload();
				})
				.fail(function(){
					BootstrapDialog.alert('Un error se produjo, el registro no pudo ser eliminado')
				})
			}
		});
	});

}

H34.CONFIG.coleccion.agregar(H34.LOGIC.organizadores.unlink);