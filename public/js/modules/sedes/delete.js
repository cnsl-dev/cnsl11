H34.createNS("H34.LOGIC.sedes");


H34.LOGIC.sedes.delete = function(){

	$('.deleteConfirm').on('click', function(evt){

        token = this.getAttribute('data-token')
        route = this.getAttribute('data-route')

		BootstrapDialog.confirm(mensaje, function(result){
			if(result){
				$.ajax({
					type: 'post',
					url : route,
					data: {_method:'delete', _token:token}
				})
				.done(function(resp){
					location.reload();
				})
				.fail(function(){
					BootstrapDialog.alert('Se ha producido un error, el registro no pudo ser eliminado')
				})
			}
		});

	}
}

H34.CONFIG.coleccion.agregar(H34.LOGIC.sedes.delete);