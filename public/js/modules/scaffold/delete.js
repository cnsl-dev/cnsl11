/**********************************************************************
    Muestra la ventana modal con el mensaje de confirmación 
    para eliminar el registro
**********************************************************************/

H34.UTILITIES.ShowModalDelete = function(){

    $('.deleteConfirm').on('click', function(evt){

        id = this.getAttribute('data-id')
        token = this.getAttribute('data-token')
        route = $("#modalDelete").attr('data-route').replace('.','/')

        $.ajax({
            url: '/' + route + '/' + id,
        }).done(function(response){
            mensaje = '<ul>';
            $.each(response, function(key, value){
                if((typeof value == 'object') && (value !== null)){
                    mensaje += '<ul>';
                    $.each(value, function(key2, value2){
                        if($.inArray(key2, ['created_at', 'updated_at', 'deleted_at', 'id'])==-1)
                            mensaje += '<li>'+key2+': '+value2+'</li>';
                    });
                    mensaje += '</ul>'
                }else
                    if($.inArray(key, ['created_at', 'updated_at', 'deleted_at', 'id'])==-1)
                        mensaje += '<li>'+key+': '+value+'</li>';
                
            });
            mensaje += '</ul>'
            $('#dataObject').html(mensaje)
            $("#deleteAccept").attr('data-id', id)
            $("#deleteAccept").attr('data-token', token)
            $('#modalDelete').modal('show')
        });
    });

}
H34.CONFIG.coleccion.agregar(H34.UTILITIES.ShowModalDelete);


/**********************************************************************
    Elimina el registro especificado cuando se indique
    en la confirmación de la eliminación del mismo en 
    la ventana modal
**********************************************************************/

H34.UTILITIES.confirmDelete = function(){

    $("#deleteAccept").on('click', function(evt){

        id = this.getAttribute('data-id')
        token = this.getAttribute('data-token')
        route = $("#modalDelete").attr('data-route').replace('.','/')

        datos = {
            type: 'post',
            data:{_method: 'delete', _token: token},
            url: '/' + route + '/' + id
        }
        console.log(datos)
        $.ajax(datos).done(function(response){
            console.log(response)
            location.reload();
            $('#modalDelete').modal('hide')
        }).fail(function(response){
            console.log(response)
        })
        ;
    });
}
H34.CONFIG.coleccion.agregar(H34.UTILITIES.confirmDelete);


/**********************************************************************
            Refesca la tabla luego de las modificaciones
**********************************************************************/
H34.UTILITIES.renderTableList = function(data){
    tabla = ''
    tabla += '<table class="table table-striped table-bordered">'
    tabla += '<head>'
    tabla += '<tr>'
    $.each(data['listObjs'][0], function(key, value){
        tabla += '<th>'+ key + '</th>'
    });
    tabla += '</tr>'
    tabla += '</head>'
    tabla += '<body>'
        for (var i = 0; i < data['listObjs'].length; i++) {
            tabla += '<tr>'
            $.each(data['listObjs'][i], function(key, value){
                tabla += '<td>'+ value + '</td>'
            });
            tabla +=
            tabla += '</tr>'
        };
    tabla += '</body>'
    tabla += '</table>'

    return tabla;
}