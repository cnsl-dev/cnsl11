
H34.UTILITIES.save = function(){
	$('#submit').on('click', function(){
		/**
		 *	Botones addmore
		 */
		botones = $('*[data-container]');
		/**
		 * Conseguir los contenedores addmore
		 */
		contenedores = ''
		$.each(botones,function(key, value){
			contenedores += ', #' + value.getAttribute('data-container') + ' .input-group';
		})
		/**
		 * Conseguir todos los inputs, textarea, select
		 */
		form_controls = $('form' + contenedores).children('input[type!=hidden], textarea, select');
		console.log(form_controls)
		/**
		 * Conseguir todos los valores de los inputs, selects y textareas para almacenarlos en
		 * la variable datos
		 */
		datos = {}
		$.each(form_controls, function(key, value){
			if(value.tagName == 'SELECT'){
				datos[value.getAttribute('name')] = H34.UTILITIES.getOptionsSelected(value.id)
			}
			else
				datos[value.getAttribute('name')] = value.value
		})

		/**
		 *	Crear el objeto con los valres de configuración de la peticion
		 */
		conf = {
			type: 'post',
            url: $('form').attr('action'),
            data: datos
		}
		console.log(conf)
		/**
		 *	Verificar si se esta creando un nuevo objeto o editando uno existente
		 */
		if(H34.UTILITIES.url()=='edit') conf['data']['_method'] = 'put'
		/**
		 * Enviar la peticion
		 */
		$.ajax(conf).done(function(response){
			// si la petición falla en la operación mostrar los mensajes de error
        	if(response.message === 'error'){
        		errors = '<ul>';
        		$.each(response.errors, function(key, value){
        			errors += '<li>'+value+'</li>'
        		})
        		errors += '</ul>';
        		$('.message').html(errors)

        		$.each(response.fails, function(key, value){
        			$('form *[name='+key+']').css('border-color', 'red');
        			$('form *[name='+key+']').css('background', 'pink');
        		})
        	}else{        		
            	window.location = $('form').attr('action');
        	}
        });
	})
}

H34.CONFIG.coleccion.agregar(H34.UTILITIES.save);