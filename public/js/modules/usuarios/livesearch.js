H34.createNS("H34.LOGIC");

H34.LOGIC.usuarios_lv = function(){

	// funcion que se ejecuta cuando una opcion es seleccionada en livesearch
	var rellenar_campos_modal = function(jq, sug, data){
		$('#nombre').html(sug.nombres)
		$('#apellido').html(sug.apellidos)
		$('#ciudad').html(sug.ciudad)
		$('#usuario_id').val(sug.id)
	}

	// agregar la funcionalidad live search para input#agregar
	H34.WIDGETS.livesearch('value', '/auth/usuarios/find/%QUERY', '#agregar', rellenar_campos_modal)

}

H34.CONFIG.coleccion.agregar(H34.LOGIC.usuarios_lv);
