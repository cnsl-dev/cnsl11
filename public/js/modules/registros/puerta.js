H34.createNS("H34.LOGIC");

H34.LOGIC.puerta = function(){

	if($('#no-certificado').attr('checked'))
		$('#data-certificado').css('display', 'none');

	if($('#efectivo').attr('checked'))
		$('#codigo_operacion').css('display', 'none');

	if($('#exonerado').attr('checked')){
		$('#contenedor-razon').css('display', 'initial');
		$('#contenedor-pago').css('display', 'none');		// OCULTA las opciones para especificar el tipo de pago
		$('#contenedor-monto').css('display', 'none');		// OCULTA las opciones para especificar el monto a pagar
	}else{
		$('#contenedor-pago').css('display', 'initial');	// hace VISIBLE las opciones para especificar el tipo de pago
		$('#contenedor-monto').css('display', 'initial');	// hace VISIBLE las opciones para especificar el monto a pagar
		$('#contenedor-razon').css('display', 'none');
		$("#select_exon option")[0].selected = 'selected';
	}


	/*
	 *	Certificado
	 */
	$('#si-certificado').on('click', function(){
		$('#data-certificado').css('display', 'initial');
	});
	$('#no-certificado').on('click', function(){
		$('#data-certificado').css('display', 'none');
	});

	/*
	 *	Depositos y transferencias
	 */
	$('#deposito').on('click', function(){
		$('#codigo_operacion').css('display', 'initial').focus();
	});
	$('#transferencia').on('click', function(){
		$('#codigo_operacion').css('display', 'initial').focus();
	});
	$('#efectivo').on('click', function(){
		$('#codigo_operacion').css('display', 'none');
	});

	/*
	 * Exonerados
	 */
	$('#exonerado').on('click', function(){
		if(this.checked){	// si es exonerado
			$('#contenedor-razon').css('display', 'initial');
			$('#contenedor-pago').css('display', 'none');		// OCULTA las opciones para especificar el tipo de pago
			$('#contenedor-monto').css('display', 'none');		// OCULTA las opciones para especificar el monto a pagar
		}else{				// si NO es exonerado
		 	$('#contenedor-pago').css('display', 'initial');	// hace VISIBLE las opciones para especificar el tipo de pago
			$('#contenedor-monto').css('display', 'initial');	// hace VISIBLE las opciones para especificar el monto a pagar
			$('#contenedor-razon').css('display', 'none');
			$("#select_exon option")[0].selected = 'selected';
			
		}
	});

}

H34.CONFIG.coleccion.agregar(H34.LOGIC.puerta);
