H34.createNS("H34.LOGIC.registros");

H34.LOGIC.registros.deleteConfirm = function(){

	$('.deleteConfirm').on('click', function(evt){
		id = this.getAttribute('data-id')
        token = this.getAttribute('data-token')
        route = this.getAttribute('data-route')

        mensaje = '¿confirma que desea eliminar este regitro?'
        BootstrapDialog.confirm(mensaje, function(result){
        	if(result){
        		console.log('route: ', route+'/'+id)
				$.ajax({
					type: 'post',
					url : route+'/'+id,
					data: {_method:'delete', _token:token}
				})
				.done(function(resp){
					location.reload();
				})
				.fail(function(e){
					console.log(e)
					BootstrapDialog.alert('Un error se produjo, el registro no pudo ser eliminado')
				})
			}
        })
	})

}

H34.CONFIG.coleccion.agregar(H34.LOGIC.registros.deleteConfirm);