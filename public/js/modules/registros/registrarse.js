H34.createNS("H34.LOGIC");

H34.LOGIC.registrarse = function(){

	sede = document.getElementById('sede')
	if(sede.options[sede.selectedIndex].value == 0)
		$('#data-certificado').css('display', 'none');

	if(document.getElementById('si-certificado').checked)
		$('#desea-certificado').css('display', 'initial');
	else
		$('#desea-certificado').css('display', 'none');


	/*
	 *	Seleccionar sede
	 */
	$('#sede option').on('click', function(){
		if(this.value != 0)
			$('#data-certificado').css('display', 'initial');
		else
			$('#data-certificado').css('display', 'none');
	})

	/*
	 *	Certificado
	 */
	$('#si-certificado').on('click', function(){
		$('#desea-certificado').css('display', 'initial');
	});
	$('#no-certificado').on('click', function(){
		$('#desea-certificado').css('display', 'none');
	});

}

H34.CONFIG.coleccion.agregar(H34.LOGIC.registrarse);