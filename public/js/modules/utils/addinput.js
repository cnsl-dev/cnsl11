n_opcion = 1;

H34.UTILITIES.addinput = function(){
	$('#agregar').on('click', function(e){
		e.preventDefault()
		nombre_contenedor = this.getAttribute('data-container')
		opciones = document.getElementById(nombre_contenedor);
		//crear input
		new_input = document.createElement('input');
		new_input.type = 'text';
		new_input.name = nombre_contenedor +'_' + (++n_opcion);
		$(new_input).addClass('form-control');
		//crear span
		new_span = document.createElement('span')
		$(new_span).addClass('input-group-btn');
		//crear button
		new_button = document.createElement('button')
		$(new_button).addClass('btn btn-danger');
		$(new_button).on('click', H34.UTILITIES.deleteinput);
		new_span.appendChild(new_button)
		// crear i
		new_i = document.createElement('i')
		$(new_i).addClass('fa fa-minus-square');
		new_button.appendChild(new_i)
		//crear div
		new_div = document.createElement('div');
		$(new_div).addClass('input-group');
		new_div.appendChild(new_input)
		new_div.appendChild(new_span)
		opciones.appendChild(new_div);
	})

	$('.delete-option').on('click', H34.UTILITIES.deleteinput);

	
}

H34.CONFIG.coleccion.agregar(H34.UTILITIES.addinput);


H34.UTILITIES.deleteinput = function(e){
		e.preventDefault();
		input = this.parentNode.parentNode.childNodes[0].tagName == 'INPUT'
				? this.parentNode.parentNode.childNodes[0]
				: this.parentNode.parentNode.childNodes[1]
		respuesta =confirm('¿Confirma que desea eliminar: ' + '"' + input.value + '"?')
		console.log(input)
		if(respuesta){
			node = input.parentNode; // div que contiene al input
			node.parentNode.removeChild(node)
			console.log(node)
		}
	}