H34.createNS("H34.LOGIC");

H34.LOGIC.modificar_status = function(){

	$('#modificar-status').on('click', function(e){

		e.preventDefault();

		$('#status-new').css('display','initial');
		$('#cancelar-cambios').css('display','initial');
		$('#guardar-cambios').css('display','initial');
		$('#status').css('display','none');
		$('#modificar-status').css('display','none');
	})
        
    $('#cancelar-cambios').on('click', function(e){

		e.preventDefault();

		$('#status-new').css('display','none');
		$('#cancelar-cambios').css('display','none');
		$('#guardar-cambios').css('display','none');
		$('#status').css('display','initial');
		$('#modificar-status').css('display','initial');
	})
        
        
        $('#guardar-cambios').on('click', function(e){

		e.preventDefault();
                
                status = $('#status').text().trim();
                td = document.getElementById('status').parentNode;
                select = document.getElementById('status-new')
                status_new = select.options[select.selectedIndex].value
                
                route = $(this).attr('data-route')
                token = $(this).attr('data-token')
                
                if (    status != status_new
                    &&  confirm('¿Confirma que desea modificar el estatus de esta ponencia a "' + status_new + '"?')) {
                    $.ajax({
                        data: {
                            status: status_new,
                            _token : token,
                            _method: 'patch',
                        },
                        type: 'post',
                        url: route,
                    })
                    .done(function(response){
                        console.log(response)
                        $('#status').text(status_new)
                        $(td).removeAttr('class').addClass(response.class)
                    })
                    .fail(function(response){
                        console.log(response)
                    })
                }

		$('#status-new').css('display','none');
		$('#cancelar-cambios').css('display','none');
		$('#guardar-cambios').css('display','none');
		$('#status').css('display','initial');
		$('#modificar-status').css('display','initial');
	})
	
}

H34.CONFIG.coleccion.agregar(H34.LOGIC.modificar_status)