H34.createNS("H34.LOGIC")

H34.LOGIC.seleccionar_sedes = function(){

	checks = $('.sede-check')

	$.each(checks, function(key, value){
		if(value.checked)
			$('#origen_'+value.value).css('display', 'initial');
		else
			$('#origen_'+value.value).css('display', 'none');
	})

	$('.sede-check').on('click', function(){
		if(this.checked){
			$('#origen_'+this.value).css('display', 'initial');
		}else
			$('#origen_'+this.value).css('display', 'none');
	})

}

H34.CONFIG.coleccion.agregar(H34.LOGIC.seleccionar_sedes)