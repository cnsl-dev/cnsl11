H34.createNS("H34.LOGIC.ponencias");

H34.LOGIC.ponencias.filtros = function(){

	this.change = function(e){

		option_sede = $('#sedes option:selected')
		option_status = $('#status option:selected')

		url = '/evento/ponencias/filtros/' + option_sede.val() + '/' + option_status.val()

			$.ajax({
				url: '/evento/ponencias/filtros/' + option_sede.val() + '/' + option_status.val()
			})
			.done(function(e){
				$('#listado').html(e)
			})
			.fail(function(e){
				console.log(e)
			})

	}

	$('#sedes').on('change', this.change)

	$('#status').on('change', this.change)

	$('#reporte').on('click', function(){
		option_sede = $('#sedes option:selected')
		option_status = $('#status option:selected')

		window.open('/evento/ponencias/filtros/' + option_sede.val() + '/' + option_status.val() + '/ods');
	})

}

H34.CONFIG.coleccion.agregar(H34.LOGIC.ponencias.filtros)